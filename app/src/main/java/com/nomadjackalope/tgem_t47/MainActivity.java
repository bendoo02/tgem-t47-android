package com.nomadjackalope.tgem_t47;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerBuffer;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.turnbased.OnTurnBasedMatchUpdateReceivedListener;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchEntity;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;
import com.google.example.games.basegameutils.BaseGameUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends Activity
implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        OnInvitationReceivedListener, OnTurnBasedMatchUpdateReceivedListener {

    public static final String TAG = "SkeletonActivity";

    // Client used to interact with Google APIs
    private GoogleApiClient mGoogleApiClient;

    // Are we currently resolving a connection failure?
    private boolean mResolvingConnectionFailure = false;

    // Has the user clicked the sign-in button?
    private boolean mSignInClicked = false;

    // Automatically start the sign-in flow when the Activity starts
    private boolean mAutoStartSignInFlow = true;

    // Current turn-based match
    private SinglePlayerMatch spMatch = new SinglePlayerMatch();

    // Local convenience pointers
    public TextView mDataView;
    public TextView mTurnTextView;

    private AlertDialog mAlertDialog;

    //------- Full screen stuff --------


    // For our intents
    private static final int RC_SIGN_IN = 9001;
    final static int RC_SELECT_PLAYERS = 10000;
    final static int RC_LOOK_AT_MATCHES = 10001;

    // Should I be showing the turn API?
    public boolean isDoingTurn = false;

    // This is the current match we're in; null if not loaded
    public TurnBasedMatch mMatch;

    // This is the current match data after being unpersisted.
    // Do not retain references to match data once you have
    // taken an action on the match, such as takeTurn()
    public GameTurn mTurnData;

    private MyGLSurfaceView gLView;
    private FrameLayout pLayout;

    public ArrayList<String> playersAvailable = new ArrayList<>();

    SharedPreferences sharedPref;

    // Audio
    private MediaPlayer mediaPlayer;
    private boolean muted;
    private int activeMusicPos;

    // --------------------- Android Activity functions ----------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("Create called");
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Create the Google API Client with access to Plus and Games
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                .build();

        // Setup signin and signout buttons
        //findViewById(R.id.sign_out_button).setOnClickListener(this);
        //findViewById(R.id.sign_in_button).setOnClickListener(this);

        pLayout = (FrameLayout) findViewById(R.id.playLayout);

        // Create a GLSurfaceView instance and set it as the ContentView for this Activity
        gLView = new MyGLSurfaceView(this);
        pLayout.addView(gLView);
//        setContentView(gLView);

        sharedPref = getSharedPreferences("planets", MODE_PRIVATE);

        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.noisetrack06202017);
        muted = sharedPref.getBoolean("musicMuted", false);

        mediaPlayer.setLooping(true);
//        mediaPlayer.start();
        //startMusic();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart(): Connecting to Google APIs");
        mGoogleApiClient.connect();

        gLView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN |
        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop(): Disconnecting from Google APIs");
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            pLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }





    // Displays your inbox. You will get back onActivityResult where
    // you will need to figure out what you clicked on.
    public void onCheckGamesClicked() {
        Intent intent = Games.TurnBasedMultiplayer.getInboxIntent(mGoogleApiClient);
        startActivityForResult(intent, RC_LOOK_AT_MATCHES);
    }

    public void createSinglePlayerGame(GameTurn initialData) {
        createGame(null, 0, initialData);
    }

    public void createGame(ArrayList<String> invitePlayers, int automatchSlots, GameTurn initialData) {
        if(invitePlayers != null && invitePlayers.isEmpty()) {
            invitePlayers = null;
        }

        mTurnData = initialData;



        // Singleplayer
        if(invitePlayers == null && automatchSlots == 0) {

            processResult(new TurnBasedMultiplayer.InitiateMatchResult() {
                @Override
                public TurnBasedMatch getMatch() {
                    return new SinglePlayerMatch();
                }

                @Override
                public Status getStatus() {
                    return new Status(GamesStatusCodes.STATUS_OK);
                }
            });
        } else {
            TurnBasedMatchConfig.Builder tbmcb = TurnBasedMatchConfig.builder();

            if(automatchSlots > 0) {
                Bundle autoMatchCriteria = RoomConfig.createAutoMatchCriteria(
                        automatchSlots, automatchSlots, 0);

                tbmcb.setAutoMatchCriteria(autoMatchCriteria);
            }

            if(invitePlayers != null) {

                tbmcb.addInvitedPlayers(invitePlayers);
            }

            // Start the match
            ResultCallback<TurnBasedMultiplayer.InitiateMatchResult> cb = new ResultCallback<TurnBasedMultiplayer.InitiateMatchResult>() {
                @Override
                public void onResult(TurnBasedMultiplayer.InitiateMatchResult result) {
                    processResult(result);
                }
            };

            Games.TurnBasedMultiplayer.createMatch(mGoogleApiClient, tbmcb.build()).setResultCallback(cb);
        }
    }

    // Opens up game generation menu
    public void onStartMatchClicked() {

        Intent intent = Games.TurnBasedMultiplayer.getSelectOpponentsIntent(mGoogleApiClient,
                1, 7, true);
        startActivityForResult(intent, RC_SELECT_PLAYERS);

//        Intent intent = Games.Players.getPlayerSearchIntent(mGoogleApiClient);
//        startActivityForResult(intent, RC_SELECT_PLAYERS);

    }

    // Create a one-on-one automatch game.
    public void onQuickMatchClicked() {

        Bundle autoMatchCriteria = RoomConfig.createAutoMatchCriteria(
                1, 1, 0);

        TurnBasedMatchConfig tbmc = TurnBasedMatchConfig.builder()
                .setAutoMatchCriteria(autoMatchCriteria).build();

        showSpinner();

        // Start the match
        ResultCallback<TurnBasedMultiplayer.InitiateMatchResult> cb = new ResultCallback<TurnBasedMultiplayer.InitiateMatchResult>() {
            @Override
            public void onResult(TurnBasedMultiplayer.InitiateMatchResult result) {
                processResult(result);
            }
        };
        Games.TurnBasedMultiplayer.createMatch(mGoogleApiClient, tbmc).setResultCallback(cb);
    }



    //---------------------- In-game controls -------------------------------

    // Cancel the game. Should possibly wait until the game is canceled before
    // giving up on the view.
    public void onCancelClicked(View view) {
        showSpinner();
        Games.TurnBasedMultiplayer.cancelMatch(mGoogleApiClient, mMatch.getMatchId())
                .setResultCallback(new ResultCallback<TurnBasedMultiplayer.CancelMatchResult>() {
                    @Override
                    public void onResult(TurnBasedMultiplayer.CancelMatchResult result) {
                        processResult(result);
                    }
                });
        isDoingTurn = false;
        setViewVisibility();
    }

    // Leave the game during your turn. Note that there is a separate // Removes you from the game
    // Games.TurnBasedMultiplayer.leaveMatch() if you want to leave NOT on your turn.
    public void onLeaveClicked(boolean isUserTurn) {
        showSpinner();

        // Multiplayer
        if(mMatch.getParticipantIds().size() > 1) {
            if (isUserTurn) {
                String nextParticipantId = getNextParticipantId();

                Games.TurnBasedMultiplayer.leaveMatchDuringTurn(mGoogleApiClient, mMatch.getMatchId(),
                        nextParticipantId).setResultCallback(
                        new ResultCallback<TurnBasedMultiplayer.LeaveMatchResult>() {
                            @Override
                            public void onResult(TurnBasedMultiplayer.LeaveMatchResult result) {
                                processResult(result);
                            }
                        });
            } else {

                Games.TurnBasedMultiplayer.leaveMatch(mGoogleApiClient, mMatch.getMatchId());
            }
        } else {
            // Single player
            gLView.updateMainState(MyGLSurfaceView.STATE_MAIN_MENU, MyGLSurfaceView.STATE_NOT_USER_TURN);
        }
    }

    // Finish the game. Sometimes, this is your only choice.
    public void onFinishClicked() {

        // Multiplayer
        if(mMatch.getParticipantIds().size() > 1) {
            showSpinner();
            Games.TurnBasedMultiplayer.finishMatch(mGoogleApiClient, mMatch.getMatchId())
                    .setResultCallback(
                            new ResultCallback<TurnBasedMultiplayer.UpdateMatchResult>() {
                                @Override
                                public void onResult(TurnBasedMultiplayer.UpdateMatchResult result) {
                                    dismissSpinner();
                                }
                            });
//                    .setResultCallback(new ResultCallback<TurnBasedMultiplayer.UpdateMatchResult>() {
//                        @Override
//                        public void onResult(TurnBasedMultiplayer.UpdateMatchResult result) {
//                            processResult(result);
//                        }
//                    });

            isDoingTurn = false;
        } else {
            //((SinglePlayerMatch) mMatch).setStatus(TurnBasedMatch.MATCH_STATUS_COMPLETE);

            // Singleplayer
//            processResult(new TurnBasedMultiplayer.UpdateMatchResult() {
//                @Override
//                public TurnBasedMatch getMatch() {
//                    return mMatch;
//                }
//
//                @Override
//                public Status getStatus() {
//                    return new Status(GamesStatusCodes.STATUS_OK);
//                }
//            });
        }
    }

    // Upload your new gamestate, then take a turn, and pass it on to the next
    // player.
    public void onDoneClicked() {
        showSpinner();

        // Multiplayer
        if(mTurnData.numHumanPlayers > 1 || mMatch.getAvailableAutoMatchSlots() > 0) {

            String nextParticipantId = getNextParticipantId();

            if(mMatch.getAvailableAutoMatchSlots() > 0) {
                nextParticipantId = null; //TODO PROBLEM, during game user could drop, then autoMatch+1, then all users switch ships
            }

            Games.TurnBasedMultiplayer.takeTurn(mGoogleApiClient, mMatch.getMatchId(),
                    mTurnData.persist(), nextParticipantId).setResultCallback(
                    new ResultCallback<TurnBasedMultiplayer.UpdateMatchResult>() {
                        @Override
                        public void onResult(TurnBasedMultiplayer.UpdateMatchResult result) {
                            processResult(result);
                        }
                    });

            mTurnData = null;

        } else {
            // Singleplayer

            ((SinglePlayerMatch) mMatch).setData(mTurnData.persist());

            processResult(new TurnBasedMultiplayer.UpdateMatchResult() {
                @Override
                public TurnBasedMatch getMatch() {
                    return mMatch;
                }

                @Override
                public Status getStatus() {
                    return new Status(GamesStatusCodes.STATUS_OK);
                }
            });


        }


    }


    // ---------------------------- Game save ---------------------------------

    class SaveGameData extends AsyncTask<byte[], String, String> {
        @Override
        protected String doInBackground(byte[]... params) {

            // Save game here

            return null;
        }
    }

    private void saveSingleGame(GameTurn singlePlayerGame) {
        String filename = "SavedGames";
        byte[] bytes = singlePlayerGame.persist();
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(filename, MODE_PRIVATE);
            outputStream.write(bytes);
            outputStream.close();

        } catch (Exception e) {
            System.out.println("MA| things went poorly saving");
            e.printStackTrace();
        }

    }

    private byte[] openSingleGame() {
        String filename = "SavedGames";
        byte[] bytes = new byte[0];
        int i = 0;

        FileInputStream inputStream;

        int data;

        try {
            inputStream = openFileInput(filename);
            bytes = new byte[inputStream.available()];
            data = inputStream.read();

            while(data != -1) {
                bytes[i] = (byte) data;

                data = inputStream.read();
                i++;
            }


        } catch (Exception e) {
            System.out.println("MA| things went poorly opening");
            e.printStackTrace();
        }

        return bytes;
    }




    // Sign-in, Sign out behavior
    public boolean isSignedIn = false;

    // Update the visibility based on what state we're in.
    public void setViewVisibility() {
        isSignedIn = (mGoogleApiClient != null) && (mGoogleApiClient.isConnected());

        if (!isSignedIn) {
            //findViewById(R.id.login_layout).setVisibility(View.VISIBLE);
            //findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);


            if (mAlertDialog != null) {
                mAlertDialog.dismiss();
            }
            return;
        } else {
            System.out.println("MA| mainState: " + gLView.getMainState());
            if(gLView.getMainState() == MyGLSurfaceView.STATE_MAIN_MENU) {
                boolean open = gLView.mainMenu.isOpen();
                Point point = gLView.mainMenu.getPosition();

                gLView.updateMainState();

                if(open) {
                    gLView.mainMenu.open(point, 1);
                }
            }
        }

        //findViewById(R.id.login_layout).setVisibility(View.GONE);

//        if (isDoingTurn) {
            gLView.setVisibility(View.VISIBLE);
//        } else {
//            gLView.setVisibility(View.GONE);
//        }
    }

    // Helpful dialogs

    public void showSpinner() {
        findViewById(R.id.progressLayout).setVisibility(View.VISIBLE);
    }

    public void dismissSpinner() {
        findViewById(R.id.progressLayout).setVisibility(View.GONE);
    }

    // Generic warning/info dialog
    public void showWarning(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle(title).setMessage(message);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                    }
                });

        // create alert dialog
        mAlertDialog = alertDialogBuilder.create();

        // show it
        mAlertDialog.show();
    }

    // This function is what gets called when you return from either the Play
    // Games built-in inbox, or else the create game built-in interface.
    // This is an Android function that is called from startActivityForResult()
    @Override
    public void onActivityResult(int request, int response, Intent data) {
        super.onActivityResult(request, response, data);
        // -------------- SIGN IN --------------------
        if (request == RC_SIGN_IN) {
            mSignInClicked = false;
            mResolvingConnectionFailure = false;
            if (response == Activity.RESULT_OK) {
                mGoogleApiClient.connect();
            } else {
                BaseGameUtils.showActivityResultError(this, request, response, R.string.signin_other_error);
            }
            // ----------------- LOOK AT MATCHES -----------------
        } else if (request == RC_LOOK_AT_MATCHES) {
            // Returning from the 'Select Match' dialog

            if (response != Activity.RESULT_OK) {
                // user canceled
                return;
            }

            TurnBasedMatch match = data
                    .getParcelableExtra(Multiplayer.EXTRA_TURN_BASED_MATCH);


            if (match != null) {

                // match might be a rematch
                startNewMatch(match);
            }

            Log.d(TAG, "Match = " + match);

        // ------------------- SELECT PLAYERS --------------------
        } else if (request == RC_SELECT_PLAYERS) {
            // Returned from 'Select players to Invite' dialog

            if (response != Activity.RESULT_OK) {
                // user canceled
                return;
            }

            // get the invitee list
            final ArrayList<String> invitees = data
                    .getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);

            // get automatch criteria
            Bundle autoMatchCriteria = null;

            int minAutoMatchPlayers = data.getIntExtra(
                    Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
            int maxAutoMatchPlayers = data.getIntExtra(
                    Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);

            if (minAutoMatchPlayers > 0) {
                autoMatchCriteria = RoomConfig.createAutoMatchCriteria(
                        minAutoMatchPlayers, maxAutoMatchPlayers, 0);
            } else {
                autoMatchCriteria = null;
            }


            // Set up our turn data to still exist when the callback comes so we know how many planets... etc. to load
            // If a player joins an already existing game this will be ignored
            // This is used by processResult(initiateMatchResult)
            mTurnData = new GameTurn(minAutoMatchPlayers + invitees.size() + 1, 0, 2, 0, 6);


            TurnBasedMatchConfig tbmc = TurnBasedMatchConfig.builder()
                    .addInvitedPlayers(invitees)
                    .setAutoMatchCriteria(autoMatchCriteria).build();

            // Start the match
            Games.TurnBasedMultiplayer.createMatch(mGoogleApiClient, tbmc).setResultCallback(
                    new ResultCallback<TurnBasedMultiplayer.InitiateMatchResult>() {
                        @Override
                        public void onResult(TurnBasedMultiplayer.InitiateMatchResult result) {
                            processResult(result);
                        }
                    });
            showSpinner();
        }
    }







    //--------------------------- Game Initialization -------------------------

    public void startNewMatch(TurnBasedMatch match) {

        // If match has not been initialized, do it. Do it now! ;)
        if(match.getData() != null) {
            System.out.println("MA| data not null");
            updateMatch(match);
            return;
        }

        // On rematch, use the previous match settings
        if(match.getPreviousMatchData() != null) {
            System.out.println("MA| prev data not null");
            GameTurn tempTurn = GameTurn.unpersist(match, match.getPreviousMatchData());

            mTurnData = new GameTurn(tempTurn.numHumanPlayers, tempTurn.numCPUPlayers,
                    tempTurn.numStationsPerPlayer, 0, tempTurn.planets.length);
        }

        mTurnData = initializeGame(mTurnData);


        if(mTurnData.numHumanPlayers > 1) {
            Games.TurnBasedMultiplayer.takeTurn(mGoogleApiClient, match.getMatchId(),
                    mTurnData.persist(), match.getPendingParticipantId()).setResultCallback(
                    new ResultCallback<TurnBasedMultiplayer.UpdateMatchResult>() {
                        @Override
                        public void onResult(TurnBasedMultiplayer.UpdateMatchResult result) {
                            processResult(result);
                        }
                    });
        } else {
            mMatch = match;
            onDoneClicked();
        }
    }

    public GameTurn initializeGame(GameTurn turn) {
        generationSuccess = false;

        // Make sure that the game setup creates a legit map where no planets or ships overlap
        int generationTries = 5;
        while(!generationSuccess && generationTries > 0) {
            generationSuccess = true;

            turn = initializePlanets(turn);

            // We only need to do this if planets succeeded
            if(generationSuccess) {
                // Create Ships
                turn = initializeShips(turn);
            }

            generationTries--;

        }

        if(generationTries == 0) {
            // I tried my best to find a location with the parameters you requested but could not.
            // Please edit your parameters and retry.
            System.out.println("MA| generationTries == 0");
        }

        return turn;
    }

    private boolean generationSuccess = false;

    public GameTurn initializePlanets(GameTurn turn) {

        for (int i = 0; i < turn.planets.length; i++) {
            turn.planets[i] = new Planet();
        }

        // 1. Pick random point
        // 2. Apply point to planet
        // 3. check if overlap w/other planets
        // 4 if fail i-- (stay on same planet)
        // 5. goto #1

        Random random = new Random();

        Boolean nextPlanet = true;

        float bias = 0.1f;

        // Tracker for debugging
        int numTimesInLoop = 0;

        for(int i = 0; i < turn.planets.length; i++) {
            // Reseed random generator
            // random = new Random(something random);
            //planets[i].sprite = Sprite::create("planet.png");
            // Got ranges from the ratio of 1920/1080 = 1.777
            turn.planets[i].setPositionX(random.nextFloat() * 34 - 17); // between -17 and 17
            turn.planets[i].setPositionY(random.nextFloat() * 20 - 10 ); // Between -10 and 10
            turn.planets[i].setRadius(random.nextFloat() * 2 + 0.25f); // Between 0.25 and 2.25
            turn.planets[i].setGravity(0.8f * turn.planets[i].getRadius() * turn.planets[i].getRadius() * turn.planets[i].getRadius()); // * planets[i].sprite->getScale();

            int p = i;
            while (p > 0) {
                p--;

                // Distance between planet centers
                float dist = getDistance(turn.planets[p], turn.planets[i]);

                // If the radii of the two planets will overlap don't move on to the next planet
                if (dist < turn.planets[i].getRadius() + turn.planets[p].getRadius() + bias) {
                    nextPlanet = false;
                }
            }

            if (!nextPlanet) {
                i--;
            }

            nextPlanet = true;

            numTimesInLoop++;

            if(numTimesInLoop > 200) {
                generationSuccess = false;
                return turn;
            }

        }

        return turn;

    }

    public GameTurn initializeShips(GameTurn turn) {

        // Iterate for the requested number of ships per player * num players
        for (int i = 0; i < turn.ships.length; i++) {
            turn.ships[i] = new Ship();

            // Real person ships
            if(i < turn.numHumanPlayers * turn.numStationsPerPlayer) {
                turn.ships[i].userId = "p_" + (i / turn.numStationsPerPlayer + 1);
            } else {
                // Computer player ships
                turn.ships[i].userId = "c_" + (i / turn.numStationsPerPlayer + turn.numHumanPlayers);
            }

            int whileStop = 300;
            while (colliding(turn.ships, turn.planets, i) && whileStop > 0) {
                whileStop--;
            }

//            System.out.println("MA| whileStop: " + whileStop);

            if(whileStop == 0) {
                // Retry planets, ship generation because the current ship is likely in a planet
                generationSuccess = false;
                return turn;
            }
        }

        return turn;

    }

    public float getDistance(Planet p1, Planet p2) {
        return (float) Math.sqrt((p2.getPositionX() - p1.getPositionX())*(p2.getPositionX() - p1.getPositionX()) + (p2.getPositionY() - p1.getPositionY())*(p2.getPositionY() - p1.getPositionY()));
    }

    public float getDistance(Ship p1, Ship p2) {
        return (float) Math.sqrt((p2.getPositionX() - p1.getPositionX())*(p2.getPositionX() - p1.getPositionX()) + (p2.getPositionY() - p1.getPositionY())*(p2.getPositionY() - p1.getPositionY()));
    }

    public float getDistance(Ship p1, Planet p2) {
        return (float) Math.sqrt((p2.getPositionX() - p1.getPositionX())*(p2.getPositionX() - p1.getPositionX()) + (p2.getPositionY() - p1.getPositionY())*(p2.getPositionY() - p1.getPositionY()));
    }

    // Returns true if collision
    private boolean colliding(Ship[] ships, Planet[] planets, int k) {
        Random random = new Random();
        // Got ratio for x vals from 1920/1080 = 1.777 * Y range
        ships[k].setNextRoundLocation(
                random.nextFloat() * 26 - 13,  // Between -13 and 13
                random.nextFloat() * 16 - 8); // Between -8 and 8

        ships[k].setNextRoundStartLocation(ships[k].getNextRoundLocation());
        ships[k].setPrevRoundLocation(ships[k].getNextRoundLocation());
        ships[k].setCurrentLocation(ships[k].getNextRoundLocation());

        // todo work out a system to not put enemy ships very close together
        for (int j = 0; j < k; ++j) {
            float dist = getDistance(ships[j], ships[k]);
            if(dist < ships[j].getScaleX() + ships[k].getScaleX()) {
                return true;
            }
        }


        for (int i = 0; i < planets.length; ++i) {
            float dist = getDistance(ships[k], planets[i]);
            if(dist < ships[k].getScaleX() + planets[i].getScaleX()) {
                return true;
            }
        }

        return false;
    }



    // ----------------------- Google Play API calls --------------------------

    // If you choose to rematch, then call it and wait for a response.
    public void rematch() {
        showSpinner();
        Games.TurnBasedMultiplayer.rematch(mGoogleApiClient, mMatch.getMatchId()).setResultCallback(
                new ResultCallback<TurnBasedMultiplayer.InitiateMatchResult>() {
                    @Override
                    public void onResult(TurnBasedMultiplayer.InitiateMatchResult result) {
                        processResult(result);
                    }
                });
        mMatch = null;
        isDoingTurn = false;
    }

    /**
     * Get the next participant. In this function, we assume that we are
     * round-robin, with all known players going before all automatch players.
     * This is not a requirement; players can go in any order. However, you can
     * take turns in any order.
     *
     * @return participantId of next player, or null if automatching
     */
    public String getNextParticipantId() {

        // Multiplayer
        if(mMatch.getParticipantIds().size() > 1) {
            String playerId = Games.Players.getCurrentPlayerId(mGoogleApiClient);
            String myParticipantId = mMatch.getParticipantId(playerId);

            ArrayList<String> participantIds = mMatch.getParticipantIds();

            int desiredIndex = -1;

            for (int i = 0; i < participantIds.size(); i++) {
                if (participantIds.get(i).equals(myParticipantId)) {
                    desiredIndex = i + 1;
                }
            }

            if (desiredIndex < participantIds.size()) {
                return participantIds.get(desiredIndex);
            }

            if (mMatch.getAvailableAutoMatchSlots() <= 0) {
                // You've run out of automatch slots, so we start over.
                return participantIds.get(0);
            } else {
                // You have not yet fully automatched, so null will find a new
                // person to play against.
                return null;
            }

        } else {

            // Single player
            if(mMatch.getParticipantIds().size() == 1) {
                return mMatch.getParticipantIds().get(0);
            } else {
                return null;
            }
        }
    }

    public String getCurrentParticipantId() {
        System.out.println("MA| current player id: " + mMatch.getPendingParticipantId());
        return mMatch.getPendingParticipantId();
    }

    // Got this from https://github.com/SempaiGames/extension-googleplaygames/blob/master/dependencies/gpgex/src/com/gpgex/GooglePlayGames.java
    public boolean loadFriends(final boolean getConnectedPlayers, boolean clearCache, final int resultsCount) {
        try {
            final int FRIENDS_PER_PAGE = 5;
            final ResultCallback<Players.LoadPlayersResult> resultCallback = new ResultCallback<Players.LoadPlayersResult>() {
                @Override
                public void onResult(@NonNull Players.LoadPlayersResult result) {
                    PlayerBuffer playerBuffer = result.getPlayers();
                    Log.v("MA", "loadAllFriends: onResult... got " + playerBuffer.getCount());

                    for(Player player : playerBuffer) {
                        player.getDisplayName();

                        // DO SOMETHING HERE to add the players to a list?
                        playersAvailable.add(player.getPlayerId());
                        System.out.println("MA| player: " + player.getDisplayName());
                    }
                }
            };

            if(getConnectedPlayers) {
                Games.Players.loadConnectedPlayers(mGoogleApiClient, clearCache).setResultCallback(resultCallback);
//                Games.Players.loadRecentlyPlayedWithPlayers(mGoogleApiClient, FRIENDS_PER_PAGE, clearCache).setResultCallback(resultCallback);
            } else {
                if(resultsCount == 0) {
                    System.out.println("MA| current player: " + Games.Players.getCurrentPlayer(mGoogleApiClient).getDisplayName());
                    Games.Players.loadInvitablePlayers(mGoogleApiClient, FRIENDS_PER_PAGE, clearCache).setResultCallback(resultCallback);
                } else {
                    Games.Players.loadMoreInvitablePlayers(mGoogleApiClient, FRIENDS_PER_PAGE).setResultCallback(resultCallback);
                }
            }
        } catch (Exception e) {
            Log.i("MA", "PlayGames: loadFriends Exception");
            Log.i("MA", e.toString());
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.d(TAG, "onConnected(): Connection successful");

        setViewVisibility();

        // As a demonstration, we are registering this activity as a handler for
        // invitation and match events.

        // This is *NOT* required; if you do not register a handler for
        // invitation events, you will get standard notifications instead.
        // Standard notifications may be preferable behavior in many cases.
        Games.Invitations.registerInvitationListener(mGoogleApiClient, this);

        // Likewise, we are registering the optional MatchUpdateListener, which
        // will replace notifications you would get otherwise. You do *NOT* have
        // to register a MatchUpdateListener.
        Games.TurnBasedMultiplayer.registerMatchUpdateListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended():  Trying to reconnect.");
        mGoogleApiClient.connect();
        setViewVisibility();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed(): attempting to resolve");
        if (mResolvingConnectionFailure) {
            // Already resolving
            Log.d(TAG, "onConnectionFailed(): ignoring connection failure, already resolving.");
            return;
        }

        // Launch the sign-in flow if the button was clicked or if auto sign-in is enabled
        if (mSignInClicked || mAutoStartSignInFlow) {
            mAutoStartSignInFlow = false;
            mSignInClicked = false;

            mResolvingConnectionFailure = BaseGameUtils.resolveConnectionFailure(this,
                    mGoogleApiClient, connectionResult, RC_SIGN_IN,
                    getString(R.string.signin_other_error));
        }

        setViewVisibility();
    }






    // This is the main function that gets called when players choose a match
    // from the inbox, or else create a match and want to start it.
    public void updateMatch(TurnBasedMatch match) {
        setViewVisibility();

        mMatch = match;

        int status = match.getStatus();
        int turnStatus = match.getTurnStatus();

        if(status == TurnBasedMatch.MATCH_STATUS_ACTIVE
                || status == TurnBasedMatch.MATCH_STATUS_AUTO_MATCHING
                || status == TurnBasedMatch.MATCH_STATUS_COMPLETE) {

            // OK, it's active. Check on turn status.
            if (turnStatus == TurnBasedMatch.MATCH_TURN_STATUS_MY_TURN) {
                mTurnData = GameTurn.unpersist(mMatch);

                gLView.updateMainState(MyGLSurfaceView.STATE_GAME, MyGLSurfaceView.STATE_USER_TURN);
            } else {
//                gLView.updateMainState(MyGLSurfaceView.STATE_GAME, MyGLSurfaceView.STATE_NOT_USER_TURN);
                gLView.updateMainState(MyGLSurfaceView.STATE_MAIN_MENU);
            }

        }

//        mTurnData = null;
    }








    private void processResult(TurnBasedMultiplayer.CancelMatchResult result) {
        dismissSpinner();

        if (!checkStatusCode(null, result.getStatus().getStatusCode())) {
            return;
        }

        isDoingTurn = false;

        showWarning("Match",
                "This match is canceled.  All other players will have their game ended.");
    }

    private void processResult(TurnBasedMultiplayer.InitiateMatchResult result) {
        TurnBasedMatch match = result.getMatch();
        dismissSpinner();

        if (!checkStatusCode(match, result.getStatus().getStatusCode())) {
            return;
        }


        System.out.println("MA| Data null, game id: " + (match.getData() == null) + ", " + match.getMatchId());

        startNewMatch(match);

    }

    private void processResult(TurnBasedMultiplayer.LeaveMatchResult result) {
        TurnBasedMatch match = result.getMatch();
        dismissSpinner();
        if (!checkStatusCode(match, result.getStatus().getStatusCode())) {
            return;
        }
        //isDoingTurn = (match.getTurnStatus() == TurnBasedMatch.MATCH_TURN_STATUS_MY_TURN);
        showWarning("Left", "You've left this match.");
    }

    public void processResult(TurnBasedMultiplayer.UpdateMatchResult result) {
        TurnBasedMatch match = result.getMatch();
        dismissSpinner();
        if (!checkStatusCode(match, result.getStatus().getStatusCode())) {
            return;
        }

        isDoingTurn = (match.getTurnStatus() == TurnBasedMatch.MATCH_TURN_STATUS_MY_TURN ||
                            match.getTurnStatus() == TurnBasedMatch.MATCH_STATUS_COMPLETE);

        if (isDoingTurn) {
            updateMatch(match);
            return;
        }

        setViewVisibility();
    }


    // Handle notification events.
    @Override
    public void onInvitationReceived(Invitation invitation) {
        Toast.makeText(
                this,
                "An invitation has arrived from "
                        + invitation.getInviter().getDisplayName(), Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onInvitationRemoved(String invitationId) {
        Toast.makeText(this, "An invitation was removed.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTurnBasedMatchReceived(TurnBasedMatch match) {
        Toast.makeText(this, "A match was updated.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTurnBasedMatchRemoved(String matchId) {
        Toast.makeText(this, "A match was removed.", Toast.LENGTH_SHORT).show();

    }

    public void showErrorMessage(TurnBasedMatch match, int statusCode,
                                 int stringId) {

        showWarning("Warning", getResources().getString(stringId));
    }

    // Returns false if something went wrong, probably. This should handle
    // more cases, and probably report more accurate results.
    private boolean checkStatusCode(TurnBasedMatch match, int statusCode) {
        switch (statusCode) {
            case GamesStatusCodes.STATUS_OK:
                return true;
            case GamesStatusCodes.STATUS_NETWORK_ERROR_OPERATION_DEFERRED:
                // This is OK; the action is stored by Google Play Services and will
                // be dealt with later.
                Toast.makeText(
                        this,
                        "Stored action for later.  (Please remove this toast before release.)",
                        Toast.LENGTH_SHORT).show();
                // NOTE: This toast is for informative reasons only; please remove
                // it from your final application.
                return true;
            case GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_NOT_TRUSTED_TESTER:
                showErrorMessage(match, statusCode,
                        R.string.status_multiplayer_error_not_trusted_tester);
                break;
            case GamesStatusCodes.STATUS_MATCH_ERROR_ALREADY_REMATCHED:
                showErrorMessage(match, statusCode,
                        R.string.match_error_already_rematched);
                break;
            case GamesStatusCodes.STATUS_NETWORK_ERROR_OPERATION_FAILED:
                showErrorMessage(match, statusCode,
                        R.string.network_error_operation_failed);
                break;
            case GamesStatusCodes.STATUS_CLIENT_RECONNECT_REQUIRED:
                showErrorMessage(match, statusCode,
                        R.string.client_reconnect_required);
                break;
            case GamesStatusCodes.STATUS_INTERNAL_ERROR:
                showErrorMessage(match, statusCode, R.string.internal_error);
                break;
            case GamesStatusCodes.STATUS_MATCH_ERROR_INACTIVE_MATCH:
                showErrorMessage(match, statusCode,
                        R.string.match_error_inactive_match);
                break;
            case GamesStatusCodes.STATUS_MATCH_ERROR_LOCALLY_MODIFIED:
                showErrorMessage(match, statusCode,
                        R.string.match_error_locally_modified);
                break;
            default:
                showErrorMessage(match, statusCode, R.string.unexpected_status);
                Log.d(TAG, "Did not have warning or string to deal with: "
                        + statusCode);
        }

        return false;
    }

    public void signIn() {
        // Check to see the developer who's running this sample code read the instructions :-)
        // NOTE: this check is here only because this is a sample! Don't include this
        // check in your actual production app.
        if (!BaseGameUtils.verifySampleSetup(this, R.string.app_id)) {
            Log.w(TAG, "*** Warning: setup problems detected. Sign in may not work!");
        }

        mSignInClicked = true;
        //findViewById(R.id.sign_in_button).setVisibility(View.GONE);
        mGoogleApiClient.connect();
    }

    // ------------------------ Audio --------------------------
    private void startMusic() {
        if(muted) return;

        if(mediaPlayer != null) {
            mediaPlayer.start();
        }
    }

    private void pauseMusic() {
        if(mediaPlayer != null) {
            if(mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            }
            activeMusicPos = mediaPlayer.getCurrentPosition();
        }
    }

    private void stopMusic() {
        pauseMusic();

        if(mediaPlayer != null) {
            if(mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void muteMusic() {
        muted = true;
        sharedPref.edit().putBoolean("musicMuted", muted).apply();
        pauseMusic();
    }

    public void unmuteMusic() {
        muted = false;
        sharedPref.edit().putBoolean("musicMuted", muted).apply();
        startMusic();
    }

    public boolean isMuted() {
        return muted;
    }

    @Override
    protected void onPause() {
        super.onPause();
        gLView.onPause();

        pauseMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gLView.onResume();

        startMusic();
    }
}
