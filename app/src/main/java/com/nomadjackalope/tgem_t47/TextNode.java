package com.nomadjackalope.tgem_t47;

/**
 * Created by benjamin on 3/27/17.
 */
public class TextNode extends Node {

    public static final int RIGHT = 0; // Means text's right side ends at node's x position // Text.
    public static final int LEFT = 1; // Means text's left side starts at nodes's x position // .Text
    public static final int CENTER = 2; // Means text's center is at node's x position  // Te.xt

    String text;

    //Unscaled space
    float spaceX;

    public TextNode(String name) {
        init(name, name);
    }

    private void init(String name, String text) {
        initDefaults(name);
        spaceX = 0.0f;

        setText(text);

        baseShape = new BatchTextRectangle(FontObjectPNG.CHAR_BATCH_SIZE);

        baseShape.touchScale = 1.5f;

        material = MyGLRenderer.fontMat;

        setScale(1.0f);
    }

    @Override
    public void draw(MyGLRenderer renderer) {
        if(material == null) {
            material = renderer.getDefaultMaterial();
        }

        for (int i = 0; i < getActions().size(); i++) {
            if(getActions().get(i).update(MyGLRenderer.clock)) {
                // We don't want to remove
                //actionsToRemove.add(i);
                getActions().remove(i);
                i--;
            }
        }

        if(isVisible()) {
            ((BatchTextRectangle) baseShape).draw(this, renderer.getViewMatrix(), renderer.getProjectionMatrix(),
                    material, getPosition(), getScaleX(), getScaleY(), getRotation());

        }

        // Tells children to render
        int childrenSize = children.size();
        for (int i = 0; i < childrenSize; i++) {
            children.get(i).draw(renderer);
        }
    }

    @Override
    public void updateMatrix() {
        super.updateMatrix();



    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setAlign(int align) {
        ((BatchTextRectangle)baseShape).align = align;
    }

    public int getAlign() {
        return ((BatchTextRectangle)baseShape).align;
    }

    public float getSpaceX() {
        return spaceX;
    }

    public void setSpaceX(float spaceX) {
        this.spaceX = spaceX;
    }

    public float getMaxWidth() {
        return ((BatchTextRectangle)baseShape).maxWidth;
    }

    public void setMaxWidth(float maxWidth) {
        ((BatchTextRectangle)baseShape).maxWidth = maxWidth;
    }
}
