package com.nomadjackalope.tgem_t47;

import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by benjamin on 1/20/17.
 */
public class Ship extends Node{

//    float radius;

    // Previous round simulation data
    float prevRoundLaserPower = 0;
    float prevRoundShieldPower = 0;
    float prevRoundEnginePower = 0;
    float prevRoundRotation = 0;
    float prevRoundX = 0;
    float prevRoundY = 0;
    boolean prevRoundDead = false;
    int prevRoundLaserShieldEngine = 0; // 0 = Laser, 1 = Shield, 2 = Engine

    // To be shown values
    float nextRoundLaserPower = 0;
    float nextRoundShieldPower = 0;
    float nextRoundEnginePower = 0;
    float nextRoundRotation = 0;
    float nextRoundStartX = 0;
    float nextRoundStartY = 0;
    float nextRoundEndX = 0;
    float nextRoundEndY = 0;
    boolean nextRoundDead = false;
    int nextRoundLaserShieldEngine = 0;

    // Live values
    float currentLaserPower = 0;
    float currentShieldPower = 0;
    float currentEnginePower = 0;
    float currentRotation = 0;
    float currentX = 0;
    float currentY = 0;
    boolean currentIsDead = false;
    int currentLaserShieldEngine = 0;

    private boolean isMoving = true;

    boolean isEnemy = true;

    String userId;

    private float previousPointRot;
    private float previousPointPow;

    Menu menu;

    // Tracks the path of the laser
    LaserPath laserPath = new LaserPath("LaserPath");

    ParticleEmitter laserEmitter = new ParticleEmitter("Particle emitter");

    // ------------ Sample Colors -------------------
    public static final float[] GREEN = {0.339f, 0.719f, 0.403f, 1.0f};
    public static final float[] RED = {0.6980f, 0.1176f, 0.2510f, 1.0f};
    public static final float[] PURPLE = {0.988f, 0.988f, 0.384f, 1.0f};
    public static final float[] ORANGE = {0.9333f, 0.6078f, 0.3411f, 1.0f};
    public static final float[] MINT = {0.3411f, 0.9333f, 0.6078f, 1.0f};

    static int tempID = 0;

    private float velocity = 0;

    public Ship() {
        init("Ship", 0, 0);
    }

    public Ship(String name) {
        init(name, 0, 0);
    }

    public Ship(String name, float x, float y) {
        init(name, x, y);
    }

    public void init(String name, float x, float y) {
        this.setName(name);
        this.setPositionX(x);
        this.setPositionY(y);

        setNextRoundLaserPower(0.32f);

        //radius = 0.5f;

        addChild(laserPath);

        // Blue
        baseShape.color = new float[] {0.24f, 0.64f, 0.95f, 1.0f};

        baseShape = new RectSprite(name + " sprite");
        baseShape.id = userId + " " + tempID++;
        ((RectSprite) baseShape).textureRef = 2;
        baseShape.touchScale = 2.0f;

        material = MyGLRenderer.texturedMat;

        this.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                Menu menu = ((Ship) sender).getMenu();
                if(menu == null) {
                    ((Ship) sender).createMenu();
                    menu = ((Ship) sender).getMenu();
                }
                menu.toggle(sender.getPosition(), sender.getScaleX());
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        createShield();

        laserEmitter.pLife = 100.0f;
        laserEmitter.pVelocityVariation.set(0.0003f, 0.0003f, 0f);


        addChild(laserEmitter);
    }

    public void createMenu() {
        if(!isEnemy) {
            createShipMenu();
        } else {
            createShipInfoMenu();
        }
    }

    private Node shield;
    private Node laser;
    private Node engine;

    // Call this if the userId == currentUserId
    public void createShipMenu() {
        // Create ship Menu
        Menu shipMenu = new Menu("Ship menu", 0, 0);

        setMenu(shipMenu);

        updateShipMenu();

    }

    private void addLSE() {
        // ------  LSE  ------
        shield = new Node("shield option");
        laser = new Node("laser option");
        engine = new Node("engine option");

        shield.setRelativePos(true, new Point(-2f, -2f));
        laser.setRelativePos(true, new Point(0f, -2f));
        engine.setRelativePos(true, new Point(2f, -2f));

        shield.baseShape = new RectSprite("LSE shield");
        laser.baseShape = new RectSprite("LSE laser");
        engine.baseShape = new RectSprite("LSE engine");

        shield.baseShape.touchScale = 1.75f;
        laser.baseShape.touchScale = 1.75f;
        engine.baseShape.touchScale = 1.75f;

        ((RectSprite) shield.baseShape).textureRef = 6;
        ((RectSprite) laser.baseShape).textureRef = 4;
        ((RectSprite) engine.baseShape).textureRef = 5;

        shield.material = MyGLRenderer.texturedMat;
        laser.material = MyGLRenderer.texturedMat;
        engine.material = MyGLRenderer.texturedMat;

//        shield.setPosition(-1, 0);
//        laser.setPosition(0, 0);
//        engine.setPosition(1, 0);

        shield.setScale(1f);
        laser.setScale(1f);
        engine.setScale(1f);

        TouchListener tl = new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                switch (sender.getName()) {
                    case "shield option":
                        setNextRoundLaserShieldEngine(1);
                        break;
                    case "laser option":
                        setNextRoundLaserShieldEngine(0);
                        break;
                    case "engine option":
                        setNextRoundLaserShieldEngine(2);
                        break;
                    default:
                        break;
                }
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        };

        shield.addTouchListener(tl);
        laser.addTouchListener(tl);
        engine.addTouchListener(tl);

        getMenu().addChild(shield, false);
        getMenu().addChild(laser, false);
        getMenu().addChild(engine, false);


    }

    private void updateShipMenu() {

        if(getMenu() == null || isEnemy) {
            return;
        }

//        getMenu().removeAllMenuItems();

        getMenu().removeAllChildren();

        addLSE();

        if(currentLaserShieldEngine == 0) {

            // ------ Laser ------
            final Node circleNode = new Node("Circle");
            circleNode.setPosition(getPosition());
            circleNode.baseShape = new Circle("laser power meter");
            circleNode.setIgnoreTouch(true);
            ((Circle) circleNode.baseShape).setNumSegmentsVisible(getCurrentLaserPower() * 2); //getPrevRoundLaserPower() * 2);
            circleNode.updateMatrix();

//            TextNode power = new TextNode("Power");
//
//            power.addTouchListener(new TouchListener() {
//                @Override
//                public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
////                    System.out.println("Ship| currentPower: " + currentLaserPower);
//                    previousPointPow = calculatePower(realPoint);
//                    return true;
//                }
//
//                @Override
//                public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
//
//                    //System.out.println("Ship| currentLaserPower: " + currentLaserPower);
//
//                    float tempPower = calculatePower(realPoint);
//                    //
//                    float deltaPower = tempPower - previousPointPow;
//                    //System.out.println("Ship| delta power: " + deltaPower);
//
//                    setNextRoundLaserPower(getCurrentLaserPower() + deltaPower);
//
//                    ((Circle) circleNode.baseShape).setNumSegmentsVisible(getNextRoundLaserPower() * 2);
//
//                    previousPointPow = tempPower;
//                    return true;
//                }
//
//                @Override
//                public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
//                    //previousPoint = null;
//                    return true;
//                }
//            });
//
//            final TextNode rotation = new TextNode("Rotation");
//
//            rotation.addTouchListener(new TouchListener() {
//                @Override
//                public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
////                    System.out.println("Ship| Rotation: " + getRotation());
//                    previousPointRot = calculateRotation(new Point(realPoint.x - getPositionX(), realPoint.y - getPositionY(), 0));
//                    return true;
//                }
//
//                @Override
//                public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
//                    // getPositionX and Y are of the ship
//
//                    // Adjust origin to ship so that we rotate with respect to the ship
//                    //      we don't project into the ship's coordinate system because if we are rotating
//                    //      the ship will rotate and our rotation we want to advance will now be 0 height
//                    //      There is probably a nice, fancy way to use that property but I am going to do it the straight forward way
//
//                    // Add to the ship's rotation, don't immediately set rotation to the finger position
//                    // Get absolute rotation
//                    float tempRotation = calculateRotation(new Point(realPoint.x - getPositionX(), realPoint.y - getPositionY(), 0));
//                    //
//                    float deltaRotation = tempRotation - previousPointRot;
////                    System.out.println("Ship| delta rotation: " + deltaRotation);
//
//                    setNextRoundRotation(getRotation() + deltaRotation);
//
////                    System.out.println("Ship| Rotation: " + getRotation());
//
//                    previousPointRot = tempRotation;
//
//                    return true;
//                }
//
//                @Override
//                public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
//                    return true;
//                }
//            });

//            Node rotGfx = new Node("rotgfx");
//            rotGfx.baseShape = new RectSprite(rotGfx.getName());
//            ((RectSprite) rotGfx.baseShape).textureRef = 7; // refers to GLES20.GL_TEXTURE0. 1 would refer to TEXTURE1
//            rotGfx.material = MyGLRenderer.texturedMat;
//
//            Node powGfx = new Node("powgfx");
//            powGfx.baseShape = new RectSprite(powGfx.getName());
//            ((RectSprite) powGfx.baseShape).textureRef = 8; // refers to GLES20.GL_TEXTURE0. 1 would refer to TEXTURE1
//            powGfx.material = MyGLRenderer.texturedMat;


            getMenu().addChild(circleNode, false);

            getMenu().setPositionZ(-5);
            getMenu().setScale(200);

            getMenu().addTouchListener(new TouchListener() {
                int changePowerRotation = 0; // beyond -3 = power, beyond 3 = rotation
                Point previousPoint = new Point();

                @Override
                public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
//                    System.out.println("S| ship menu onDown");
                    previousPointPow = calculatePower(realPoint);
                    previousPointRot = calculateRotation(realPoint);
                    changePowerRotation = 0;
                    previousPoint.set(realPoint.x, realPoint.y, realPoint.z);
                    return false;
                }

                @Override
                public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                    System.out.println("S| changePowRot: " + changePowerRotation);
                    // Change Power
                    if(changePowerRotation < -1) {
                        System.out.println("Ship| currentLaserPower: " + currentLaserPower);

                        float tempPower = calculatePower(realPoint);

                        float deltaPower = (tempPower - previousPointPow) / 10;

                        setNextRoundLaserPower(getCurrentLaserPower() + deltaPower);

                        ((Circle) circleNode.baseShape).setNumSegmentsVisible(getNextRoundLaserPower() * 2);

                        previousPointPow = tempPower;

                        circleNode.updateMatrix();

                        return true;
                    } else if (changePowerRotation > 1) { // Change rotation
//                         getPositionX and Y are of the ship

                        // Adjust origin to ship so that we rotate with respect to the ship
                        //      we don't project into the ship's coordinate system because if we are rotating
                        //      the ship will rotate and our rotation we want to advance will now be 0 height
                        //      There is probably a nice, fancy way to use that property but I am going to do it the straight forward way

                        // Add to the ship's rotation, don't immediately set rotation to the finger position
                        // Get absolute rotation
                        float tempRotation = calculateRotation(new Point(realPoint.x - getPositionX(), realPoint.y - getPositionY(), 0));
                        System.out.println("S| temp rot: " + tempRotation);
                        System.out.println("S| previous rot: " + previousPointRot);
                        float deltaRotation = tempRotation - previousPointRot;
                        System.out.println("S| change rot: " + deltaRotation);
                        setNextRoundRotation(getRotation() + deltaRotation);

                        previousPointRot = tempRotation;

                        return true;
                    } else {    // Figure out which the user wishes to change
                        System.out.println("S| change x, y: " + Math.abs(realPoint.x - previousPoint.x) + ", " + Math.abs(realPoint.y - previousPoint.y));
                        System.out.println("S| distance realPoint to prevPoint: " + realPoint.distance(previousPoint));
                        // If change of y > change of x rotate

                        if (realPoint.distance(previousPoint) > 1) { // get percent of screen not pixel values
                             if(Math.abs(realPoint.x - previousPoint.x) > Math.abs(realPoint.y - previousPoint.y)) {
                                changePowerRotation -= 2;
                            } else {
                                changePowerRotation += 2;
                            }
                        }
                        if(changePowerRotation > 1 || changePowerRotation < -1) {
                            previousPointPow = calculatePower(realPoint);
                            previousPointRot = calculateRotation(new Point(realPoint.x - getPositionX(), realPoint.y - getPositionY(), 0));
                            return true;
                        }
                    }

                    return true; //false
                }

                @Override
                public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                    return Math.abs(changePowerRotation) > 1;
                }
            });

        } else if(currentLaserShieldEngine == 1) {
            // ------ Shield ------
            final Node circleNode = new Node("Circle");
            circleNode.setPosition(getPosition());
            circleNode.setIgnoreTouch(true);
            circleNode.baseShape = new Circle("shield meter");
            circleNode.baseShape.color = new float[] { 0.1f, 0.1f, 0.8f, 1.0f };

            getMenu().addChild(circleNode, false);

            if(getMenu().isOpen()) {
                circleNode.setVisible(true);
                circleNode.updateMatrix();
            }

        } else if(currentLaserShieldEngine == 2) {
            // ------ Engine ------

            Node moveNode = new Node("Move");
            moveNode.baseShape = new Circle("move loc");
            moveNode.setScale(0.5f);
            moveNode.setPosition(getNextRoundLocation().x, getNextRoundLocation().y + 1.5f);

            final Point movePoint = new Point();
            final Vector moveVector = new Vector();

            TouchListener tl = new TouchListener() {
                @Override
                public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                    return true;
                }

                @Override
                public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                    movePoint.set(realPoint.x, realPoint.y, 0);
                    moveVector.set(getPosition(), movePoint);

                    if(movePoint.distance(getPosition()) > 7) {
                        movePoint.x = 7 * moveVector.x/moveVector.length() + getPositionX();
                        movePoint.y = 7 * moveVector.y/moveVector.length() + getPositionY();
                    }

                    sender.setPosition(movePoint);

                    setNextRoundRotation(calculateRotation(new Point(movePoint.x - getPositionX(), movePoint.y - getPositionY(), 0)) - 90);

                    setNextRoundLocation(movePoint);



                    return true;
                }

                @Override
                public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                    return false;
                }
            };

            moveNode.addTouchListener(tl);

            getMenu().addChild(moveNode, false);

        }

        if(getMenu().isOpen()) {
            getMenu().open(getPosition(), 1f);
        }
    }

    // Gets rotation with respect to rotation origin (counter-clock-wise from positive x axis?)
    private float calculateRotation(Point point)  {
        // We don't want a divide by 0 error
        if(point.x == 0) {
            point.x = 0.000001f;
        }

        //System.out.println("Ship| touch location: " + point.x + ", " + point.y);

        // Add to the ship's rotation, don't immediately set rotation to the finger position
        // Get absolute rotation
        return (float) Math.atan2(point.y, point.x) * 57.29578f;
    }

    private float calculatePower(Point point) {
         return (point.x - getPositionX()) / 10;
    }

    // call this if the userId != currentUserId
    public void createShipInfoMenu() {
        Menu shipMenu = new Menu("Enemy ship info", 0, 0);

        shipMenu.setVisible(false, true);
        shipMenu.setPositionZ(-10);

        setMenu(shipMenu);
    }

//    public float getRadius() {
//        return radius;
//    }
//
//    public void setRadius(float radius) {
//        this.radius = radius;
//    }

    public Node shieldCircle;

    public void createShield() {
        shieldCircle = new Node("Circle");
        shieldCircle.setIgnoreTouch(true);
        shieldCircle.baseShape = new Circle("simulation shield");
        shieldCircle.setPosition(getPosition());
        shieldCircle.baseShape.color = new float[] { 0.1f, 0.1f, 0.8f, 1.0f };
    }

    public void showShield() {
        if(shieldCircle == null) {
            Log.e("Ship", "shieldCircle is null");
            return;
        }
        if(!children.contains(shieldCircle)) {
            shieldCircle.setPosition(getPosition());
            addChild(shieldCircle);
        }

    }


    /*---------------------- Live data -----------------------
      This is what is currently showing
    */

    public float getCurrentLaserPower() {
        return currentLaserPower;
    }

    public void setCurrentLaserPower(float currentLaserPower) {
        if(currentLaserPower >= 0.5f ) {
            this.currentLaserPower = 0.5f;
        } else if(currentLaserPower <= 0){
            this.currentLaserPower = 0.0f;
        } else {
            this.currentLaserPower = currentLaserPower;
        }
    }

    public float getCurrentShieldPower() {
        return currentShieldPower;
    }

    public void setCurrentShieldPower(float currentShieldPower) {
        this.currentShieldPower = currentShieldPower;
    }

    public float getCurrentEnginePower() {
        return currentEnginePower;
    }

    public void setCurrentEnginePower(float currentEnginePower) {
        this.currentEnginePower = currentEnginePower;
    }

    public float getCurrentRotation() {
        return currentRotation;
    }

    public void setCurrentRotation(float currentRotation) {
        this.currentRotation = currentRotation;
        setRotation(currentRotation);
    }

    public Point getCurrentLocation() { return new Point(currentX, currentY); }

    public void setCurrentLocation(float currentX, float currentY) {
        this.currentX = currentX;
        this.currentY = currentY;
        setPosition(currentX, currentY);
    }

    public void setCurrentLocation(Point currentLocation) {
        setCurrentLocation(currentLocation.x, currentLocation.y);
    }

    public boolean isShipIsDead() {
        return currentIsDead;
    }

    public void setShipIsDead(boolean shipIsDead) {
        this.currentIsDead = shipIsDead;
        // Make this invisible if it is dead
        this.setVisible(!shipIsDead);
    }

    public void setColor(float[] color) {
        baseShape.color = color;
        if(laserEmitter != null) {
            laserEmitter.baseShape.color = color;
        }
        if(laserPath != null) {
            laserPath.tracker.baseShape.color = color;
            laserPath.trace.baseShape.color = color;
        }
        // if myship !null { set color }
    }

    public boolean isEnemy() {
        return isEnemy;
    }

    public void setEnemy(boolean enemy) {
        isEnemy = enemy;

        if(!enemy) {
            Node node = new Node("ship not enemy dot");
            node.baseShape = new Circle("shipIdDot");
            node.baseShape.color = baseShape.color;
            node.setRelativePos(true, Point.ZERO);
            node.setScale(0.125f);
            addChild(node);
        }
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        addChild(menu);
        this.menu = menu;
    }

    public int getCurrentLaserShieldEngine() {
        return currentLaserShieldEngine;
    }

    public void setCurrentLaserShieldEngine(int currentLaserShieldEngine) {
        this.currentLaserShieldEngine = currentLaserShieldEngine;
        updateShipMenu();
    }

    public boolean isMoving() {
        return isMoving;
    }

    public void setMoving(boolean moving) {
        isMoving = moving;
    }

    public float getVelocity() {
        return velocity;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }


    //--------------------- Simulation Data -----------------------------

    public float getPrevRoundLaserPower() {
        return prevRoundLaserPower;
    }

    public void setPrevRoundLaserPower(float prevRoundLaserPower) {
        this.prevRoundLaserPower = prevRoundLaserPower;
    }

    public float getPrevRoundShieldPower() {
        return prevRoundShieldPower;
    }

    public void setPrevRoundShieldPower(float prevRoundShieldPower) {
        this.prevRoundShieldPower = prevRoundShieldPower;
    }

    public float getPrevRoundEnginePower() {
        return prevRoundEnginePower;
    }

    public void setPrevRoundEnginePower(float prevRoundEnginePower) {
        this.prevRoundEnginePower = prevRoundEnginePower;
    }

    public float getPrevRoundRotation() {
        return prevRoundRotation;
    }

    public void setPrevRoundRotation(float prevRoundRotation) {
        this.prevRoundRotation = prevRoundRotation;
    }

    public Point getPrevRoundLocation() { return new Point(prevRoundX, prevRoundY); }

    public void setPrevRoundLocation(float prevRoundX, float prevRoundY) {
        this.prevRoundX = prevRoundX;
        this.prevRoundY = prevRoundY;
    }

    public void setPrevRoundLocation(Point prevLocation) {
        setPrevRoundLocation(prevLocation.x, prevLocation.y);
    }

    public void setPrevRoundX(float prevRoundX) {
        setPrevRoundLocation(prevRoundX, getPrevRoundLocation().y);
    }

    public void setPrevRoundY(float prevRoundY) {
        setPrevRoundLocation(getPrevRoundLocation().x, prevRoundY);
    }

    public boolean isPrevRoundDead() {
        return prevRoundDead;
    }

    public void setPrevRoundDead(boolean prevRoundDead) {
        this.prevRoundDead = prevRoundDead;
    }

    public int getPrevRoundLaserShieldEngine() {
        return prevRoundLaserShieldEngine;
    }

    public void setPrevRoundLaserShieldEngine(int prevRoundLaserShieldEngine) {
        this.prevRoundLaserShieldEngine = prevRoundLaserShieldEngine;
    }



    //-------------------------- Future Data ------------------------

    public float getNextRoundLaserPower() {
        return nextRoundLaserPower;
    }

    public void setNextRoundLaserPower(float nextRoundLaserPower) {
        this.nextRoundLaserPower = nextRoundLaserPower;
        setCurrentLaserPower(nextRoundLaserPower);
    }

    public float getNextRoundShieldPower() {
        return nextRoundShieldPower;
    }

    public void setNextRoundShieldPower(float nextRoundShieldPower) {
        this.nextRoundShieldPower = nextRoundShieldPower;
        setCurrentShieldPower(nextRoundShieldPower);
    }

    public float getNextRoundEnginePower() {
        return nextRoundEnginePower;
    }

    public void setNextRoundEnginePower(float nextRoundEnginePower) {
        this.nextRoundEnginePower = nextRoundEnginePower;
        setCurrentEnginePower(nextRoundEnginePower);
    }

    public float getNextRoundRotation() {
        return nextRoundRotation;
    }

    public void setNextRoundRotation(float nextRoundRotation) {
        this.nextRoundRotation = nextRoundRotation;
        setCurrentRotation(nextRoundRotation);
    }

    public Point getNextRoundLocation() { return new Point(nextRoundEndX, nextRoundEndY); }

    public Point getNextRoundStartLocation() { return new Point(nextRoundStartX, nextRoundStartY); }

    public void setNextRoundLocation(float nextRoundEndX, float nextRoundEndY) {
        this.nextRoundEndX = nextRoundEndX;
        this.nextRoundEndY = nextRoundEndY;
        //setCurrentLocation(nextRoundX, nextRoundY);
    }

    public void setNextRoundStartLocation(float nextRoundStartX, float nextRoundStartY) {
        this.nextRoundStartX = nextRoundStartX;
        this.nextRoundStartY = nextRoundStartY;
        //setCurrentLocation(nextRoundX, nextRoundY);
    }

    public void setNextRoundLocation(Point nextLocation) {
        setNextRoundLocation(nextLocation.x, nextLocation.y);
    }

    public void setNextRoundStartLocation(Point nextStartLocation) {
        setNextRoundStartLocation(nextStartLocation.x, nextStartLocation.y);
    }

    public void setNextRoundX(float nextRoundX) {
        setNextRoundLocation(nextRoundX, getNextRoundLocation().y);
    }

    public void setNextRoundY(float nextRoundY) {
        setNextRoundLocation(getNextRoundLocation().x, nextRoundY);
    }

    public void setNextRoundStartX(float nextRoundX) {
        setNextRoundStartLocation(nextRoundX, getNextRoundStartLocation().y);
    }

    public void setNextRoundStartY(float nextRoundY) {
        setNextRoundStartLocation(getNextRoundStartLocation().x, nextRoundY);
    }



    public boolean isNextRoundDead() {
        return nextRoundDead;
    }

    public void setNextRoundDead(boolean nextRoundDead) {
        this.nextRoundDead = nextRoundDead;
        setShipIsDead(nextRoundDead);
    }

    public int getNextRoundLaserShieldEngine() {
        return nextRoundLaserShieldEngine;
    }

    public void setNextRoundLaserShieldEngine(int nextRoundLaserShieldEngine) {
        this.nextRoundLaserShieldEngine = nextRoundLaserShieldEngine;
        setCurrentLaserShieldEngine(nextRoundLaserShieldEngine);
    }




    public void moveFutToPrev() {
        setPrevRoundLaserPower(getNextRoundLaserPower());
        setPrevRoundShieldPower(getNextRoundShieldPower());
        setPrevRoundEnginePower(getNextRoundEnginePower());

        setPrevRoundRotation(getNextRoundRotation());

        setPrevRoundLocation(getNextRoundStartLocation());
        setNextRoundStartLocation(getNextRoundLocation());


        setPrevRoundDead(isNextRoundDead());

        setPrevRoundLaserShieldEngine(getNextRoundLaserShieldEngine());
    }

    public void setVisualDefaults() {
        setCurrentLaserPower(getPrevRoundLaserPower());
        setCurrentShieldPower(getPrevRoundShieldPower());
        setCurrentEnginePower(getPrevRoundEnginePower());

        setCurrentRotation(getPrevRoundRotation());
        setCurrentLocation(getPrevRoundLocation());

        setShipIsDead(isPrevRoundDead());

        setCurrentLaserShieldEngine(getPrevRoundLaserShieldEngine());

    }







}
