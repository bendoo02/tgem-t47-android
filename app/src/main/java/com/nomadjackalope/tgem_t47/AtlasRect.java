package com.nomadjackalope.tgem_t47;

import android.opengl.GLES20;

/**
 * Created by benjamin on 5/15/17.
 */
public class AtlasRect extends BatchTextRectangle {

    TextureRegion texReg;


    public AtlasRect(int maxSprites, TextureRegion textureRegion) {
        texReg = textureRegion;

        color = new float[]{0.988f, 0.988f, 0.384f, 1.0f};

        this.vertexBuffer = new float[maxSprites * VERTICES_PER_SPRITE * VERTEX_SIZE];  // Create Vertex Buffer
        this.vertices = new Vertices(maxSprites * VERTICES_PER_SPRITE, maxSprites * INDICES_PER_SPRITE);  // Create Rendering Vertices
        this.bufferIndex = 0;                           // Reset Buffer Index
        this.maxSprites = maxSprites;                   // Save Maximum Sprites
        this.numSprites = 0;                            // Clear Sprite Counter

        short[] indices = new short[maxSprites * INDICES_PER_SPRITE];  // Create Temp Index Buffer
        int len = indices.length;                       // Get Index Buffer Length
        short j = 0;                                    // Counter
        for ( int i = 0; i < len; i+= INDICES_PER_SPRITE, j += VERTICES_PER_SPRITE )  {  // FOR Each Index Set (Per Sprite)
            indices[i + 0] = (short)( j + 0 );           // Calculate Index 0
            indices[i + 1] = (short)( j + 1 );           // Calculate Index 1
            indices[i + 2] = (short)( j + 2 );           // Calculate Index 2
            indices[i + 3] = (short)( j + 2 );           // Calculate Index 3
            indices[i + 4] = (short)( j + 3 );           // Calculate Index 4
            indices[i + 5] = (short)( j + 0 );           // Calculate Index 5
        }
        vertices.setIndices( indices, 0, len );         // Set Index Buffer for Rendering
    }


    public void draw(TextNode node, float[] vMatrix, float[] pMatrix, Material mat, Point position, float scaleX, float scaleY, float rotation) {
        // enable texture + alpha blending
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        begin(mat);

        updateMatrix(vMatrix, pMatrix, position, scaleX, scaleY, rotation);

        drawSprite(position.x, position.y, scaleX, scaleY, texReg, modelMatrix);

        end();

        GLES20.glDisable(GLES20.GL_BLEND);
    }

    protected void begin(Material mat) {
        GLES20.glUseProgram(mat.getProgram()); // specify the program to use

        // Get Handles to all the necessary variables
        colorHandle = GLES20.glGetUniformLocation(mat.getProgram(), "u_Color");
        textureHandle = GLES20.glGetUniformLocation(mat.getProgram(), "u_Texture");
        mVPMatrixHandle = GLES20.glGetUniformLocation(mat.getProgram(), "u_MVPMatrix");

        GLES20.glUniform4fv(colorHandle, 1, color , 0);
        GLES20.glEnableVertexAttribArray(colorHandle);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE4);  // Set the active texture unit to texture unit 0

        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0
        GLES20.glUniform1i(textureHandle, 4);

        numSprites = 0;
        bufferIndex = 0;

    }

}
