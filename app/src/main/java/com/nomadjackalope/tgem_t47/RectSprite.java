package com.nomadjackalope.tgem_t47;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL;

/**
 * Created by benjamin on 1/8/17.
 *
 * Rectangle shape used for drawing
 * Base shape used in Node
 */
public class RectSprite extends Rectangle {

    static private FloatBuffer textureBuffer;

    private int textureHandle;
    private int textureCoordHandle;

    final static int TEXCOORDS_PER_VERTEX = 2;

    static float[] textureCoords = {
            0.0f, 1.0f,
            1.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f
    };

//    0.0f, 0.0f,
//    0.0f, 1.0f,
//    1.0f, 0.0f,
//    1.0f, 1.0f

    private Bitmap textureBitmap;

    public int textureRef = 0;

    private int[] textures;


    public RectSprite(String name) {
        this.name = name;
        init();
    }

    @Override
    void init() {
        // initialize vertex byte buffer for shape coords
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // number of coord vals * 4 bytes per float
                rectCoords.length * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(rectCoords);
        // set the buffer to read the first coordinate
        vertexBuffer.position(0); // or vertexBuffer.rewind(); either works

        ByteBuffer tb = ByteBuffer.allocateDirect(
                // number of coord vals * 4 bytes per float
                textureCoords.length * 4);
        tb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        textureBuffer = tb.asFloatBuffer();
        textureBuffer.put(textureCoords);
        // set the buffer to read the first coordinate
        textureBuffer.position(0);


    }

    @Override
    public void draw(float[] vMatrix, float[] pMatrix, Material mat, Point position, float scaleX, float scaleY, float rotation) {
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mat.getProgram());

        // Get int that refers to variable vPosition and set it to coordinates matrix
        positionHandle = GLES20.glGetAttribLocation(mat.getProgram(), "vPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        // get handle to fragment shader's vColor member and set color for drawing the triangle
        colorHandle = GLES20.glGetUniformLocation(mat.getProgram(), "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);

        textureCoordHandle = GLES20.glGetAttribLocation(mat.getProgram(), "texCoordIn");
        GLES20.glEnableVertexAttribArray(textureCoordHandle);
        GLES20.glVertexAttribPointer(textureCoordHandle, 2,
                GLES20.GL_FLOAT, false,
                textureCoords.length, textureBuffer);

        textureHandle = GLES20.glGetUniformLocation(mat.getProgram(), "texture");
        GLES20.glUniform1i(textureHandle, textureRef);


        // Matrix math ----------------------------------------
        updateMatrix(vMatrix, pMatrix, position, scaleX, scaleY, rotation);


        // get handle to shape's transformation matrix and pass the projectionView transformation to the shader
        mVPMatrixHandle = GLES20.glGetUniformLocation(mat.getProgram(), "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mVPMatrixHandle, 1, false, mVPMatrix, 0);

        // Draw the rectangle
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, vertexCount);


        // Disable vertex array // Don't know why
        GLES20.glDisableVertexAttribArray(positionHandle);
        GLES20.glDisableVertexAttribArray(textureHandle);

        GLES20.glDisable(GLES20.GL_BLEND);

    }


}
