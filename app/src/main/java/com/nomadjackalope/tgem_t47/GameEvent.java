package com.nomadjackalope.tgem_t47;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by benjamin on 6/29/17.
 */
public interface GameEvent  {
    void run(Node n, Object arg);
}
