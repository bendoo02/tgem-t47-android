package com.nomadjackalope.tgem_t47;

import android.opengl.GLES20;

/**
 * Created by benjamin on 5/16/17.
 */
public class JaserSprite extends Rectangle {

    private final static int SPRITES_PER_BATCH = 1000;
    private float[] uMVPMatrices = new float[SPRITES_PER_BATCH*16];

    final static int VERTEX_SIZE = 5;                  // Vertex Size (in Components) ie. (X,Y,U,V,M), M is MVP matrix index
    final static int VERTICES_PER_SPRITE = 4;          // Vertices Per Sprite
    final static int INDICES_PER_SPRITE = 6;           // Indices Per Sprite

    Vertices vertices;
    float[] vertexBuffer;
    int bufferIndex;
    int maxSprites = SPRITES_PER_BATCH;
    int numSprites;

    int textureHandle;

    public JaserSprite() {
        this.vertexBuffer = new float[maxSprites * VERTICES_PER_SPRITE * VERTEX_SIZE];
        this.vertices = new Vertices(maxSprites * VERTICES_PER_SPRITE, maxSprites * INDICES_PER_SPRITE);

        bufferIndex = 0;
        maxSprites = maxSprites;
        numSprites = numSprites;

        // Create Temp index buffer
        short[] indices = new short[maxSprites * INDICES_PER_SPRITE];
        int len = indices.length;
        short j = 0;
        for ( int i = 0; i < len; i+= INDICES_PER_SPRITE, j += VERTICES_PER_SPRITE )  {  // FOR Each Index Set (Per Sprite)
            indices[i + 0] = (short)( j + 0 );           // Calculate Index 0
            indices[i + 1] = (short)( j + 1 );           // Calculate Index 1
            indices[i + 2] = (short)( j + 2 );           // Calculate Index 2
            indices[i + 3] = (short)( j + 2 );           // Calculate Index 3
            indices[i + 4] = (short)( j + 3 );           // Calculate Index 4
            indices[i + 5] = (short)( j + 0 );           // Calculate Index 5
        }
        vertices.setIndices( indices, 0, len );         // Set Index Buffer for Rendering
    }

    public void draw(ParticleEmitter node, float[] vMatrix, float[] pMatrix, Material mat, Point position, float scaleX, float scaleY, float rotation) {
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE);//GLES20.GL_ONE_MINUS_SRC_ALPHA);

        begin(mat);

        updateMatrix(vMatrix, pMatrix, position, scaleX, scaleY, rotation);

        drawSelf(node);

        end();

        GLES20.glDisable(GLES20.GL_BLEND);
    }



    protected void begin(Material mat) {
        GLES20.glUseProgram(mat.getProgram());

        // Get Handles to all the necessary variables
        colorHandle = GLES20.glGetUniformLocation(mat.getProgram(), "u_Color");
        textureHandle = GLES20.glGetUniformLocation(mat.getProgram(), "u_Texture");
        mVPMatrixHandle = GLES20.glGetUniformLocation(mat.getProgram(), "u_MVPMatrix");

        GLES20.glUniform4fv(colorHandle, 1, color , 0);
        GLES20.glEnableVertexAttribArray(colorHandle);

//        GLES20.glActiveTexture(GLES20.GL_TEXTURE3);  // Set the active texture unit to texture unit 0

//        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, fontObject.getTextureId()); // Bind the texture to this unit
//        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
//        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0
        GLES20.glUniform1i(textureHandle, 1);

        numSprites = 0;
        bufferIndex = 0;
    }

    Particle curParticle;
    protected void drawSelf(ParticleEmitter node) {
        for (int i = 0; i < node.particles.size(); i++) {
            curParticle = node.particles.get(i);
            addSprite(curParticle.position.x,
                    curParticle.position.y,
                    curParticle.size.x,
                    curParticle.size.y);
        }
    }

    protected void end() {
        if(numSprites > 0) {
            // bind MVP matrix to shader
            GLES20.glUniformMatrix4fv(mVPMatrixHandle, 1, false, mVPMatrix, 0);
            //GLES20.glEnableVertexAttribArray(mVPMatrixHandle);

            vertices.setVertices(vertexBuffer, 0, bufferIndex);
            vertices.bind();
            vertices.draw(GLES20.GL_TRIANGLES, 0, numSprites * INDICES_PER_SPRITE);
            vertices.unbind();
        }
    }

    protected void addSprite(float x, float y, float width, float height) {
        if(numSprites == maxSprites) {
            end();
            // NOTE: leave current texture bound!!
            numSprites = 0;
            bufferIndex = 0;
        }

        float halfWidth = width / 2.0f;
        float halfHeight = height / 2.0f;
        float x1 = x - halfWidth;
        float y1 = y - halfHeight;
        float x2 = x + halfWidth;
        float y2 = y + halfHeight;

        vertexBuffer[bufferIndex++] = x1;               // Add X for Vertex 0
        vertexBuffer[bufferIndex++] = y1;               // Add Y for Vertex 0
        vertexBuffer[bufferIndex++] = 0;        // Add U for Vertex 0
        vertexBuffer[bufferIndex++] = 0;        // Add V for Vertex 0
        vertexBuffer[bufferIndex++] = numSprites;

        vertexBuffer[bufferIndex++] = x2;               // Add X for Vertex 1
        vertexBuffer[bufferIndex++] = y1;               // Add Y for Vertex 1
        vertexBuffer[bufferIndex++] = 1;        // Add U for Vertex 0
        vertexBuffer[bufferIndex++] = 0;        // Add V for Vertex 0
        vertexBuffer[bufferIndex++] = numSprites;

        vertexBuffer[bufferIndex++] = x2;               // Add X for Vertex 2
        vertexBuffer[bufferIndex++] = y2;               // Add Y for Vertex 2
        vertexBuffer[bufferIndex++] = 1;        // Add U for Vertex 0
        vertexBuffer[bufferIndex++] = 1;        // Add V for Vertex 0
        vertexBuffer[bufferIndex++] = numSprites;

        vertexBuffer[bufferIndex++] = x1;               // Add X for Vertex 3
        vertexBuffer[bufferIndex++] = y2;               // Add Y for Vertex 3
        vertexBuffer[bufferIndex++] = 0;        // Add U for Vertex 0
        vertexBuffer[bufferIndex++] = 1;        // Add V for Vertex 0
        vertexBuffer[bufferIndex++] = numSprites;

        for (int i = 0; i < 16; i++) {
            uMVPMatrices[numSprites*16+i] = mVPMatrix[i];
        }

        numSprites++;
    }


}
