package com.nomadjackalope.tgem_t47;

/**
 * Created by benjamin on 4/3/17.
 */
public class ActionScale extends Action {

    float initialScale;
    float finalScale;
    float tempScale;

    public ActionScale(float initialScale, float finalScale, float millis, Node target) {
        this.initialScale = initialScale;
        this.finalScale = finalScale;

        tempScale = 0.0f;


        init(target, millis);
    }

    @Override
    protected void updateSubClass() {

        tempScale = initialScale + ((finalScale - initialScale) * getPercentDone());

        //System.out.println("AS| % done: " + getPercentDone());
        //System.out.println("AS| tempScale: " + tempScale);

        if(getTarget() != null) {
            getTarget().setScale(tempScale);
            //System.out.println("AS| scale: " + getTarget().getScaleX());
        }
    }
}
