package com.nomadjackalope.tgem_t47;

import android.view.MotionEvent;

import java.util.ArrayList;

/**
 * Created by benjamin on 1/23/17.
 *
 * This class is used to hold the displayed info
 * It is not used to manage its own opening and closing. That is the MenuManager
 *
 * This menu will inherit its parent //??hmm
 */
public class Menu extends Node {

    public static Menu activeMenu;

    private boolean isOpen = false;

    private ArrayList<Boolean> isMenuItemList = new ArrayList<>();

    public Menu(String name, float x, float y) {
        this.setName(name);
        this.setPositionX(x);
        this.setPositionY(y);
        setIgnoreTouch(true);

        setVisible(false);
        setTouchEvenIfInvisible(true);
    }

    public Node addMenuItem(String name) {

        Node node = new Node(name);

        // Add node to menu
        addChild(node);
        return node;
    }

    // Returns number of menu items removed
    public int removeAllMenuItems() {
        int numRemoved = 0;

        for (int i = 0; i < isMenuItemList.size(); i++) {
            if(isMenuItemList.get(i)) {
                removeChild(i);
                i--;
                numRemoved++;
            }
        }

        return numRemoved;
    }

    public void layoutMenu(Point pos, float radius) {

        setPosition(pos.x, pos.y);

        float offset = radius + 1.0f;

        int childrenSize = getChildren().size();

        int menuItem = 0;

        for (int i = 0; i < childrenSize; i++) {

            // if the child is a menu item
            if (isMenuItemList.get(i)) {

                // Set position of button based on number of elements in menu
                switch (menuItem) {
                    case 0:
                        getChild(i).setPositionX(pos.x);
                        getChild(i).setPositionY(pos.y + offset);
                        try {
                            ((TextNode)getChild(i)).setAlign(TextNode.CENTER);
                        } catch (ClassCastException e) {
                            System.out.println("M| not a text node 0");
                        }
                        break;
                    case 1:
                        getChild(i).setPositionX(pos.x + offset);
                        getChild(i).setPositionY(pos.y);
                        try {
                            ((TextNode)getChild(i)).setAlign(TextNode.LEFT);
                        } catch (ClassCastException e) {
                            System.out.println("M| not a text node 1");
                        }
                        break;
                    case 2:
                        getChild(i).setPositionX(pos.x);
                        getChild(i).setPositionY(pos.y - offset);
                        try {
                            ((TextNode)getChild(i)).setAlign(TextNode.CENTER);
                        } catch (ClassCastException e) {
                            System.out.println("M| not a text node 2");
                        }
                        break;
                    case 3:
                        getChild(i).setPositionX(pos.x - offset);
                        getChild(i).setPositionY(pos.y);
                        try {
                            ((TextNode)getChild(i)).setAlign(TextNode.RIGHT);
                        } catch (ClassCastException e) {
                            System.out.println("M| not a text node 3");
                        }
                        break;
                }

                menuItem++;
            }
        }
    }

    public void open(Point point, float radius) {
        if(activeMenu != null) {
//            if(activeMenu.getPosition().distance(this.getPosition()) < 4) {
//                return;
//            }
            activeMenu.close();
        }

        layoutMenu(point, radius);

        setAllChildrenVisibility(true);

//        for (int i = 0; i < children.size(); i++) {
//
//            children.get(i).removeAllActions();
//            children.get(i).addAction(new ActionScale(0.0f, children.get(i).getScaleX(), 120f, children.get(i)));
//            children.get(i).setIgnoreTouch(false);
//        }

//        MyGLSurfaceView.activeAction = true;

        isOpen = true;
        activeMenu = this;
        setIgnoreTouch(false);
    }

    public void close() {
        setAllChildrenVisibility(false);

        for (int i = 0; i < children.size(); i++) {
//            children.get(i).removeAllActions();
//
////            for (Action action : children.get(i).getActions()) {
////                action.forceFinish();
////            }
////
////            children.get(i).removeAllActions();
//
//            final Node child = children.get(i);
//            final float initScale = child.getScaleX();
//
//            Action scale = new ActionScale(children.get(i).getScaleX(), 0.0f, 80f, children.get(i));
//            scale.setRunAtPercent(new ActionRunnable() {
//                @Override
//                public boolean run() {
//                    child.setScaleX(initScale);
//                    child.setVisible(false);
//                    return false;
//                }
//            }, 1.0f);
//
//            children.get(i).addAction(scale);
//            children.get(i).setIgnoreTouch(true);
        }

        isOpen = false;
        activeMenu = null;
        setIgnoreTouch(true);
    }

    // Returns isOpen
    public boolean toggle(Point point, float radius) {
        if(!isOpen) {
            open(point, radius);
        } else {
            close();
        }

        return isOpen();
    }

    // Override this to automatically set the new child's visibility to this menu's visibility
    @Override
    public Node addChild(Node child) {
        addChild(child, true);
        return child;
    }


    public Node addChild(Node child, boolean isMenuItem) {
        child.setVisible(false, true);
        child.setParent(this);
        children.add(child);

        isMenuItemList.add(isMenuItem);

        return child;
    }

    // Returns if it worked ~roughly
    public boolean removeChild(int index) {

        boolean menuR = isMenuItemList.remove(index);
        boolean childR = (children.remove(index) != null);

        return menuR && childR;
    }

    @Override
    public void removeAllChildren() {
        super.removeAllChildren();

        isMenuItemList.clear();
    }

    public boolean isOpen() {
        return isOpen;//return isOpen != CLOSED;
    }
}
