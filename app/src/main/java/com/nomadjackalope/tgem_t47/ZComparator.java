package com.nomadjackalope.tgem_t47;

import java.util.Comparator;

/**
 * Created by benjamin on 1/24/17.
 */

public class ZComparator implements Comparator<Node> {
    @Override
    public int compare(Node o1, Node o2) {
        if(o1.getPositionZ() > o2.getPositionZ()) {
            return -1;
        } else if(o1.getPositionZ() == o2.getPositionZ()) {
            return 0;
        } else {
            return 1;
        }
    }
}
