package com.nomadjackalope.tgem_t47;

import android.opengl.GLES20;

/**
 * Created by benjamin on 1/9/17.
 */
public class Material {

    private String vertexShaderCode;

    private String fragmentShaderCode;

    private int program = -1;

    private int vertexShader = -1;
    private int fragmentShader = -1;

    public MyGLRenderer renderer;

    public Material(String vShader, String fShader, MyGLRenderer renderer) {

        vertexShaderCode = vShader;
        fragmentShaderCode = fShader;

        this.renderer = renderer;

    }

    // Call this from in an OpenGL Context so some callback from OpenGL, exmpl: MyGLRenderer.onSurfaceCreated
    public void loadShaders() {
        loadShaders(new AttribVariable[0]);
    }

    public void loadShaders(AttribVariable[] variables) {
        vertexShader = createShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        fragmentShader = createShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

        // create empty OpenGL ES Program and add shaders to it
        program = GLES20.glCreateProgram();
        GLES20.glAttachShader(program, vertexShader);
        GLES20.glAttachShader(program, fragmentShader);

        for (AttribVariable var : variables) {
            GLES20.glBindAttribLocation(program, var.getHandle(), var.getName());
        }

        // Gets log of programs
        //String programLinkLog = GLES20.glGetProgramInfoLog(program);

        // creates OpenGL ES program executables
        GLES20.glLinkProgram(program);
    }

    public static int createShader(int type, String shaderCode){

        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        //String shaderCompileLog = GLES20.glGetShaderInfoLog(shader);

        return shader;
    }

    public void dispose() {
        GLES20.glDetachShader(program, vertexShader);
        GLES20.glDetachShader(program, fragmentShader);

        GLES20.glDeleteShader(vertexShader);
        GLES20.glDeleteShader(fragmentShader);

        GLES20.glDeleteProgram(program);

    }

    public int getProgram() {
        return program;
    }


}
