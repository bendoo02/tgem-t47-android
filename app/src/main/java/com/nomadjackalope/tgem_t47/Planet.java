package com.nomadjackalope.tgem_t47;

import android.content.Context;

import com.google.android.gms.games.Player;

/**
 * Created by benjamin on 1/20/17.
 *
 * Planet object
 */
public class Planet extends Node {

    private float gravity;
    float mass = 100f; // kg

    public Planet() {

        init("Planet", 0, 0);
    }

    public Planet(String name, float x, float y) {
        init(name, x, y);
    }

    void init(String name, float x, float y) {
        this.setPositionX(x);
        this.setPositionY(y);
        this.setName(name);

        baseShape = new RectSprite(name);
        ((RectSprite) baseShape).textureRef = 0; // refers to GLES20.GL_TEXTURE0. 1 would refer to TEXTURE1

        baseShape.color = new float[] {1.0f, 1.0f, 1.0f, 1.0f};

        gravity = 50f;



        material = MyGLRenderer.texturedMat;
    }

    public float getRadius() {
        return getScaleX() / 2;
    }

    public void setRadius(float radius) {
        setScaleX(radius * 2);
        setScaleY(radius * 2);
    }

    public float getGravity() {
        return gravity;
    }

    public void setGravity(float gravity) {
        this.gravity = gravity;
    }

    public float getMass() {
        return mass;
    }

    public void setMass(float mass) {
        this.mass = mass;
    }
}
