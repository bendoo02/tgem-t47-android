package com.nomadjackalope.tgem_t47;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by benjamin on 1/8/17.
 *
 * Rectangle shape used for drawing
 * Base shape used in Node
 */
public class Rectangle {

    String name = "";
    String id = "";

    static FloatBuffer vertexBuffer;
    //private FloatBuffer colorBuffer;

    int positionHandle;
    int colorHandle;

    float layer;

    // Use this to extend the touchable area of an object
    float touchScale = 1.0f;

    // Use to access and set the view/projection transformation
    int mVPMatrixHandle;

    volatile float[] modelMatrix = new float[16];
    float[] mPMatrix = new float[16];
    float[] mVMatrix = new float[16];
    float[] mVPMatrix = new float[16];

    float[] combMatrix = new float[rectCoordsF.length];

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    static float rectCoords[] = {
            -0.5f, -0.5f, 0.0f,
            0.5f, -0.5f, 0.0f,
            -0.5f, 0.5f, 0.0f,
            0.5f, 0.5f, 0.0f
    };
//
//    static float rectCoords[] = {
//        -1, -1, 0,
//        1, -1, 0,
//        -1, 1, 0,
//        1, 1, 0
//    };

    static float rectCoordsF[] = {
            -0.5f, -0.5f, 0.0f, 1.0f,
            0.5f, -0.5f, 0.0f, 1.0f,
            -0.5f, 0.5f, 0.0f, 1.0f,

            -0.5f, 0.5f, 0.0f, 1.0f,
            0.5f, -0.5f, 0.0f, 1.0f,
            0.5f, 0.5f, 0.0f, 1.0f
    };

    final int vertexCount = rectCoords.length / COORDS_PER_VERTEX;
    final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex

    // Set color with rgba
    float color[] = { 0.63f, 0.42f, 0.22f, 1.0f };


    public Rectangle() {
        init();
    }

    public Rectangle(String name) {
        this.name = name;
        init();
    }

    void init() {
        // initialize vertex byte buffer for shape coords
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // number of coord vals * 4 bytes per float
                rectCoords.length * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(rectCoords);
        // set the buffer to read the first coordinate
        vertexBuffer.position(0); // or vertexBuffer.rewind(); either works
    }

    private float tempV[] = new float[4];
    private float tempVIn[] = new float[4];
    public void updateMatrix(float[] vMatrix, float[] pMatrix, Point position, float scaleX, float scaleY, float rotation) {
        // Update layer
        layer = position.z;

        // Matrix math ----------------------------------------
        // Draw the triangle facing straight on.
        Matrix.setIdentityM(modelMatrix, 0);
//        Matrix.scaleM(modelMatrix, 0, scaleX, scaleY, 1.0f);
//        Matrix.rotateM(modelMatrix, 0, rotation, 0.0f, 0.0f, 1.0f);
//        Matrix.translateM(modelMatrix, 0, position.x, position.y, position.z);

        // Old order - correct order - don't ask me
        Matrix.translateM(modelMatrix, 0, position.x, position.y, position.z);
        Matrix.rotateM(modelMatrix, 0, rotation, 0.0f, 0.0f, 1.0f);
        Matrix.scaleM(modelMatrix, 0, scaleX, scaleY, 1.0f);

        if(modelMatrix[0] == 0.0f) {
            //Log.i("R", "stop");
        }

        // Don't use these
//        Matrix.multiplyMM(mVMatrix, 0, modelMatrix, 0, vMatrix, 0);
//
//        Matrix.multiplyMM(mVPMatrix, 0, mVMatrix, 0, pMatrix, 0);

//        Matrix.multiplyMM(mVMatrix, 0, vMatrix, 0, modelMatrix, 0);
//
//        Matrix.multiplyMM(mVPMatrix, 0, pMatrix, 0, mVMatrix, 0);


//        Matrix.multiplyMM(mVPMatrix, 0, modelMatrix, 0, pMatrix, 0);


        // This one works
        Matrix.multiplyMM(mVMatrix, 0, pMatrix, 0, vMatrix, 0);

        Matrix.multiplyMM(mVPMatrix, 0, mVMatrix, 0, modelMatrix, 0);


//        System.out.println("R| " + name + " Scale, translate: " + mVPMatrix[0] + "," + mVPMatrix[5] + "," + mVPMatrix[10] + " | " + mVPMatrix[12] + "," + mVPMatrix[13]);


        for (int i = 0; i < rectCoordsF.length / 4; i++) {

            for (int j = 0; j < tempVIn.length; j++) {
                tempVIn[j] = rectCoordsF[j+(4*i)];
            }

            Matrix.multiplyMV(tempV, 0, mVPMatrix, 0, tempVIn, 0);

            for (int j = 0; j < tempV.length; j++) {
                combMatrix[j+(4*i)] = tempV[j];
            }
        }

    }

    public void draw(float[] vMatrix, float[] pMatrix, Material mat, Point position, float scaleX, float scaleY, float rotation) {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mat.getProgram());

        // Get int that refers to variable vPosition and set it to coordinates matrix
        positionHandle = GLES20.glGetAttribLocation(mat.getProgram(), "vPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        // get handle to fragment shader's vColor member and set color for drawing the triangle
        colorHandle = GLES20.glGetUniformLocation(mat.getProgram(), "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);


        //updateMatrix(vMatrix, pMatrix, position, scaleX, scaleY, rotation);


        // get handle to shape's transformation matrix and pass the projectionView transformation to the shader
        mVPMatrixHandle = GLES20.glGetUniformLocation(mat.getProgram(), "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mVPMatrixHandle, 1, false, mVPMatrix, 0);

        //GLES20.glLineWidth(3.0f);

        // Draw the rectangle
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, vertexCount);

        // Disable vertex array // Don't know why
        GLES20.glDisableVertexAttribArray(positionHandle);

    }

    boolean tempMatRet;

    float[] tempModelBefore;
    float[] tempModelAfter;
    float[] lockedModelMatrix;
    float[] inverted = new float[16];

    public boolean isTouched(Point position, boolean isSim) {
        // Convert into modelView space from projection space
//        if(name.equals("Ship sprite")) {
//            System.out.print("R| model matrix before: ");
//            for (float x : modelMatrix) {
//                System.out.print(x);
//                System.out.print(", ");
//            }
//            System.out.println();
//        }
        tempModelBefore = modelMatrix;
        lockedModelMatrix = modelMatrix;


        tempMatRet = Matrix.invertM(inverted, 0, lockedModelMatrix, 0);
        //System.out.println("R| matrix was inverted: " + tempMatRet);

        //TODO BUG: Modelmatrix sometimes comes in filled with zeros
        // This makes sure that if the model matrix is wrong, this function will not return
        //  saying that it has been touched. We forgo the times where it might be true,
        //  for the times where it is not even close.
        // This might be a thread volatile situation
        if(!tempMatRet && name.equals("Ship sprite")) {
            // try again
//            System.out.println("R| matrix was not inverted");
//            System.out.print("R| model matrix: ");
//            for (float x : modelMatrix) {
//                System.out.print(x);
//                System.out.print(", ");
//            }
//            System.out.println();

            lockedModelMatrix[0] = 1.0f;
            lockedModelMatrix[5] = 1.0f;
            lockedModelMatrix[10] = 1.0f;

            tempModelAfter = lockedModelMatrix;

            tempMatRet = Matrix.invertM(inverted, 0, lockedModelMatrix, 0);
            System.out.println("R| replacement worked: " + tempMatRet);
            System.out.println("R| ________________");

            System.out.print("R| model matrix before: ");
            for (float x : tempModelBefore) {
                System.out.print(x);
                System.out.print(", ");
            }
            System.out.println();

            System.out.print("R| model matrix after: ");
            for (float x : tempModelAfter) {
                System.out.print(x);
                System.out.print(", ");
            }
            System.out.println();

            if(!tempMatRet) {
                Log.e("Rectangle", "Model matrix could not be inverted. Aborting collision check");
                return false;
            }
        }

        // Copy values because we don't want to change touchPoint as it is used for all checks
        float[] eachRectTouch = new float[4];
        eachRectTouch[0] = position.x;
        eachRectTouch[1] = position.y;
        eachRectTouch[2] = 0;
        eachRectTouch[3] = 1;

        // Convert touchPoint into model coordinate system
        Matrix.multiplyMV(eachRectTouch, 0, inverted, 0, eachRectTouch, 0);
        //System.out.println("Rectangle| touch4 " + name +  " x,y: " + eachRectTouch[0] + ", " + eachRectTouch[1]);

        float x = eachRectTouch[0];
        float y = eachRectTouch[1];

        // Check if coordinates are within the boundaries of the box
        // 0 =

        if(isSim) {
            if(x >= (rectCoords[0]) && x <= (rectCoords[9])
                    && y >= (rectCoords[1]) && y <= (rectCoords[10]) )
            {
                //Log.i("Matched","Rect/Line" + x + ", " + y);
                return true;
            }
        } else {
            if(x >= (rectCoords[0] * touchScale) && x <= (rectCoords[9] * touchScale)
                    && y >= (rectCoords[1] * touchScale) && y <= (rectCoords[10] * touchScale) )
            {
                //Log.i("Matched","Rect/Line" + x + ", " + y);
                return true;
            }
        }


        return false;
    }

    // Don't use this on a thread other than main?? It is not being used but it causes glitchy action
    public Point unproject(Point point) {


        float[] inverted = new float[16];
        Matrix.invertM(inverted, 0, modelMatrix, 0);

        // Copy values because we don't want to change touchPoint as it is used for all checks
        float[] eachRectTouch = new float[4];
        eachRectTouch[0] = point.x;
        eachRectTouch[1] = point.y;
        eachRectTouch[2] = point.z;
        eachRectTouch[3] = 1;

        float[] outMat = new float[4];

        // Convert touchPoint into model coordinate system
        Matrix.multiplyMV(outMat, 0, inverted, 0, eachRectTouch, 0);

        return new Point(eachRectTouch[0], eachRectTouch[1], eachRectTouch[2]);
    }
//
//    public boolean invert4x4Mat(float[] matOut, float[] matIn) {
//        for (int i = 0; i < matIn.length; i++) {
//            matOut[i]
//        }
//
//
//
//        return true;
//    }
}
