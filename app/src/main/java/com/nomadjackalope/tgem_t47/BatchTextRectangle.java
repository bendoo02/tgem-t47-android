package com.nomadjackalope.tgem_t47;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import javax.microedition.khronos.opengles.GL;

/**
 * Created by benjamin on 3/27/17.
 *
 * Uses code from d3alek/Texample2 @ github
 *
 * Holds a string that it renders to the size of the node containing it
 */
public class BatchTextRectangle extends Rectangle {

    // must be the same as the size of u_MVPMatrix
    // in BatchTextProgram

    final static int VERTEX_SIZE = 5;                  // Vertex Size (in Components) ie. (X,Y,U,V,M), M is MVP matrix index
    final static int VERTICES_PER_SPRITE = 4;          // Vertices Per Sprite
    final static int INDICES_PER_SPRITE = 6;           // Indices Per Sprite

    int align;
    float totalWidth = 0;

    // Set the width for which a character will be moved to the next line
    float maxWidth = Float.MAX_VALUE;

    //--Members--//

    int textureHandle;

    Vertices vertices;                                 // Vertices Instance Used for Rendering
    float[] vertexBuffer;                              // Vertex Buffer
    int bufferIndex;                                   // Vertex Buffer Start Index
    int maxSprites;                                    // Maximum Sprites Allowed in Buffer
    int numSprites;

    private float[] uMVPMatrices = new float[FontObjectPNG.CHAR_BATCH_SIZE*16];

    private FontObjectPNG fontObject;

    public BatchTextRectangle() {

    }

    public BatchTextRectangle(int maxSprites) {
        fontObject = MyGLRenderer.fontObj; //TODO this should be able to be other fonts

        color = new float[]{0.988f, 0.988f, 0.384f, 1.0f};

        this.vertexBuffer = new float[maxSprites * VERTICES_PER_SPRITE * VERTEX_SIZE];  // Create Vertex Buffer
        this.vertices = new Vertices(maxSprites * VERTICES_PER_SPRITE, maxSprites * INDICES_PER_SPRITE);  // Create Rendering Vertices
        this.bufferIndex = 0;                           // Reset Buffer Index
        this.maxSprites = maxSprites;                   // Save Maximum Sprites
        this.numSprites = 0;                            // Clear Sprite Counter

        short[] indices = new short[maxSprites * INDICES_PER_SPRITE];  // Create Temp Index Buffer
        int len = indices.length;                       // Get Index Buffer Length
        short j = 0;                                    // Counter
        for ( int i = 0; i < len; i+= INDICES_PER_SPRITE, j += VERTICES_PER_SPRITE )  {  // FOR Each Index Set (Per Sprite)
            indices[i + 0] = (short)( j + 0 );           // Calculate Index 0
            indices[i + 1] = (short)( j + 1 );           // Calculate Index 1
            indices[i + 2] = (short)( j + 2 );           // Calculate Index 2
            indices[i + 3] = (short)( j + 2 );           // Calculate Index 3
            indices[i + 4] = (short)( j + 3 );           // Calculate Index 4
            indices[i + 5] = (short)( j + 0 );           // Calculate Index 5
        }
        vertices.setIndices( indices, 0, len );         // Set Index Buffer for Rendering
    }

    private float tempV[] = new float[4];
    private float tempVIn[] = new float[4];
    @Override
    public void updateMatrix(float[] vMatrix, float[] pMatrix, Point position, float scaleX, float scaleY, float rotation) {
        // Update layer
        layer = position.z;

        // Matrix math ----------------------------------------
        // Draw the triangle facing straight on.
        Matrix.setIdentityM(modelMatrix, 0);
//        Matrix.scaleM(modelMatrix, 0, scaleX, scaleY, 1.0f);
//        Matrix.rotateM(modelMatrix, 0, rotation, 0.0f, 0.0f, 1.0f);
//        Matrix.translateM(modelMatrix, 0, position.x, position.y, position.z);

        float alignOffset = 0;
        switch (align) {
            case TextNode.RIGHT:
                alignOffset = -totalWidth;
                break;
            case TextNode.CENTER:
                alignOffset = -(totalWidth / 2);
                break;
            case TextNode.LEFT:
                alignOffset = 0;
                break;
            default:
                break;
        }

        // Old order - correct order - don't ask me
        Matrix.translateM(modelMatrix, 0, position.x + alignOffset, position.y, position.z);
        Matrix.rotateM(modelMatrix, 0, rotation, 0.0f, 0.0f, 1.0f);
        Matrix.scaleM(modelMatrix, 0, scaleX, scaleY, 1.0f);

        if(modelMatrix[0] == 0.0f) {
            //Log.i("R", "stop");
        }

        // Don't use these
//        Matrix.multiplyMM(mVMatrix, 0, modelMatrix, 0, vMatrix, 0);
//
//        Matrix.multiplyMM(mVPMatrix, 0, mVMatrix, 0, pMatrix, 0);

//        Matrix.multiplyMM(mVMatrix, 0, vMatrix, 0, modelMatrix, 0);
//
//        Matrix.multiplyMM(mVPMatrix, 0, pMatrix, 0, mVMatrix, 0);


//        Matrix.multiplyMM(mVPMatrix, 0, modelMatrix, 0, pMatrix, 0);


        // This one works
        Matrix.multiplyMM(mVMatrix, 0, pMatrix, 0, vMatrix, 0);

        Matrix.multiplyMM(mVPMatrix, 0, mVMatrix, 0, modelMatrix, 0);


//        System.out.println("R| " + name + " Scale, translate: " + mVPMatrix[0] + "," + mVPMatrix[5] + "," + mVPMatrix[10] + " | " + mVPMatrix[12] + "," + mVPMatrix[13]);


        for (int i = 0; i < rectCoordsF.length / 4; i++) {

            for (int j = 0; j < tempVIn.length; j++) {
                tempVIn[j] = rectCoordsF[j+(4*i)];
            }

            Matrix.multiplyMV(tempV, 0, mVPMatrix, 0, tempVIn, 0);

            for (int j = 0; j < tempV.length; j++) {
                combMatrix[j+(4*i)] = tempV[j];
            }
        }

    }

    public void draw(TextNode node, float[] vMatrix, float[] pMatrix, Material mat, Point position, float scaleX, float scaleY, float rotation) {
        // enable texture + alpha blending
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        begin(mat);

        updateMatrix(vMatrix, pMatrix, position, scaleX, scaleY, rotation);

        drawSelf(node);

        end();

        GLES20.glDisable(GLES20.GL_BLEND);
    }

    //doesn't work // I did get the texture to show by changing planet.baseshape.textureref = 3
    private void drawTest() {};

    private void begin(Material mat) {
        GLES20.glUseProgram(mat.getProgram()); // specify the program to use

        // Get Handles to all the necessary variables
        colorHandle = GLES20.glGetUniformLocation(mat.getProgram(), "u_Color");
        textureHandle = GLES20.glGetUniformLocation(mat.getProgram(), "u_Texture");
        mVPMatrixHandle = GLES20.glGetUniformLocation(mat.getProgram(), "u_MVPMatrix");

        GLES20.glUniform4fv(colorHandle, 1, color , 0);
        GLES20.glEnableVertexAttribArray(colorHandle);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE3);  // Set the active texture unit to texture unit 0

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, fontObject.getTextureId()); // Bind the texture to this unit
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0
        GLES20.glUniform1i(textureHandle, 3);

        numSprites = 0;
        bufferIndex = 0;

    }

    private final static float scaleFactor = 26;

    private void drawSelf(TextNode node) {
        float chrHeight = fontObject.getCellHeight() * node.getScaleY() / scaleFactor;          // Calculate Scaled Character Height
        float chrWidth = fontObject.getCellWidth() * node.getScaleX() / scaleFactor;            // Calculate Scaled Character Width
        int len = node.getText().length();                        // Get String Length

//        float x = node.getPositionX();
//        float y = node.getPositionY();
//        float z = node.getPositionZ();
//
//        x += ( chrWidth / 2.0f ) - ( fontObject.getFontPadX() * node.getScaleX() );  // Adjust Start X
//        y += ( chrHeight / 2.0f ) - ( fontObject.getFontPadY() * node.getScaleY() );  // Adjust Start Y

        // create a model matrix based on x, y and angleDeg
        //float[] modelMatrix = new float[16];
        //Matrix.setIdentityM(modelMatrix, 0);
        //Matrix.translateM(modelMatrix, 0, x, y, z);
       // Matrix.rotateM(modelMatrix, 0, node.getRotation(), 0, 0, 1); //TODO maybe this is the wrong way
        //Matrix.rotateM(modelMatrix, 0, angleDegX, 1, 0, 0);
        //Matrix.rotateM(modelMatrix, 0, angleDegY, 0, 1, 0);
        totalWidth = 0;

        // Need to get total width early on because the mVPMatrix depends on it
        for (int i = 0; i < len; i++)  {              // FOR Each Character in String
            int c = (int)node.getText().charAt(i) - FontObjectPNG.CHAR_START;  // Calculate Character Index (Offset by First Char in Font)
            if (c < 0 || c >= FontObjectPNG.CHAR_CNT)                // IF Character Not In Font
                c = FontObjectPNG.CHAR_UNKNOWN;                         // Set to Unknown Character Index
            totalWidth += (fontObject.getCharRgn()[c].xadvance * node.getScaleX() / scaleFactor);//(fontObject.getCharWidths()[c] + node.getSpaceX() ) * node.getScaleX() / scaleFactor;    // Advance X Position by Scaled Character Width

            if(totalWidth > maxWidth) {
                totalWidth = maxWidth;
                i = len;
            }
        }

        node.updateMatrix();

        float letterX, letterY;
        letterX = letterY = 0;

        for (int i = 0; i < len; i++)  {              // FOR Each Character in String
            int c = (int)node.getText().charAt(i) - FontObjectPNG.CHAR_START;  // Calculate Character Index (Offset by First Char in Font)
            if (c < 0 || c >= FontObjectPNG.CHAR_CNT)                // IF Character Not In Font
                c = FontObjectPNG.CHAR_UNKNOWN;                         // Set to Unknown Character Index
            TextureRegion reg = fontObject.getCharRgn()[c];
            //TODO: optimize - applying the same model matrix to all the characters in the string?
            drawSprite(letterX, letterY, reg.width/fontObject.getCellWidth() * chrWidth, chrHeight, reg, modelMatrix);  // Draw the Character
            letterX += (reg.xadvance * node.getScaleX() / scaleFactor);//(fontObject.getCharWidths()[c] + node.getSpaceX() ) * node.getScaleX() / scaleFactor;    // Advance X Position by Scaled Character Width
//            System.out.println("BTR| maxWidth: " + maxWidth);
            if(letterX > maxWidth) {
                letterX = 0;
                letterY -= chrHeight;
            }
        }
    }

    protected void drawSprite(float x, float y, float width, float height, TextureRegion region, float[] modelMatrix)  {
        if ( numSprites == maxSprites )  {              // IF Sprite Buffer is Full
            end();                                  // End Batch
            // NOTE: leave current texture bound!!
            numSprites = 0;                              // Empty Sprite Counter
            bufferIndex = 0;                             // Reset Buffer Index (Empty)
        }

        float halfWidth = width / 2.0f;                 // Calculate Half Width
        float halfHeight = height / 2.0f;               // Calculate Half Height
        float x1 = x - halfWidth;                       // Calculate Left X
        float y1 = y - halfHeight;                      // Calculate Bottom Y
        float x2 = x + halfWidth;                       // Calculate Right X
        float y2 = y + halfHeight;                      // Calculate Top Y

        vertexBuffer[bufferIndex++] = x1;               // Add X for Vertex 0
        vertexBuffer[bufferIndex++] = y1;               // Add Y for Vertex 0
        vertexBuffer[bufferIndex++] = region.u1;        // Add U for Vertex 0
        vertexBuffer[bufferIndex++] = region.v2;        // Add V for Vertex 0
        vertexBuffer[bufferIndex++] = numSprites;

        vertexBuffer[bufferIndex++] = x2;               // Add X for Vertex 1
        vertexBuffer[bufferIndex++] = y1;               // Add Y for Vertex 1
        vertexBuffer[bufferIndex++] = region.u2;        // Add U for Vertex 1
        vertexBuffer[bufferIndex++] = region.v2;        // Add V for Vertex 1
        vertexBuffer[bufferIndex++] = numSprites;

        vertexBuffer[bufferIndex++] = x2;               // Add X for Vertex 2
        vertexBuffer[bufferIndex++] = y2;               // Add Y for Vertex 2
        vertexBuffer[bufferIndex++] = region.u2;        // Add U for Vertex 2
        vertexBuffer[bufferIndex++] = region.v1;        // Add V for Vertex 2
        vertexBuffer[bufferIndex++] = numSprites;

        vertexBuffer[bufferIndex++] = x1;               // Add X for Vertex 3
        vertexBuffer[bufferIndex++] = y2;               // Add Y for Vertex 3
        vertexBuffer[bufferIndex++] = region.u1;        // Add U for Vertex 3
        vertexBuffer[bufferIndex++] = region.v1;        // Add V for Vertex 3
        vertexBuffer[bufferIndex++] = numSprites;

        // add the sprite mvp matrix to uMVPMatrices array

        //Matrix.multiplyMM(mPMatrix, 0, pMatrix , 0, modelMatrix, 0);

//        //TODO: make sure numSprites < 24
        System.arraycopy(mVPMatrix, 0, uMVPMatrices, numSprites * 16, 16);

        numSprites++;                                   // Increment Sprite Count
    }

    protected void end() {
        if ( numSprites > 0 )  {                        // IF Any Sprites to Render
            // bind MVP matrices array to shader
            GLES20.glUniformMatrix4fv(mVPMatrixHandle, numSprites, false, uMVPMatrices, 0);
            GLES20.glEnableVertexAttribArray(mVPMatrixHandle);

            vertices.setVertices( vertexBuffer, 0, bufferIndex);  // Set Vertices from Buffer
            vertices.bind();                             // Bind Vertices
            vertices.draw( GLES20.GL_TRIANGLES, 0, numSprites * INDICES_PER_SPRITE );  // Render Batched Sprites
            vertices.unbind();                           // Unbind Vertices
        }
    }


    @Override
    public boolean isTouched(Point position, boolean isSim) {
        // Convert into modelView space from projection space

        if(vertexBuffer == null || vertexBuffer.length <= 0 || numSprites == 0) {
            return false;
        }

        float[] inverted = new float[16];
        Matrix.invertM(inverted, 0, modelMatrix, 0);

        // Copy values because we don't want to change touchPoint as it is used for all checks
        float[] eachRectTouch = new float[4];
        eachRectTouch[0] = position.x;
        eachRectTouch[1] = position.y;
        eachRectTouch[2] = 0;
        eachRectTouch[3] = 1;

        // Convert touchPoint into model coordinate system
        Matrix.multiplyMV(eachRectTouch, 0, inverted, 0, eachRectTouch, 0);
        //System.out.println("Rectangle| touch4 " + name +  " x,y: " + eachRectTouch[0] + ", " + eachRectTouch[1]);

        float x = eachRectTouch[0];
        float y = eachRectTouch[1];

        // Check if coordinates are within the boundaries of the box
        // TODO needs to be top left to bottom right

        // TODO implement isSim here?
        if(numSprites == 0) {
          Log.e("BTR", "Sprites count = 0?!");
        } else if(x >= (vertexBuffer[0] * touchScale) && x <= (vertexBuffer[numSprites * VERTICES_PER_SPRITE * VERTEX_SIZE - (2 * VERTEX_SIZE)] * touchScale)
                && y >= (vertexBuffer[1] * touchScale) && y <= (vertexBuffer[10] * touchScale) )
        {
            Log.i("BTR","text touched");
            return true;
        }

        return false;
    }
}
