package com.nomadjackalope.tgem_t47;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Random;

/**
 * Created by benjamin on 4/11/17.
 */
public class BackgroundMesh extends Rectangle {


    private FloatBuffer vertBuffer;
    private ShortBuffer gridOrderBuffer;

    private float[] gridCoords;
    private short[] gridOrder;

    private int gridWidth;
    private int gridHeight;


    private int randSeedHandle = -1;
    private float[] randSeeds;
    private FloatBuffer randBuffer;

    private int timeHandle = -1;

    private Random random = new Random();

    private int updateSpeed = 20;
    private int numFrames = 0;


    public BackgroundMesh(String name, int gridWidth, int gridHeight) {
        this.name = name;

        init(gridWidth, gridHeight);
    }

    private void generateGridCoords(int width, int height) {
        gridCoords = new float[(width + 1) * (height + 1) * COORDS_PER_VERTEX];

        float widthF = width;
        float heightF = height;

        for (int i = 0; i <= height; i++) {
            for (int j = 0; j <= width; j++) {
                gridCoords[i * (width + 1) * COORDS_PER_VERTEX + j * COORDS_PER_VERTEX] = (-widthF / 2 + j);
                gridCoords[i * (width + 1) * COORDS_PER_VERTEX + j * COORDS_PER_VERTEX + 1] = (-heightF / 2 + i);
                gridCoords[i * (width + 1) * COORDS_PER_VERTEX + j * COORDS_PER_VERTEX + 2] = 0.0f;

            }
        }

        gridOrder = new short[((width + 1) * height + (height + 1) * width) * 2]; // 2 = num verts per line,
//
        int offset = 0;

        for (int row = 0; row <= height; row++) {

            // Iterate across horizontal lines
            for (int i = 0; i < width; i++) {
                int j = i * 2 + offset;
                int adjust = row * (width + 1);

                gridOrder[j] = (short) (i + adjust);
                gridOrder[j + 1] = (short) (i + 1 + adjust);

            }

            offset += width * 2; // 2 = num verts per line

            // If we are not at the top
            if (row < height) {
                // Iterate across vertical lines
                for (int i = 0; i <= width; i++) {
                    int j = i * 2 + offset;
                    int adjust = row * (width + 1);

                    gridOrder[j] = (short) (i + adjust);
                    gridOrder[j + 1] = (short) (i + 1 + width + adjust);
                }

                offset += (width + 1) * 2;
            }
        }


    }

    private void generateRandSeeds(int width, int height) {
        randSeeds = new float[(width + 1) * (height + 1)];

        System.out.print("BM| randSeeds ");

        for (int i = 0; i < randSeeds.length; i++) {
            randSeeds[i] = random.nextFloat();

            System.out.print(randSeeds[i]);
            System.out.print(", ");
        }

        System.out.println("done");


    }

    private static final short[] squareOrder = new short[] { 0,1,2, 2,1,3 };

    void init(int gridWidth, int gridHeight) {

        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;

        generateGridCoords(gridWidth, gridHeight);
        generateRandSeeds(gridWidth, gridHeight);

        // initialize vertex byte buffer for shape coords
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // number of coord vals * 4 bytes per float
                gridCoords.length * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        vertBuffer = bb.asFloatBuffer();
        vertBuffer.put(gridCoords);
        // set the buffer to read the first coordinate
        vertBuffer.position(0); // or vertexBuffer.rewind(); either works

        ByteBuffer gob = ByteBuffer.allocateDirect(
                // number of coord vals * 4 bytes per float
                gridOrder.length * 4
        );
        gob.order(ByteOrder.nativeOrder());

        gridOrderBuffer = gob.asShortBuffer();
        gridOrderBuffer.put(gridOrder);
        gridOrderBuffer.position(0);

        // Random buffer
        ByteBuffer rb = ByteBuffer.allocateDirect(
                randSeeds.length * 4);
        bb.order(ByteOrder.nativeOrder());

        randBuffer = rb.asFloatBuffer();
        randBuffer.put(randSeeds);
        randBuffer.position(0);

    }

    int[] vbo = new int[1];
    int[] ibo = new int[1];

    @Override
    public void draw(float[] vMatrix, float[] pMatrix, Material mat, Point position, float scaleX, float scaleY, float rotation) {

        numFrames++;

        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mat.getProgram());

        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);


        // Get int that refers to variable vPosition and set it to coordinates matrix
        positionHandle = GLES20.glGetAttribLocation(mat.getProgram(), "vPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertBuffer);

        randSeedHandle = GLES20.glGetAttribLocation(mat.getProgram(), "vRandSeed");
        GLES20.glEnableVertexAttribArray(randSeedHandle);
        GLES20.glVertexAttribPointer(randSeedHandle, 1,
                GLES20.GL_FLOAT, false,
                0, randBuffer);


        timeHandle = GLES20.glGetUniformLocation(mat.getProgram(), "time");
        GLES20.glUniform1f(timeHandle, ((float) numFrames) / 50.0f);


        // get handle to fragment shader's vColor member and set color for drawing the triangle
        colorHandle = GLES20.glGetUniformLocation(mat.getProgram(), "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);

        updateMatrix(vMatrix, pMatrix, position, scaleX, scaleY, rotation);


        // get handle to shape's transformation matrix and pass the projectionView transformation to the shader
        mVPMatrixHandle = GLES20.glGetUniformLocation(mat.getProgram(), "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mVPMatrixHandle, 1, false, mVPMatrix, 0);


        //----------------- learnopengles lesson 8 -----------------------


        GLES20.glGenBuffers(1, vbo, 0);
        GLES20.glGenBuffers(1, ibo, 0);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo[0]);
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertBuffer.capacity()
                * 4, vertBuffer, GLES20.GL_STATIC_DRAW);

        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, ibo[0]);
        GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, gridOrderBuffer.capacity()
                * 2, gridOrderBuffer, GLES20.GL_STATIC_DRAW);


        GLES20.glLineWidth(0.5f);

        GLES20.glDrawElements(GLES20.GL_LINES, gridOrder.length, GLES20.GL_UNSIGNED_SHORT, 0);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);


        GLES20.glDisable(GLES20.GL_BLEND);

        // Draw the rectangle
        //GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, vertexCount);
        //GLES20.glDrawElements(GLES20.GL_LINES, 0, GLES20.GL_UNSIGNED_SHORT, gridOrderBuffer);

        // Disable vertex array // Don't know why
        GLES20.glDisableVertexAttribArray(positionHandle);
        GLES20.glDisableVertexAttribArray(randSeedHandle);

    }
}
