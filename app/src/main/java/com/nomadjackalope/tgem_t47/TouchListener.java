package com.nomadjackalope.tgem_t47;

import android.view.MotionEvent;

/**
 * Created by benjamin on 1/28/17.
 */
public interface TouchListener {
    boolean onTouchDown(MotionEvent event, Point realPoint, Node sender);
    boolean onTouchMove(MotionEvent event, Point realPoint, Node sender);
    boolean onTouchUp(MotionEvent event, Point realPoint, Node sender);
}
