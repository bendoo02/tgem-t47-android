package com.nomadjackalope.tgem_t47;

import android.util.Log;

/**
 * Created by benjamin on 1/23/17.
 *
 * This class takes inspiration from Cocos2d's Action class
 *
 * Base class for all actions that change properties over time
 *
 * Use this to animate things easily. NOT INTENDED TO BE SAFE FOR GAME OBJECTS. INTENDED FOR UI
 */
public class Action {

    private boolean isDone;
    private Node target;
    private float speed = 1.0f; // use to speed up and slow down animation (can use speed in functions
    private float baseRate; // percent change per update

    private long previousTime;
    private long deltaTime;
    private float percentDone;
    private boolean active;

    private ActionRunnable runAtPercent;
    private float runPercent = 1.0f;
    private boolean ran = false;

    private Action nextAction;

    // Constuctor
    public Action() {
        init(null, 1.0f);
    }

    public Action(Node target) {
        init(target, 1000f);
    }

    public Action(Node target, float millis) {
        init(target, millis);
    }

    protected void init(Node target, float millis) {
        setTarget(target);

        setBaseRate(millis);

        start();
    }


    // This class is called from Node and handles
    // Returns true if done
    public boolean update(long timeNow) {
//        System.out.println("A| timeNow: " + timeNow);
        // If we haven't started this before update the previous time to now for accuracy
        if(!isActive()) {
            start();
            setActive(true);
        }

        updatePercentDone(timeNow);

        // Calls the subclass's updateSubClass() method
        updateSubClass();

        if(runPercent >= getPercentDone() && runAtPercent != null && !ran) {
            ran = true;
            runAtPercent.run();
        }

        checkDone();

        if(!isDone) {
            MyGLRenderer.activeRenderer.myView.requestRender();
        }

        return isDone;
    }

    // Override this class and change this function to perform differently shaped motions such as easing
    // Linear update
    protected void updateSubClass() {
        Log.w("Action", "You need to subclass Action to update a property of a node.");
    }

    private void checkDone() {
        if(getPercentDone() >= 1.0f) {
            stop();
        }
    }

    public void start() {
        previousTime = MyGLRenderer.clock;

//        if(target != null) {
//            target.setIgnoreTouch(true);
//        }
    }

    private void stop() {
//        if(target != null) {
//            target.setIgnoreTouch(false);
//        }

        if(nextAction != null) {
            target.addAction(nextAction);
        }

        target = null;
        isDone = true;

    }

    // millis
    protected long updateDeltaTime(long timeNow) {

        deltaTime = timeNow - previousTime;

        previousTime = timeNow;

        return deltaTime;
    }

    protected float updatePercentDone(long timeNow) {
        // To make non-linear interpolation, put this in a new function to be overridden or something similar
        setPercentDone(getPercentDone() + getBaseRate() * updateDeltaTime(timeNow) * getSpeed());

        // If this is finished we just go to the end state and then set things to
        if(getPercentDone() >= 1.0f) {
            setPercentDone(1.0f);
        }
        return getPercentDone();
    }

    // sets percent done to 1.0f and updates
    public void forceFinish() {
        update(Long.MAX_VALUE);
    }

    // ---------------------- Getters and Setters ------------------------
    public boolean isDone() {
        isDone = percentDone >= 1;
        return isDone;
    }

    public Node getTarget() {
        return target;
    }

    public void setTarget(Node target) {
        this.target = target;
    }

    public float getSpeed() {
        return speed;
    }

    protected void setBaseRate(float millis) {
        baseRate = 1 / millis;
    }

    protected float getBaseRate() {
        return baseRate;
    }

    public float getPercentDone() {
        return percentDone;
    }

    public void setPercentDone(float percentDone) {
        this.percentDone = percentDone;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public ActionRunnable getRunAtPercent() {
        return runAtPercent;
    }

    public void setRunAtPercent(ActionRunnable runnable, float percent) {
        runAtPercent = runnable;
        runPercent = percent;
    }

    public void setNextAction(Action nextAction) {
        this.nextAction = nextAction;
    }

    public Action getNextAction() { return nextAction; }
}
