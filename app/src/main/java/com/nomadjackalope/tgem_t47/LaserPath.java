package com.nomadjackalope.tgem_t47;

import android.opengl.Matrix;

import java.util.ArrayList;

/**
 * Created by benjamin on 1/30/17.
 *
 * Used to hold all the individual laser sprites
 *
 * Nodes are not used here for performance reasons
 */
public class LaserPath extends Node {

    boolean laserIsDead;

    boolean initialForceApplied = false;
    Vector laserForce = new Vector(0f,0.5f);
    Vector laserVelocity = new Vector();
    Point laserPosition = new Point(0, 0);

    float laserMass = 1; // kg

    //private ArrayList<Particle> laserPoints = new ArrayList<>();
    public ParticleEmitter trace = new ParticleEmitter("LaserPath trace");

    // Shows where laser is when off screen, and how far
    public Node tracker;

    public LaserPath(String name) {
        initDefaults(name);
        setVisible(false);

        tracker = new Node("Laser tracker");
//        tracker.setVisible(false);
        tracker.baseShape.color = new float[]{0.12f, 0.12f, 0.93f, 1.0f};

        addChild(tracker);


        trace.setEmitterSpeed(30);
        trace.pLife = Float.MAX_VALUE;
        trace.setVisible(false);
        addChild(trace);
    }

    public boolean isLaserIsDead() {
        return laserIsDead;
    }

    public void setLaserIsDead(boolean laserIsDead) {
        this.laserIsDead = laserIsDead;

        tracker.setVisible(false);
        // Make this invisible if it is dead
       // this.setVisible(!laserIsDead, true);
    }

    float[] laserVector = new float[4];
    float[] myModelMatrix = new float[16];
    float absX;
    float absY;
    float[] glCoord;
    Point tempPoint = new Point();

    @Override
    public void draw(MyGLRenderer renderer) {
        if(material == null) {
            material = renderer.getDefaultMaterial();
        }

        if(isVisible()) {
            baseShape.draw(renderer.getViewMatrix(), renderer.getProjectionMatrix(),
                    material, getPosition(), getScaleX(), getScaleY(), getRotation());
        }

        // If laser is off screen update tracker
        laserVector[0] = laserPosition.x;
        laserVector[1] = laserPosition.y;
        laserVector[2] = 0.0f;
        laserVector[3] = 1.0f;


        Matrix.setIdentityM(myModelMatrix, 0);
        //Matrix.translateM(myModelMatrix, 0, laserVector[0], laserVector[1], laserVector[2]);
        Matrix.multiplyMM(myModelMatrix, 0, renderer.getViewMatrix(), 0, myModelMatrix, 0);
        Matrix.multiplyMM(myModelMatrix, 0, renderer.getProjectionMatrix(), 0, myModelMatrix, 0);
        Matrix.multiplyMV(laserVector, 0, myModelMatrix, 0, laserVector, 0); // children.get(children.size() - 1).baseShape.mVPMatrix

        absX  = Math.abs(laserVector[0]);
        absY = Math.abs(laserVector[1]);

        if(absX > 1 || absY > 1) {

            if(Math.max(absX, absY) == absX) {
                // Y is the limiter

                glCoord = renderer.glCoordinates(laserVector[0]/absX , laserVector[1]/absX);

            } else {
                // X is the limiter

                glCoord = renderer.glCoordinates(laserVector[0]/absY, laserVector[1]/absY);

            }

            tracker.setPosition(glCoord[0], glCoord[1]);
            tracker.setRotation((float)Math.atan2(tracker.getPositionY(), tracker.getPositionX()) * 57.29577951f);
            tracker.setScale(tracker.getPosition().distance(tempPoint) / 10 + 0.1f);

            tracker.setVisible(true);

        } else {
            tracker.setVisible(false);
        }

        // Tells children to render
        int childrenSize = children.size();
        for (int i = 0; i < childrenSize; i++) {
            children.get(i).draw(renderer);
        }





    }

    @Override
    public ArrayList<Node> checkTouch(Point position, ArrayList<Node> touchedNodes) {

        // We don't even want to ignore touches on laser sprites because there could be so many

        return touchedNodes;
    }

    private void updateTracker(MyGLRenderer renderer) {
        // Set position on edge of screen closest to laser pos
        //tracker.setPosition();

        // rotate to face laser pos
        //tracker.setRotation();

        // indicate the distance from edge of screen

    }

//    public ArrayList<Particle> getLaserPoints() {
//        return laserPoints;
//    }
//
//    public void setLaserPoints(ArrayList<Particle> laserPoints) {
//        this.laserPoints = laserPoints;
//    }
//
//    public void addLaserPoint(Particle particle) {
//        laserPoints.add(particle);
//    }

    //----------------------- Particle Emitter -------------------------

    public void reconfigure() {

    }
}
