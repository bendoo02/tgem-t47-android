package com.nomadjackalope.tgem_t47;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * Created by benjamin on 5/1/17.
 *
 * Fonts that are from pre-rasterized fonts
 *
 * Most of this is taken from Libgdx - BitmapFontData
 */
public class FontObjectPNG {
    final String TAG = "FOPNG";

    //--Constants--//
    public final static int CHAR_START = 32;           // First Character (ASCII Code)
    public final static int CHAR_END = 126;            // Last Character (ASCII Code)
    public final static int CHAR_CNT = ( ( ( CHAR_END - CHAR_START ) + 1 ) + 1 );  // Character Count (Including Character to use for Unknown)

    public final static int CHAR_NONE = 32;            // Character to Use for Unknown (ASCII Code)
    public final static int CHAR_UNKNOWN = ( CHAR_CNT - 1 );  // Index of the Unknown Character

    public final static int FONT_SIZE_MIN = 6;         // Minumum Font Size (Pixels)
    public final static int FONT_SIZE_MAX = 180;       // Maximum Font Size (Pixels)

    public final static int CHAR_BATCH_SIZE = 36;     // Number of Characters to Render Per Batch
    // must be the same as the size of u_MVPMatrix
    // in BatchTextProgram

    /** An array of the image paths, for multiple texture pages. */
    public String imagePaths;
    public String fontFile;
    public float padTop, padRight, padBottom, padLeft;

    private int fontPadX, fontPadY;

    private final float[] charWidths;                          // Width of Each Character (Actual; Pixels)
    public TextureRegion[] charRgn;

    private int textureId;                                     // Font Texture ID [NOTE: Public for Testing Purposes Only!]
    private int textureSize;                                   // Texture Size for Font (Square)

    private int cellWidth, cellHeight;                         // Character Cell Width/Height


    public FontObjectPNG() {

        charWidths = new float[FontObject.CHAR_CNT];               // Create the Array of Character Widths
        charRgn = new TextureRegion[FontObject.CHAR_CNT];          // Create the Array of Character Regions

        // initialize remaining members
        fontPadX = 0;
        fontPadY = 0;

        textureId = -1;
        textureSize = 0;

    }

    public void load (String fontFile, Context context) {
        if (imagePaths != null) throw new IllegalStateException("Already loaded.");

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(context.getAssets().open(fontFile)), 512);

            String line = reader.readLine(); // info
            if (line == null) Log.e(TAG, "File is empty.");

            line = line.substring(line.indexOf("padding=") + 8);
            String[] padding = line.substring(0, line.indexOf(' ')).split(",", 4);
            if (padding.length != 4) Log.e(TAG, "Invalid padding.");
            padTop = Integer.parseInt(padding[0]);
            padRight = Integer.parseInt(padding[1]);
            padBottom = Integer.parseInt(padding[2]);
            padLeft = Integer.parseInt(padding[3]);

            fontPadX = (int)(padRight + padLeft);
            fontPadY = (int)(padTop + padBottom);

            line = reader.readLine();
            if (line == null) Log.e(TAG, "Missing common header.");
            String[] common = line.split(" ", 7); // At most we want the 6th element; i.e. "page=N"

            // At least lineHeight and base are required.
            if (common.length < 3) Log.e(TAG, "Invalid common header.");

            if (!common[1].startsWith("lineHeight=")) Log.e(TAG, "Missing: lineHeight");
            cellHeight = Integer.parseInt(common[1].substring(11));

            if (!common[2].startsWith("base=")) Log.e(TAG, "Missing: base");
            float baseLine = Integer.parseInt(common[2].substring(5));

            if(!common[3].startsWith("scaleW")) Log.e(TAG, "Missing: scaleW");
            float scaleW = Integer.parseInt(common[3].substring(7));
            textureSize = (int)scaleW;

            if(!common[4].startsWith("scaleH")) Log.e(TAG, "Missing: scaleH");
            float scaleH = Integer.parseInt(common[4].substring(7));


            int i = FontObjectPNG.CHAR_START;

            while (true) {
                line = reader.readLine();
                if (line == null) break; // EOF
                if (line.startsWith("kernings ")) break; // Starting kernings block.
                if (!line.startsWith("char ")) continue;


                if(i > FontObjectPNG.CHAR_END) break;

                line = line.replaceAll("\\s+", ",");

                String[] result = line.split("=|,");

                if(Integer.parseInt(result[2]) >= FontObjectPNG.CHAR_START) {

                    while (i < Integer.parseInt(result[2])) {
                        charRgn[i - FontObjectPNG.CHAR_START] = new TextureRegion(scaleW, scaleH, 0, 0, 0, 0);
                        i++;
                    }

                    charWidths[i - FontObject.CHAR_START] = Integer.parseInt(result[8]);


                    charRgn[i - FontObjectPNG.CHAR_START] = new TextureRegion(scaleW, scaleH,
                            Integer.parseInt(result[4]), Integer.parseInt(result[6]),
                            Integer.parseInt(result[8]), Integer.parseInt(result[10]));

                    charRgn[i - FontObjectPNG.CHAR_START].xadvance = Integer.parseInt(result[16]);

                    // Gets largest cell width
                    if(cellWidth < Integer.parseInt(result[8])) {
                        cellWidth = Integer.parseInt(result[8]);
                    }

                    i++;
                }
            }

            reader.close();


        } catch (Exception ex) {
            Log.e(TAG, "Error loading font file: " + fontFile, ex);
        } finally {
            // close reader?
        }
    }

    public String getFontFile () {
        return fontFile;
    }

    public int getFontPadX() {
        return fontPadX;
    }

    public int getFontPadY() {
        return fontPadY;
    }

    public TextureRegion[] getCharRgn() {
        return charRgn;
    }

    public float[] getCharWidths() {
        return charWidths;
    }

    public int getCellWidth() {
        return cellWidth;
    }

    public int getCellHeight() {
        return cellHeight;
    }

    public int getTextureId() {
        return textureId;
    }

    public void setTextureId(int id) {
        textureId = id;
    }

}
