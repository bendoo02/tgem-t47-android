package com.nomadjackalope.tgem_t47;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;
import android.util.StringBuilderPrinter;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by benjamin on 1/5/17.
 */
public class Cube {

    private FloatBuffer vertexBuffer;
    private ShortBuffer drawListBuffer;

    private static final String vertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    // the matrix must be included as a modifier of gl_Position
                    // Note that the uMVPMatrix factor *must be first* in order
                    // for the matrix multiplication product to be correct.
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "}";

    // Use to access and set the view transformation
    private int mMVPMatrixHandle;

    private static final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";

    private static int mProgram;

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    static float cubeCoords[] = {
            -0.25f, -0.25f, 0.0f,
            0.25f, -0.25f, 0.0f,
            0.25f, 0.25f, 0.0f,
            -0.25f, 0.25f, 0.0f,
            -0.25f, -0.25f, 0.5f,
            0.25f, -0.25f, 0.5f,
            0.25f, 0.25f, 0.5f,
            -0.25f, 0.25f, 0.5f
    };

    private float myCubeCoords[] = cubeCoords;

//    static float cubeCoords[] = {
//            -0.25f,  0.25f, 0.0f,   // top left front
//            -0.25f, -0.25f, 0.0f,   // bottom left front
//            0.25f, -0.25f, 0.0f,   // bottom right front
//            0.25f,  0.25f, 0.0f }; // top right front

    private short drawOrder[] = {0,1,3, 1,2,3}; // , 1,6,7, 1,2,7, 6,7,4, 6,4,5, 5,4,3, 5,3,0, 3,4,7, 3,2,7, 0,1,6, 0,5,6};

    // Set color with rgba
    float color[] = { 0.63f, 0.42f, 0.22f, 1.0f };

    private int mPositionHandle;
    private int mColorHandle;

    private final int vertexCount = myCubeCoords.length / COORDS_PER_VERTEX;
    private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex

    public Cube() {
        System.out.println("C| here");
        // initialize vertex byte buffer for shape coords
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // number of coord vals * 4 bytes per float
                myCubeCoords.length * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        // add the coordinates to the FloatBuffer
        vertexBuffer.put(myCubeCoords);
        // set the buffer to read the first coordinate
        vertexBuffer.position(0); // or vertexBuffer.rewind(); either works

        // initialize byte buffer for the draw list
        ByteBuffer dlb = ByteBuffer.allocateDirect(
                // # of coord vals * 2 bytes per short
                drawOrder.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);
    }

    public static void loadShaders() {

        // Alternate way to do it -----------
//
//        int vertexShader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
//        GLES20.glShaderSource(vertexShader, vertexShaderCode);
//        GLES20.glCompileShader(vertexShader);
//        String vertexShaderCompileLog = GLES20.glGetShaderInfoLog(vertexShader);
//
//        int fragmentShader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
//        GLES20.glShaderSource(fragmentShader, fragmentShaderCode);
//        GLES20.glCompileShader(fragmentShader);
//        String fragmentShaderCompileLog = GLES20.glGetShaderInfoLog(fragmentShader);
        //-----------

        // Load shaders ----------------------------------------------
        int vertexShader = createShader(GLES20.GL_VERTEX_SHADER,
                vertexShaderCode);
        int fragmentShader = createShader(GLES20.GL_FRAGMENT_SHADER,
                fragmentShaderCode);

        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram();

        // add the vertex shader to program
        GLES20.glAttachShader(mProgram, vertexShader);

        // add the fragment shader to program
        GLES20.glAttachShader(mProgram, fragmentShader);

        // Gets log of programs
        //String programLinkLog = GLES20.glGetProgramInfoLog(mProgram);

        // creates OpenGL ES program executables
        GLES20.glLinkProgram(mProgram);
    }

    public static int createShader(int type, String shaderCode){

        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }

    public void draw(float[] mvpMatrix) {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mProgram);

        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        // get handle to fragment shader's vColor member
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");

        // Set color for drawing the triangle
        GLES20.glUniform4fv(mColorHandle, 1, color, 0);

        // get handle to shape's transformation matrix
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);

        // Draw the triangle
        //GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length, GLES20.GL_UNSIGNED_SHORT, drawListBuffer);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }

    public void setPos(float x, float y) {
        Matrix.translateM(myCubeCoords, 0, x, y, 0);
    }

    public boolean isTouched(float[] touchPoint) {
        float x2 = touchPoint[0];
        float y2 = touchPoint[1];

        if(x2>=myCubeCoords[0] && x2<=myCubeCoords[6]&& y2>=myCubeCoords[1] && y2<=myCubeCoords[7] )
        {
            Log.i("Matched","Rect/Line");
            return true;
        }

        return false;
    }
}
