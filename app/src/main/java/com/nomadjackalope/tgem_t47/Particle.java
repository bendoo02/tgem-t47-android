package com.nomadjackalope.tgem_t47;

/**
 * Created by benjamin on 5/16/17.
 *
 * This is a single particle held, updated, and drawn through particle emitter objects
 */
public class Particle {

    Point position = new Point();
    Point size = new Point();
    float life;
    Vector veloctiy = new Vector();
    float[] startColor = {1.0f, 1.0f, 1.0f, 1.0f};
    float[] endColor = {1.0f, 0.0f, 0.0f, 0.0f};
    float[] currentColor = new float[4];

    public Particle() {
        init(0, 0, Float.MAX_VALUE, 0, 0);
    }

    public Particle(float x, float y) {
        init(x, y, Float.MAX_VALUE, 0, 0);
    }

    public Particle(float x, float y, float life, float angleDeg, float speed) {
        init(x, y, life, angleDeg, speed);
    }

    private void init(float x, float y, float life, float angleDeg, float speed) {
        position.x = x;
        position.y = y;
        this.life = life;

        float angleInRadians = (float)(angleDeg * Math.PI / 180);

        veloctiy = new Vector(
                (float)(speed * Math.cos(angleInRadians)),
                (float)(speed * Math.sin(angleInRadians))
        );

        startColor[0] = 0.96643f;
        startColor[1] = 0.653f;
        startColor[2] = 0.358f;
        startColor[3] = 1.0f;

        size = new Point(0.25f, 0.25f);

    }

    // Returns true if alive
    public boolean update(float deltaTime) {
        life -= deltaTime;

        // If alive
        if(life > 0) {
            position.x += veloctiy.x * deltaTime;
            position.y += veloctiy.y * deltaTime;

            currentColor[0] = 1.0f;
            currentColor[1] = 1.0f;
            currentColor[2] = 0.0f;
            currentColor[3] = 1.0f;

            return true;
        }

        return false;
    }



}
