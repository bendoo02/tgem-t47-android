package com.nomadjackalope.tgem_t47;

/**
 * Created by benjamin on 3/27/17.
 *
 * Copied from d3alek/Texample2
 */
public enum AttribVariable {
    A_Position(1, "a_Position"),
    A_TexCoordinate(2, "a_TexCoordinate"),
    A_MVPMatrixIndex(3, "a_MVPMatrixIndex");

    private int mHandle;
    private String mName;

    AttribVariable(int handle, String name) {
        mHandle = handle;
        mName = name;
    }

    public int getHandle() {
        return mHandle;
    }

    public String getName() {
        return mName;
    }
}
