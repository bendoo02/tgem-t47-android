package com.nomadjackalope.tgem_t47;

/**
 * Created by benjamin on 1/30/17.
 *
 * A three-dimensional vector with basic math methods
 */
public class Vector {

    float x;
    float y;
    float z;

    public Vector() {
        x = 0;
        y = 0;
        z = 0;
    }

    public Vector(float x, float y) {
        this.x = x;
        this.y = y;
        z = 0;
    }

    public Vector(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Creates a new Vector pointing from p1 to p2
     * @param p1 origin point
     * @param p2 pointing point
     */
    public Vector(Point p1, Point p2) {
        set(p1, p2);
    }

    public void add(Vector v) {
        x += v.x;
        y += v.y;
        z += v.z;
    }

    public void subtract(Vector v) {
        x -= v.x;
        y -= v.y;
        z -= v.z;
    }

    // Multiplies this vector by a given scalar
    public void multiply(float scalar) {
        x *= scalar;
        y *= scalar;
        z *= scalar;
    }

    public void set(Point p1, Point p2) {
        x = p2.x - p1.x;
        y = p2.y - p1.y;
    }

    public void set(Vector v) {
        x = v.x;
        y = v.y;
        z = v.z;
    }

    public void set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float length() {
        return (float)Math.sqrt((x * x) + (y * y) + (z * z));
    }

    public void setToUnitLength() {
        float length = length();

        x = x/length;
        y = y/length;
        z = z/length;

    }


}
