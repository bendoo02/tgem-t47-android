package com.nomadjackalope.tgem_t47;

import android.database.CharArrayBuffer;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.games.Game;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;

import java.util.ArrayList;

/**
 * Created by benjamin on 1/18/17.
 */
public class SinglePlayerMatch implements TurnBasedMatch {

    private ArrayList<String> players = new ArrayList<>();
    private byte[] data;
    private int status = TurnBasedMatch.MATCH_STATUS_ACTIVE;
    private int turnStatus = TurnBasedMatch.MATCH_TURN_STATUS_MY_TURN;

    public SinglePlayerMatch() {
        players.add("p_1");
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public SinglePlayerMatch createFromParcel(Parcel source) {
            return new SinglePlayerMatch();
        }

        @Override
        public SinglePlayerMatch[] newArray(int size) {
            return new SinglePlayerMatch[size];
        }
    };

    @Override
    public Game getGame() {
        return null;
    }

    @Override
    public String getMatchId() {
        return null;
    }

    @Override
    public String getCreatorId() {
        return null;
    }

    @Override
    public long getCreationTimestamp() {
        return 0;
    }

    @Override
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int getTurnStatus() {
        return turnStatus;
    }

    public void setTurnStatus(int turnStatus) {
        this.turnStatus = turnStatus;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void getDescription(CharArrayBuffer charArrayBuffer) {

    }

    @Override
    public int getVariant() {
        return 0;
    }

    @Override
    public String getLastUpdaterId() {
        return null;
    }

    @Override
    public long getLastUpdatedTimestamp() {
        return 0;
    }

    @Override
    public String getPendingParticipantId() {
        return players.get(0);
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public byte[] getData() {
        return data;
    }

    @Override
    public int getVersion() {
        return 0;
    }

    @Override
    public boolean canRematch() {
        return false;
    }

    @Override
    public String getRematchId() {
        return null;
    }

    @Override
    public byte[] getPreviousMatchData() {
        return null;
    }

    @Override
    public int getMatchNumber() {
        return 0;
    }

    @Override
    public Bundle getAutoMatchCriteria() {
        return null;
    }

    @Override
    public int getAvailableAutoMatchSlots() {
        return 0;
    }

    @Override
    public boolean isLocallyModified() {
        return false;
    }

    @Override
    public int getParticipantStatus(String s) {
        return 0;
    }

    @Override
    public ArrayList<String> getParticipantIds() {
        return players;
    }

    @Override
    public String getParticipantId(String s) {
        return null;
    }

    @Override
    public Participant getParticipant(String s) {
        return null;
    }

    @Override
    public String getDescriptionParticipantId() {
        return null;
    }

    @Override
    public Participant getDescriptionParticipant() {
        return null;
    }

    @Override
    public TurnBasedMatch freeze() {
        return null;
    }

    @Override
    public boolean isDataValid() {
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    @Override
    public ArrayList<Participant> getParticipants() {
        return null;
    }
}
