package com.nomadjackalope.tgem_t47;

/**
 * Created by benjamin on 6/26/17.
 */
public class ActionChangeText extends Action {

    String initialText;
    String finalText;

    public ActionChangeText(String initialText, String finalText, float millis, Node target) {
        this.initialText = initialText;
        this.finalText = finalText;

        init(target, millis);
    }

    @Override
    protected void updateSubClass() {
        if(getPercentDone() >= 0.0f) {
            ((TextNode) getTarget()).setText(finalText);
        }
    }

    public String getInitialText() {
        return initialText;
    }

    public void setInitialText(String initialText) {
        this.initialText = initialText;
    }

    public String getFinalText() {
        return finalText;
    }

    public void setFinalText(String finalText) {
        this.finalText = finalText;
    }
}
