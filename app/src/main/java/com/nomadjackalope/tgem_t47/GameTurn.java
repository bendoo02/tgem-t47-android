package com.nomadjackalope.tgem_t47;

import android.util.Log;

import com.google.android.gms.games.Player;
import com.google.android.gms.games.internal.constants.PlatformType;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/**
 * Created by benjamin on 1/16/17.
 */
public class GameTurn {

    public static final String TAG = "GameTurn";

    // If turn is less
    public int turn;
    public boolean simulate;

    public int numHumanPlayers;
    public int numCPUPlayers;
    public int numStationsPerPlayer;
    public Planet[] planets;
    public Ship[] ships;
    //public int turnCounter;

    public GameTurn() {
    }

    public GameTurn(int numHumans, int numCPUs, int numStationsPerPlayer, int type, int numPlanets) {
        numHumanPlayers = numHumans;
        numCPUPlayers = numCPUs;
        this.numStationsPerPlayer = numStationsPerPlayer;
        ships = new Ship[numHumans * numStationsPerPlayer + numCPUs * numStationsPerPlayer];
        planets = new Planet[numPlanets];

    }

    // This is the byte array we will write out to the TBMP API.
    public byte[] persist() {
        JSONObject retVal = new JSONObject();

        try {
            retVal.put("numCPUPlayers", numCPUPlayers);
            retVal.put("numSPP", numStationsPerPlayer);

            turn++;
            retVal.put("turn", turn);

            // Create JSONArray of JSONObjects for planets
            JSONArray planetArr = new JSONArray();
            for (int i = 0; i < planets.length; i++) {
                planetArr.put(createPlanetJSONObj(planets[i]));
            }

            JSONArray shipArr = new JSONArray();
            for (int i = 0; i < ships.length; i++) {
                shipArr.put(createShipJSONObj(ships[i]));
            }

            retVal.put("planets", planetArr);
            //retVal.put("turnCounter", turnCounter);

            retVal.put("ships", shipArr);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String st = retVal.toString();

        Log.d(TAG, "==== PERSISTING\n" + st);

        return st.getBytes(Charset.forName("UTF-8"));
    }

//    // Creates a new instance of GameTurn.
//    static public GameTurn unpersist() {
//
//        byte[] byteArray = match.getData();
//
//        if (byteArray == null) {
//            Log.d(TAG, "Empty array---possible bug.");
//            return new GameTurn();
//        }
//
//        String st = null;
//        try {
//            st = new String(byteArray, "UTF-8");
//        } catch (UnsupportedEncodingException e1) {
//            e1.printStackTrace();
//            return null;
//        }
//
//        Log.d(TAG, "====UNPERSIST \n" + st);
//
//        GameTurn retVal = new GameTurn();
//
//        try {
//            JSONObject obj = new JSONObject(st);
//
//            if (obj.has("x")) {
//                retVal.x = (float) obj.getDouble("x");
//            }
//            if (obj.has("y")) {
//                retVal.y = (float) obj.getDouble("y");
//            }
//            if (obj.has("turn")) {
//                retVal.turn = obj.getInt("turn");
//
//                // Turn starts at 1 not 0. If 3 people are in the game, on turn 4 we would be back to player 1
//                if(retVal.turn > match.getParticipantIds().size()) {
//                    retVal.simulate = true;
//                }
//            }
//            if (obj.has("planets")) {
//                JSONObject[] tempArr = destroyJSONArray(obj.getJSONArray("planets"));
//                retVal.planets = new Planet[tempArr.length];
//
//                for (int i = 0; i < tempArr.length; i++) {
//                    retVal.planets[i] = destroyPlanetJSONObj(tempArr[i]);
//                }
//            }
//            if (obj.has("ships")) {
//                JSONObject[] tempArr = destroyJSONArray(obj.getJSONArray("ships"));
//                retVal.ships = new Ship[tempArr.length];
//
//                for (int i = 0; i < tempArr.length; i++) {
//                    retVal.ships[i] = destroyShipJSONObj(tempArr[i], match.getPendingParticipantId());
//                }
//            }
//
//        } catch (JSONException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//        // go to next turn as it moves the current user's ship's
//        //      currentPower/Rotation to previousPower/Rotation
//        Log.d(TAG, "turn: " + retVal.turn);
//        Log.d(TAG, "participant ids size = " + match.getParticipantIds().size());
//        if((retVal.turn - 1) % match.getParticipantIds().size() == 0) {
//            Log.d(TAG, "GT| MATCH CHANGED AND MOVECURTOPREV HAPPENED");
//            for (int i = 0; i < retVal.ships.length; i++) {
//                retVal.ships[i].moveCurToPrev();
//
//                if(retVal.ships[i].userId.equals(match.getPendingParticipantId())) {
//                    retVal.ships[i].setEnemy(false);
//                    retVal.ships[i].createMenu();
//                    // Set the visible rotation
//                    retVal.ships[i].setRotation(retVal.ships[i].getCurrentRotation());
//                } else {
//                    retVal.ships[i].createMenu();
//                    // Set the rotation to a variable that is invisible
//                    retVal.ships[i].setRotation(retVal.ships[i].getPreviousRotation());
//                }
//            }
//        } else {
//            for (int i = 0; i < retVal.ships.length; i++) {
//
//                if(retVal.ships[i].userId.equals(match.getPendingParticipantId())) {
//                    retVal.ships[i].setEnemy(false);
//                    retVal.ships[i].createMenu();
//                    // Set the visible rotation
//                    retVal.ships[i].setRotation(retVal.ships[i].getCurrentRotation());
//                } else {
//                    retVal.ships[i].createMenu();
//                    // Set the rotation to a variable that is invisible
//                    retVal.ships[i].setRotation(retVal.ships[i].getPreviousRotation());
//                }
//            }
//        }
//
//
//
//        return retVal;
//    }

    public int totalPlayers() {
        return numCPUPlayers + numHumanPlayers;
    }

    static public GameTurn unpersist(TurnBasedMatch match) {
        return unpersist(match, match.getData());
    }

    static public GameTurn unpersist(TurnBasedMatch match, byte[] bytes) {

        if (bytes == null) {
            Log.d(TAG, "Empty array---possible bug.");
            return new GameTurn();
        }

        String st = null;
        try {
            st = new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
            return null;
        }

        Log.d(TAG, "====UNPERSIST \n" + st);

        GameTurn retVal = new GameTurn();

        retVal.numHumanPlayers = match.getParticipantIds().size();

        try {
            JSONObject obj = new JSONObject(st);

            if (obj.has("numCPUPlayers")) {
                retVal.numCPUPlayers = obj.getInt("numCPUPlayers");
            }
            if (obj.has("numSPP")) {
                retVal.numStationsPerPlayer = obj.getInt("numSPP");
            }
            if (obj.has("turn")) {
                retVal.turn = obj.getInt("turn");

                // Turn starts at 1 not 0. If 3 people are in the game, on turn 4 we would be back to player 1
                if(retVal.turn > retVal.numHumanPlayers) {
                    retVal.simulate = true;
                }
            }
            if (obj.has("planets")) {
                JSONObject[] tempArr = destroyJSONArray(obj.getJSONArray("planets"));
                retVal.planets = new Planet[tempArr.length];

                for (int i = 0; i < tempArr.length; i++) {
                    retVal.planets[i] = destroyPlanetJSONObj(tempArr[i]);
                }
            }
            if (obj.has("ships")) {
                JSONObject[] tempArr = destroyJSONArray(obj.getJSONArray("ships"));
                retVal.ships = new Ship[tempArr.length];

                for (int i = 0; i < tempArr.length; i++) {
                    retVal.ships[i] = destroyShipJSONObj(tempArr[i], match.getPendingParticipantId());
                }
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // go to next turn as it moves the current user's ship's
        //      currentPower/Rotation to previousPower/Rotation
        Log.d(TAG, "turn: " + retVal.turn);
        Log.d(TAG, "num humans = " + retVal.numHumanPlayers);
        if((retVal.turn - 1) % retVal.numHumanPlayers == 0) {
            Log.d(TAG, "GT| MATCH CHANGED AND MOVECURTOPREV HAPPENED");

            for (int i = 0; i < retVal.ships.length; i++) {
                retVal.ships[i].moveFutToPrev();
            }
        }

        // Set the ship menus so the active player can change their ship
        for (int i = 0; i < retVal.ships.length; i++) {

            if(retVal.ships[i].userId.equals(match.getPendingParticipantId())) {
                retVal.ships[i].setEnemy(false);
                retVal.ships[i].createMenu();
            } else {
                retVal.ships[i].createMenu();
            }

            // Sets current pow, rot, isdead to prev values
            retVal.ships[i].setVisualDefaults();
        }

        return retVal;
    }

    static private JSONObject createPlanetJSONObj(Planet planet) {
        JSONObject tempObj = new JSONObject();

        try {
            tempObj.put("radius", planet.getRadius());
            tempObj.put("gravity", planet.getGravity());
            tempObj.put("x", planet.getPositionX());
            tempObj.put("y", planet.getPositionY());
        } catch (JSONException e) {
            // TODO Auto-generated catch block?
            e.printStackTrace();
        }

        return tempObj;

    }

    static private Planet destroyPlanetJSONObj(JSONObject planet) {
        Planet tempPlanet = new Planet();

        try {
            if (planet.has("radius")) {
                tempPlanet.setRadius((float) planet.getDouble("radius"));
            }
            if (planet.has("gravity")) {
                tempPlanet.setGravity((float) planet.getDouble("gravity"));
            }
            if (planet.has("x")) {
                tempPlanet.setPositionX((float) planet.getDouble("x"));
            }
            if (planet.has("y")) {
                tempPlanet.setPositionY((float) planet.getDouble("y"));
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return tempPlanet;
    }

    static private JSONObject[] destroyJSONArray(JSONArray array) {
        JSONObject[] tempArr = new JSONObject[array.length()];

        for (int i = 0; i < array.length(); i++) {
            try {
                tempArr[i] = array.getJSONObject(i);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return tempArr;
    }

    static private JSONObject createShipJSONObj(Ship ship) {
        JSONObject tempObj = new JSONObject();

        try {
            //tempObj.put("radius", ship.radius);
            tempObj.put("userId", ship.userId);


            // Next
            tempObj.put("nextRot", ship.getNextRoundRotation());

            tempObj.put("nextX", ship.getNextRoundLocation().x);
            tempObj.put("nextY", ship.getNextRoundLocation().y);

            tempObj.put("nextStartX", ship.getNextRoundStartLocation().x);
            tempObj.put("nextStartY", ship.getNextRoundStartLocation().y);

            tempObj.put("nextLaserPow", ship.getNextRoundLaserPower());
            tempObj.put("nextShieldPow", ship.getNextRoundShieldPower());
            tempObj.put("nextEnginePow", ship.getNextRoundEnginePower());

            tempObj.put("nextDead", ship.isNextRoundDead());
            tempObj.put("nextLSE", ship.getNextRoundLaserShieldEngine());

            // Previous
            tempObj.put("prevRot", ship.getPrevRoundRotation());

            tempObj.put("prevX", ship.getPrevRoundLocation().x);
            tempObj.put("prevY", ship.getPrevRoundLocation().y);

            tempObj.put("prevLaserPow", ship.getPrevRoundLaserPower());
            tempObj.put("prevShieldPow", ship.getPrevRoundShieldPower());
            tempObj.put("prevEnginePow", ship.getPrevRoundEnginePower());

            tempObj.put("prevDead", ship.isPrevRoundDead());
            tempObj.put("prevLSE", ship.getPrevRoundLaserShieldEngine());

        } catch (JSONException e) {
            // TODO Auto-generated catch block?
            e.printStackTrace();
        }

        return tempObj;
    }

    static private Ship destroyShipJSONObj(JSONObject ship, String currentUserId) {
        Ship tempShip = new Ship("Ship");

        try {
            //--------------- Set future values which aren't visible yet -------------
            if (ship.has("nextRot")) {
                tempShip.setNextRoundRotation((float) ship.getDouble("nextRot"));
            }

            if (ship.has("nextX")) {
                tempShip.setNextRoundX((float) ship.getDouble("nextX"));
            }
            if (ship.has("nextY")) {
                tempShip.setNextRoundY((float) ship.getDouble("nextY"));
            }
            if (ship.has("nextStartX")) {
                tempShip.setNextRoundStartX((float) ship.getDouble("nextStartX"));
            }
            if (ship.has("nextStartY")) {
                tempShip.setNextRoundStartY((float) ship.getDouble("nextStartY"));
            }


            if (ship.has("nextLaserPow")) {
                tempShip.setNextRoundLaserPower((float) ship.getDouble("nextLaserPow"));
            }
            if (ship.has("nextShieldPow")) {
                tempShip.setNextRoundShieldPower((float) ship.getDouble("nextShieldPow"));
            }
            if (ship.has("nextEnginePow")) {
                tempShip.setNextRoundEnginePower((float) ship.getDouble("nextEnginePow"));
            }

            if(ship.has("nextDead")) {
                tempShip.setNextRoundDead(ship.getBoolean("nextDead"));
            }
            if(ship.has("nextLSE")) {
                tempShip.setNextRoundLaserShieldEngine(ship.getInt("nextLSE"));
            }

            //--------------- Set previous values which are visible --------------
            if (ship.has("prevRot")) {
                tempShip.setPrevRoundRotation((float) ship.getDouble("prevRot"));
            }

            if (ship.has("prevX")) {
                tempShip.setPrevRoundX((float) ship.getDouble("prevX"));
            }
            if (ship.has("prevY")) {
                tempShip.setPrevRoundY((float) ship.getDouble("prevY"));
            }


            if (ship.has("prevLaserPow")) {
                tempShip.setPrevRoundLaserPower((float) ship.getDouble("prevLaserPow"));
            }
            if (ship.has("prevShieldPow")) {
                tempShip.setPrevRoundShieldPower((float) ship.getDouble("prevShieldPow"));
            }
            if (ship.has("prevEnginePow")) {
                tempShip.setPrevRoundEnginePower((float) ship.getDouble("prevEnginePow"));
            }

            if(ship.has("prevDead")) {
                tempShip.setPrevRoundDead(ship.getBoolean("prevDead"));
            }
            if(ship.has("prevLSE")) {
                tempShip.setPrevRoundLaserShieldEngine(ship.getInt("prevLSE"));
            }

            //------------------------- Set constants -------------------------------
//            if (ship.has("radius")) {
//                tempShip.radius = (float) ship.getDouble("radius");
//            }
            if (ship.has("userId")) {
                tempShip.userId = ship.getString("userId");


                switch (tempShip.userId.charAt(tempShip.userId.length() - 1)) {
                    case '1':
                        tempShip.setColor(Ship.GREEN);
                        break;
                    case '2':
                        tempShip.setColor(Ship.RED);
                        break;
                    case '3':
                        tempShip.setColor(Ship.PURPLE);
                        break;
                    case '4':
                        tempShip.setColor(Ship.ORANGE);
                        break;
                    case '5':
                        tempShip.setColor(Ship.MINT);
                        break;
                    default:
                        break;
                }
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return tempShip;
    }

}
