package com.nomadjackalope.tgem_t47;

/**
 * Created by benjamin on 1/9/17.
 */
public class BasicShader {

    public static final String basicVertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
                    "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "}";

    public static final String basicFragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";

    // Background shader
    public static final String backgroundVertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
//                    "attribute float vRandSeed;" +
//                    "uniform float time;" +
//                    "" +
//                    "vec4 updatePos() {" +
//                    "  return vec4(sin(time + vRandSeed) / 100.0, sin(time + vRandSeed) / 100.0, 0.0, 0.0);" +
//                    "}" +
//                    "" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;"+ //uMVPMatrix * vPosition + updatePos();" +
                    "}" +
                    "" +
                    "";

    public static final String backgroundFragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "" +
                    "float isGridLine(vec2 fragCoord) {" +
                    // Define the size we want each grid square in pixels
                    "   vec2 vPixelsPerGridSquare = vec2(32.0, 32.0);"+

                    // fragCoord is an input to the shader, it defines the pixel co-ordinate of the current pixel
                    "   vec2 vScreenPixelCoordinate = fragCoord.xy;"+

                    // Get a value in the range 0->1 based on where we are in each grid square
                    // fract() returns the fractional part of the value and throws away the whole number part
                    // This helpfully wraps numbers around in the 0->1 range
                    "   vec2 vGridSquareCoords = fract(vScreenPixelCoordinate / vPixelsPerGridSquare);"+

                    // Convert the 0->1 co-ordinates of where we are within the grid square
                    // back into pixel co-ordinates within the grid square
                    "   vec2 vGridSquarePixelCoords = vGridSquareCoords * vPixelsPerGridSquare;"+

                    // step() returns 0.0 if the second parmeter is less than the first, 1.0 otherwise
                    // so we get 1.0 if we are on a grid line, 0.0 otherwise
                    "   vec2 vIsGridLine = step(vGridSquarePixelCoords, vec2(1.0));"+

                    // Combine the x and y gridlines by taking the maximum of the two values
                    "   float fIsGridLine = max(vIsGridLine.x, vIsGridLine.y);"+

                    // return the result
                    "   return fIsGridLine;"+
                    "}" +
                    "" +
                    "void main() {" +
//                    "  gl_FragColor = vColor;" +
                    //---- static grid ----
                    "   vec3 rgb = vec3(isGridLine(gl_FragCoord.xy));"+
                    "   gl_FragColor = vec4(rgb * vColor.rgb, vColor.a);"+
                    "}";

//    public static final String backgroundFragmentShaderCode =
//            "precision mediump float;" +
//            "const int iterations = 17;"+
//            "const float formuparam = 0.53;"+
//
//            "const int volsteps = 20;"+ //20
//            "const float stepsize = 0.1;"+
//
//            "const float zoom = 0.800;"+
//            "const float tile = 0.850;"+
//            "const float speed = 0.010;"+
//
//            "const float brightness = 0.0015;"+
//            "const float darkmatter = 0.300;"+
//            "const float distfading = 0.730;"+
//            "const float saturation = 0.850;"+
//
//    "void main()"+
//    "{" +
//            "vec2 fragCoord = vec2(gl_FragCoord.xy);"+
//        //get coords and direction
//          "vec2 uv = fragCoord.xy/100.0;"+  //        "vec2 uv=fragCoord.xy/iResolution.xy-.5;"+
////        "uv.y*=iResolution.y/iResolution.x;"+
//           "vec3 dir=vec3(uv*zoom,1.0);"+
//////        "float time=iGlobalTime*speed+.25;"+
////
//        "vec3 from=vec3(1.0,0.5,0.5);"+
////
////        //volumetric rendering
//        "float s=0.1,fade=1.0;"+
//        "vec3 v=vec3(0.0);"+
//        "for (int r=0; r<volsteps; r++) {"+
//        "    vec3 p=from+s*dir*0.5;"+
//        "    p = abs(vec3(tile)-mod(p,vec3(tile*2.0)));"+ // tiling fold
//        "    float pa,a=pa=0.0;"+
//        "    for (int i=0; i<iterations; i++) {"+
//        "        p=abs(p)/dot(p,p)-formuparam;"+ // the magic formula
//        "        a+=abs(length(p)-pa);"+ // absolute sum of average change
////        "        pa=length(p);"+
//        "    }"+
////        "    float dm=max(0.0,darkmatter-a*a*0.001);"+ //dark matter
//        "    a*=a*a;"+ // add contrast
////        "    if (r>6) fade*=1.0-dm;"+ // dark matter, don't render near
////        "    //v+=vec3(dm,dm*0.5,0.0);"+
////        "    v+=fade;"+
//        "    v+=vec3(s,s*s,s*s*s*s)*a*brightness*fade;"+ // coloring based on distance
//        "    fade*=distfading;"+ // distance fading
//        "    s+=stepsize;"+
//        "}"+
////        "v=mix(vec3(length(v)),v,saturation);"+ //color adjust
//        "gl_FragColor = vec4(v*0.01,1.0);"+
////            "gl_FragColor = vec4(0.4, 0.4, v.r*0.01, 1.0);"+
//    "}";

//    public static final String backgroundFragmentShaderCode =
//            "precision mediump float;" +
//                    "float Noise2d(vec2 x) {" +
//                    "   float xhash = cos(x.x * 37.0);" +
//                    "   float yhash = cos(x.y * 57.0);" +
//                    "   return fract(415.92653 * (xhash + yhash));" +
//                    "}" +
//                    "void main() {" +
//                    "   float noiseRet = 1.0 - step(Noise2d(vec2(gl_FragCoord.xy)), 0.99);" +
//                    "   gl_FragColor = vec4(noiseRet, noiseRet, noiseRet, 1.0);" +
//                    "}";

    public static final String glowVertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "}";

    public static final String glowFragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  float dist = distance(" +
                    "  gl_FragColor = vColor;" +
                    "}";

    //--------------------- Texture shader -------------------------------
    public static final String textureVertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "attribute vec2 texCoordIn;" +
                    "varying vec2 texCoordOut;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "  texCoordOut = texCoordIn;" +
                    "}";

    public static final String textureFragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "uniform sampler2D texture;" +
                    "varying vec2 texCoordOut;" +
                    "void main() {" +
                    "  gl_FragColor = vColor * texture2D(texture, texCoordOut);" +
                    "}";

    //--------------------- Barymetric shader -------------------------------
    public static final String barymetricVertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "attribute vec2 baryCoordIn;" +
                    "varying vec2 baryCoordOut;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "baryCoordOut = baryCoordIn;" +
                    "}";

    public static final String barymetricFragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "uniform sampler2D texture;" +
                    "varying vec2 baryCoordOut;" +
                    "void main() {" +
                    "  float dist = floor(max(abs(0.5 - baryCoordOut.x), abs(0.5 - baryCoordOut.y)) + 0.51);" +
                    "  gl_FragColor = vColor * dist;" +
                    "}";

    //--------------------- Batch Texture shader -------------------------------
    public static final String batchTextureVertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
                    "attribute vec4 vPosition;" +
                    "attribute vec4 vColorIn;" +
                    "varying vec4 vColorOut;" +
                    "void main() {" +
                    "  gl_Position = vPosition;" +
                   // "  vColorOut = vColorIn;" +
                    "}";

    public static final String batchTextureFragmentShaderCode =
            "precision mediump float;" +
                    "varying vec4 vColorOut;" +
                    "void main() {" +
                    "  gl_FragColor = vec4(0.5, 0.83, 0.7, 1.0);" +
                    "}";

    //--------------------- Batch Texture shader -------------------------------
    public static final String batchVertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "uniform mat4 u_MVPMatrix[];" +
                    "attribute vec4 vColorIn;" +
                    "varying vec4 vColorOut;" +
                    "void main() {" +
                    "  gl_Position = vPosition;" +
                    // "  vColorOut = vColorIn;" +
                    "}";

    public static final String batchFragmentShaderCode =
            "precision mediump float;" +
                    "varying vec4 vColorOut;" +
                    "void main() {" +
                    "  gl_FragColor = vec4(0.5, 0.83, 0.7, 1.0);" +
                    "}";

    // Copied from d3alek/Texample2 @ github
    public static final String fontVertexShaderCode =
            "uniform mat4 u_MVPMatrix[36];      \n"     // An array representing the combined
                    // model/view/projection matrices for each sprite
                    // This is an array of mat4 objects which are 4x4 matrices

                    + "attribute float a_MVPMatrixIndex; \n"	// The index of the MVPMatrix of the particular sprite
                    + "attribute vec4 a_Position;     \n"     // Per-vertex position information we will pass in.
                    + "attribute vec2 a_TexCoordinate;\n"     // Per-vertex texture coordinate information we will pass in
                    + "varying vec2 v_TexCoordinate;  \n"   // This will be passed into the fragment shader.
                    + "void main()                    \n"     // The entry point for our vertex shader.
                    + "{                              \n"
                    + "   int mvpMatrixIndex = int(a_MVPMatrixIndex); \n"
                    + "   v_TexCoordinate = a_TexCoordinate; \n"
                    + "   gl_Position = u_MVPMatrix[mvpMatrixIndex]   \n"     // gl_Position is a special variable used to store the final position.
                    + "               * a_Position;   \n"     // Multiply the matrix(mat4) by the vertex(vec4) to get the final point(vec4) in
                    // normalized screen coordinates.
                    + "}                              \n";


    public static final String fontFragmentShaderCode =
            "uniform sampler2D u_Texture;       \n"    // The input texture.
                    +	"precision mediump float;       \n"     // Set the default precision to medium. We don't need as high of a
                    // precision in the fragment shader.
                    + "uniform vec4 u_Color;          \n"
                    + "varying vec2 v_TexCoordinate;  " +
                    "const float smoothing = 1.0/16.0;" // Interpolated texture coordinate per fragment.

                    + "void main()                    \n"     // The entry point for our fragment shader.
                    + "{" +
//                    "   vec4 tex = texture2D(u_Texture, v_TexCoordinate);" +
                    "   float distance = texture2D(u_Texture, v_TexCoordinate).a;" +
                    "   float alpha = smoothstep(0.5 - smoothing, 0.5 + smoothing, distance);" +
                    "   gl_FragColor = vec4(u_Color.rgb, u_Color.a * alpha);"+ //vec4(0.4, 0.4, alpha, 0.4);"+
                    //"   gl_FragColor = texture2D(u_Texture, v_TexCoordinate).w * u_Color;\n" // texture is grayscale so take only grayscale value from
                    // it when computing color output (otherwise font is always black)
                    "}";

    public static final String myFontVertexShaderCode = textureVertexShaderCode;

    public static final String myFontFragmentShaderCode =
            "precision mediump float;" +
                    "uniform sampler2D texture;" +
                    "uniform vec4 vColor;" +
                    "varying vec2 texCoordOut;" +
                    "const float smoothing = 0.42/15.0;" +
                    "void main() {" +
                    "   vec4 tex = texture2D(texture, texCoordOut);" +
                    "   float alpha = smoothstep(0.5 - smoothing, 0.5 + smoothing, tex.a) * vColor.a;" +
                    "   tex *= vColor;" +
                    "   gl_FragColor = vec4(tex.rgb, alpha);" +
                    "}";



    // Copied from d3alek/Texample2 @ github
    public static final String particleVertexShaderCode =
        "uniform mat4 u_MVPMatrix;      \n"     // An array representing the combined
                // model/view/projection matrices for each sprite

                + "attribute float a_MVPMatrixIndex; \n"	// The index of the MVPMatrix of the particular sprite
                + "attribute vec4 a_Position;     \n"     // Per-vertex position information we will pass in.
                + "attribute vec2 a_TexCoordinate;\n"     // Per-vertex texture coordinate information we will pass in
                + "varying vec2 v_TexCoordinate;  \n"   // This will be passed into the fragment shader.
                + "void main()                    \n"     // The entry point for our vertex shader.
                + "{                              \n"
                //+ "   int mvpMatrixIndex = int(a_MVPMatrixIndex); \n"
                + "   v_TexCoordinate = a_TexCoordinate; \n"
                + "   gl_Position = u_MVPMatrix   \n"//[mvpMatrixIndex]   \n"     // gl_Position is a special variable used to store the final position.
                + "               * a_Position;   \n"     // Multiply the vertex by the matrix to get the final point in
                // normalized screen coordinates.
                + "}                              \n";


    public static final String particleFragmentShaderCode =
            "uniform sampler2D u_Texture;       \n"    // The input texture.
                    +	"precision mediump float;       \n"     // Set the default precision to medium. We don't need as high of a
                    // precision in the fragment shader.
                    + "uniform vec4 u_Color;          \n"
                    + "varying vec2 v_TexCoordinate;  "
                    //+ "const float smoothing = 1.0/16.0;" // Interpolated texture coordinate per fragment.

                    + "void main()                    \n"     // The entry point for our fragment shader.
                    + "{" +
                    "   vec4 tex = texture2D(u_Texture, v_TexCoordinate);" +
                    //"   float distance = texture2D(u_Texture, v_TexCoordinate).a;" +
                    //"   float alpha = smoothstep(0.5 - smoothing, 0.5 + smoothing, distance);" +
                    "   gl_FragColor = tex * u_Color;"//vec4(u_Color.rgb, u_Color.a * alpha);"+ //vec4(0.4, 0.4, alpha, 0.4);"+
                    //"   gl_FragColor = texture2D(u_Texture, v_TexCoordinate).w * u_Color;\n" // texture is grayscale so take only grayscale value from
                    // it when computing color output (otherwise font is always black)
                    + "}";
}
