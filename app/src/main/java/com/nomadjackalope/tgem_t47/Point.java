package com.nomadjackalope.tgem_t47;

/**
 * Created by benjamin on 1/23/17.
 *
 *  A point in three-dimensional Euclidean space
 */
public class Point {

    public final static Point ZERO = new Point();

    float x;
    float y;
    float z;

    public Point() {
        x = 0;
        y = 0;
        z = 0;
    }

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
        z = 0;
    }

    public Point(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point(Point p) {
        this.x = p.x;
        this.y = p.y;
        this.z = p.z;
    }

    public float distance(Point other) {
        float deltaX = other.x - this.x;
        float deltaY = other.y - this.y;
        return (float) Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));
    }

    public void set(float x, float y, float z) {
        this.x = x;
        this.y = y;
    }
}
