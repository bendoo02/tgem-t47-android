package com.nomadjackalope.tgem_t47;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Debug;
import android.os.Handler;
import android.test.suitebuilder.TestMethod;
import android.text.method.Touch;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

import com.google.android.gms.drive.internal.StreamContentsRequest;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.event.Event;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;

import org.w3c.dom.Text;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import java.util.Observable;
import java.util.Random;
import java.util.function.Function;

/**
 * Created by benjamin on 1/4/17.
 * Only view, handles all touch input
 */
public class MyGLSurfaceView extends GLSurfaceView {

    public final MyGLRenderer mRenderer;

    ArrayList<Node> nodesTouched = new ArrayList<>();

    MainActivity mAContext;

    public Node rootNode;
//    public Node background;

    // Touches
    private ArrayList<Node> activeNode = new ArrayList<>();
    private Node curNode;
    private boolean zooming = false;

    public Menu shipMenu;
    public Menu mainMenu;

    public boolean simRunning;

    private int gameState = -1;
    private int mainState = -1;

    private float prevFingerDistance;
    private Point prevFinger1 = new Point();
    private Point prevFinger2 = new Point();
    private Point finger1 = new Point();
    private Point finger2 = new Point();
    private Point finger3 = new Point();

    private ZComparator zComparator = new ZComparator();

    private TouchListener mainMenuTL = new TouchListener() {
        Point currentFinger = new Point();
        Point prevFinger = new Point();
        int moved = -1;
        @Override
        public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
//                System.out.println("MGLSV| rootNode onDown");
            setFingerGameCoords(prevFinger, event.getX(), event.getY());
            moved = 0;
            return true;
        }

        @Override
        public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
            if(moved < 0) {
                return true;
            }
            moved++;

            setFingerGameCoords(currentFinger, event.getX(), event.getY());

            float deltaX = currentFinger.x - prevFinger.x;
            float deltaY = currentFinger.y - prevFinger.y;

//                System.out.println("MGLSV| delta x,y: " + deltaX + ", " + deltaY);

            mRenderer.translateView(deltaX, deltaY);

            prevFinger.set(currentFinger.x, currentFinger.y, 0);
            return true;
        }

        @Override
        public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
            if(moved < 3 && moved >= 0) {
                mainMenu.toggle(realPoint, 1);
            }
            moved = -1;
            return true;
        }
    };

    Random random = new Random();

    public static boolean activeAction = false;

    public MyGLSurfaceView(Context context) {
        super(context);

        mAContext = (MainActivity) context;

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);

        // Set the Renderer for drawing on the GLSurfaceView
        mRenderer = new MyGLRenderer(this);
        setRenderer(mRenderer);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        createRootNode();

        // Create main scene
//        setMainState(STATE_INTRO_SCENE);
        setMainState(STATE_MAIN_MENU);
//        setMainState(STATE_TUTORIAL_SCENE);

        updateMainState();

        requestRender();

    }

    private void createRootNode() {
        rootNode = new Node("Root node");
        rootNode.setScale(40, 30);
        rootNode.setPosition(0,0,-10);
        rootNode.baseShape.color = new float[]{0.12f, 0.12f, 0.93f, 0.25f}; // 0.25 alpha
        rootNode.material = MyGLRenderer.backgroundMat;
    }

    public final static String DRAW_LOCK = "DRAW";
    public final static String EDIT_LOCK = "EDIT";
    public final static String UNLOCKED = "UNLOCKED";
    private volatile String nodesLocked = UNLOCKED;

    long simulationTimer = 0;
    long simDeltaTime = 0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {

//        if (event.getAction() == MotionEvent.ACTION_DOWN) {
//            createBackground();
//        }

        // Only handling touches if there are 1 or two fingers down
//        System.out.println("MGLSV| Num Pointers: " + event.getPointerCount());

//        System.out.println("MA| onTouchEvent, simRunning" + simRunning);
        if(event.getPointerCount() == 2) { // && !simRunning
            // Scale viewport
            if(event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN) {
                zooming = true;

                setFingerGameCoords(finger1, event.getX(0), event.getY(0));

                setFingerGameCoords(finger2, event.getX(1), event.getY(1));

                prevFingerDistance = finger1.distance(finger2);
                System.out.println("MGLSV| initialFingerDistance: " + prevFingerDistance);
                prevFinger1.x = finger1.x;
                prevFinger1.y = finger1.y;
                prevFinger2.x = finger2.x;
                prevFinger2.y = finger2.y;


            } else if(event.getActionMasked() == MotionEvent.ACTION_POINTER_UP) {
                // Assuming that on action_pointer_up when there are 3 fingers, count == 3
                //  so this won't deal with that



            } else if(event.getActionMasked() == MotionEvent.ACTION_MOVE) {
                // Action_move so we scale

                setFingerGameCoords(finger1, event.getX(0), event.getY(0));

                setFingerGameCoords(finger2, event.getX(1), event.getY(1));




//                float a = prevFinger1.x - finger1.x;
//                float b = prevFinger2.x - finger2.x;
//                float c = prevFinger1.y - finger1.y;
//                float d = prevFinger2.y - finger2.y;


                // TODO make translation and scaling more natural and smoother
                // Translate view
                //if(((a < 0 && b < 0) || (a > 0 && b > 0)) || ((c < 0 && d < 0) || (c > 0 && d > 0))) {
//                    float e;
//                    float f;
//
//                    if(Math.abs(a) < Math.abs(b)) {
//                        e = a;
//                    } else {
//                        e = b;
//                    }
//
//                    if(Math.abs(c) < Math.abs(d)) {
//                        f = c;
//                    } else {
//                        f = d;
//                    }
//
//                    //mRenderer.translateView(e, f);
               // } else {
                    mRenderer.scaleFromDist(prevFingerDistance - finger1.distance(finger2));
               // }




                prevFingerDistance = finger1.distance(finger2);

                prevFinger1.x = finger1.x;
                prevFinger1.y = finger1.y;
                prevFinger2.x = finger2.x;
                prevFinger2.y = finger2.y;

            }


        } else if (event.getPointerCount() == 1 && !simRunning) {
            System.out.println("MGLSV| MotionEvent: " + event.getAction());

            if ((event.getAction() == MotionEvent.ACTION_DOWN
                    || event.getAction() == MotionEvent.ACTION_MOVE
                    || event.getAction() == MotionEvent.ACTION_UP)
                    ) {

                //float mid = 640f;


                // Testing touching points in rectangles
//            Point[] simulatedPoints = new Point[] {new Point(mid, conv(0)), new Point(mid, conv(-8)),
//                    new Point(mid, conv(-8.48f)), new Point(mid, conv(-7.52f)) };
//
//            for (int i = 0; i < simulatedPoints.length; i++) {

//                Point simulatedPoint = simulatedPoints[i];

                if(event.getAction() == MotionEvent.ACTION_UP && zooming) {
                    zooming = false;
                } else if(!zooming) {
                    Point realPoint = convertCoords(event);

                    touchHighestZ(event, trawlNodesForTouches(realPoint), realPoint);
                }
//            }

            }
        } else if(event.getPointerCount() == 1 && simRunning) { // just want to be able to move the screen around
            if ((event.getAction() == MotionEvent.ACTION_DOWN
                    || event.getAction() == MotionEvent.ACTION_MOVE
                    || event.getAction() == MotionEvent.ACTION_UP)
                    ) {

                if(event.getAction() == MotionEvent.ACTION_UP && zooming) {
                    zooming = false;
                } else if(!zooming) {
                    Point realPoint = convertCoords(event);

                    nodesTouched.clear();
                    nodesTouched.add(rootNode);

                    touchHighestZ(event, nodesTouched, realPoint);
                }

            }
        }

        requestRender();

        // ----------- TEST CODE -------------------------

//        float[] fakeComb = new float[] {
//                1.0f, 1.0f, 1.0f, 1.0f,
//                0.0f, 0.0f, 0.0f, 0.0f,
//                2.0f, 2.0f, 2.0f, 2.0f
//        };
//
//        float[] fakeTexCoords = new float[] {
//                0.0f, 0.1f,
//                0.2f, 0.3f,
//                1.0f, 1.0f
//        };
//
//        int numTris = 2;
//        int BYTES_PER_FLOAT = 4;
//
//        ByteBuffer testBb = ByteBuffer.allocateDirect(
//                fakeComb.length * BYTES_PER_FLOAT * numTris +
//                fakeTexCoords.length * BYTES_PER_FLOAT * numTris);
//
//        testBb.order(ByteOrder.nativeOrder());
//
//        FloatBuffer test = testBb.asFloatBuffer();
//
//        test.position(0);
//        // 2 triangles
//        for (int i = 0; i < numTris; i++) {
//
//            for (int j = 0; j < 3; j++) {
//                // 3 verts per triangle
//                test.put(fakeComb, j * fakeComb.length / 3, 4);
//                test.put(fakeTexCoords, j * fakeTexCoords.length / 3, 2);
//            }
//
//        }

        return true;
    }

    //TEMP // Changes screen coords
    public float conv(float y) {
        float scale = 20f; // Number of units from - to + Y on screen
        float pixels = 752f; // Number of pixels on Y axis on screen

        return (y - 10f) / -scale * pixels;
    }

    // Calls root node onTouch which calls all other nodes onTouch and returns which ones were touched
    private ArrayList<Node> trawlNodesForTouches(Point position) {
        nodesTouched.clear();
        return rootNode.checkTouch(position, nodesTouched);
    }

    // This calls the listeners essentially?
    // Sorts the list by Z order and then calls the nodes' onTouch method until one returns true indicating the touch was handled
    private void touchHighestZ(MotionEvent event, ArrayList<Node> tempNodesTouched, Point realPoint) {
        // Sort tempNodesTouched by their Z value
        Collections.sort(tempNodesTouched, zComparator);

        for (Node node : tempNodesTouched) {
            System.out.println("MGLSV| node z: " + node.getName() + ", " + node.getPositionZ());
        }

        // Action down will find which node needs to be called, action move and action up will call that same node, the activeNode
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            // Runs through the array as long as it returns false
            // If onTouch returns true the touch has been handled
            for (int i = 0; i < tempNodesTouched.size(); i++) {

                while(!requestHold(EDIT_LOCK)) {
                    Thread.yield();
                }

                curNode = tempNodesTouched.get(i);

                activeNode.add(curNode);

                if(curNode.onTouchDown(event, realPoint, curNode)) {
//                    activeNode.clear();
//                    activeNode.add(curNode);
                    i = tempNodesTouched.size();
                }

                requestHold(UNLOCKED);
            }
        } else if(event.getAction() == MotionEvent.ACTION_MOVE) {
            if(activeNode.size() > 0) {
                while(!requestHold(EDIT_LOCK)) {
                    Thread.yield();
                }

                for(int i = 0; i < activeNode.size(); i++) {

                    curNode = activeNode.get(i);

                    if(curNode.onTouchMove(event, realPoint, curNode)) {
//                        activeNode.clear();
//                        activeNode.add(curNode);
                        i = activeNode.size();
                    }
                }

                requestHold(UNLOCKED);
            }
        } else if(event.getAction() == MotionEvent.ACTION_UP) {
            if(activeNode.size() > 0) {
                while(!requestHold(EDIT_LOCK)) {
                    Thread.yield();
                }

                // Call all activeNodes unless one returns true
                for (int i = 0; i < activeNode.size(); i++) {

                    curNode = activeNode.get(i);

                    if (curNode.onTouchUp(event, realPoint, curNode)) {
//                        activeNode.clear();
//                        activeNode.add(curNode);
                        i = activeNode.size();
                    }
                }

                activeNode.clear();

                requestHold(UNLOCKED);
            }
        }

        requestRender();
    }

    public Point convertCoords(MotionEvent event) {
//        System.out.println("MGLSV| touch1 x,y: " + event.getX() + ", " + event.getY());

        float[] realCoords = SimpleTouch2GLCoord(event.getX(), event.getY());    //new Point((int) event.getX(), (int)  event.getY()));
//        System.out.println("MGLSV| touch2 x,y: " + realCoords[0] + ", " + realCoords[1]);

        realCoords = mRenderer.glCoordinates(realCoords[0], realCoords[1]);
//        System.out.println("MGLSV| touch3 x,y: " + realCoords[0] + ", " + realCoords[1]);

        return new Point(realCoords[0], realCoords[1]);
    }

    public Point convertCoords(Point point) {
        System.out.println("MGLSV| touch1 x,y: " + point.x + ", " + point.y);

        float[] realCoords = SimpleTouch2GLCoord(point.x, point.y);    //new Point((int) event.getX(), (int)  event.getY()));
        System.out.println("MGLSV| touch2 x,y: " + realCoords[0] + ", " + realCoords[1]);

        realCoords = mRenderer.glCoordinates(realCoords[0], realCoords[1]);
        System.out.println("MGLSV| touch3 x,y: " + realCoords[0] + ", " + realCoords[1]);

        return new Point(realCoords[0], realCoords[1]);
    }

    // Android specific!
    // Normalized to left -1 right 1 top 1 bottom -1
    public float[] SimpleTouch2GLCoord(float touchX, float touchY) {
//        Display display = mAContext.getWindowManager().getDefaultDisplay();
//        android.graphics.Point size = new android.graphics.Point();
//        display.getSize(size);

        Point size = mRenderer.glViewportSize;

        if(size == null) {
            Log.e("MGLSV", "ERROR!! glViewportSize == null");
            return new float[3];
        }

        float screenW = size.x;
        float screenH = size.y;

        float normalizedX = 2f * touchX/screenW - 1f;
        float normalizedY = 1f - 2f * touchY/screenH;
        float normalizedZ = 0.0f;

        return (new float[]{normalizedX, normalizedY, normalizedZ});

    }


    public void setFingerGameCoords(Point finger, float eventX, float eventY) {
        float[] realCoords = SimpleTouch2GLCoord(eventX, eventY);
        //realCoords = mRenderer.glCoordinates(realCoords[0], realCoords[1]);
        finger.x = realCoords[0];
        finger.y = realCoords[1];
    }

    // ----------------------- APP STATES --------------------------------

    // Main state
    public static final int STATE_MAIN_MENU = 0;
    public static final int STATE_GAME = 1;
    public static final int STATE_GAME_GEN = 2;
    public static final int STATE_TEST_SCENE = 3;
    public static final int STATE_INTRO_SCENE = 4;
    public static final int STATE_TUTORIAL_SCENE = 5;

    private void createIntroScene() {
        System.out.println("MGSLV| creating Intro Scene");
        rootNode.removeAllChildren();
        rootNode.setVisible(false);

        final TextNode node1 = new TextNode("intro line1");
        node1.setAlign(TextNode.LEFT);
        node1.setScale(0.7f);
        //node1.setAnchorPoint(new Point(0, 1));
        node1.setMaxWidth(10);
        node1.setPositionInNormCoords(true);
        node1.setPosition(-0.95f, 0.95f);

        node1.setText("Booting TGEM-T7 Terminal v. 1.4.2 ...");//getResources().getString(R.string.introLine1));

        float timing = 1000;

        Action preAction = new Action(node1, timing);


        ActionChangeText action = new ActionChangeText(node1.getText(), getResources().getString(R.string.introLine2), timing, node1);
        ActionChangeText action1 = new ActionChangeText(action.getFinalText(), getResources().getString(R.string.introLine3), timing, node1);

        String string4 = getResources().getString(R.string.introLine4);
        if(mAContext.isMuted()) {
            string4 += " " + getResources().getString(R.string.introLine4b);
        } else {
            string4 += " " +  getResources().getString(R.string.introLine4a);
        }

        ActionChangeText action2 = new ActionChangeText(action1.getFinalText(), string4, timing, node1);

        // If (user has not used this game before) {
        ActionChangeText action3 = new ActionChangeText(action2.getFinalText(), getResources().getString(R.string.introLine5), timing, node1);

        Action action4 = new Action(node1, timing);
        action4.setRunAtPercent(new ActionRunnable() {
            @Override
            public boolean run() {
                rootNode.setVisible(true);
                node1.setText(getResources().getString(R.string.tutorialLine1c));

                TextNode yes = new TextNode(getResources().getString(R.string.yes));
                TextNode no = new TextNode(getResources().getString(R.string.no));

                yes.setPosition(-2, 0);
                no.setPosition(2, 0);

                yes.addTouchListener(new TouchListener() {
                    @Override
                    public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                        setMainState(STATE_TUTORIAL_SCENE);
                        mAContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateMainState();
                            }
                        });
                        return true;
                    }

                    @Override
                    public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                        return false;
                    }

                    @Override
                    public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                        return false;
                    }
                });
                no.addTouchListener(new TouchListener() {
                    @Override
                    public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                        setMainState(STATE_MAIN_MENU);
                        mAContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateMainState();
                            }
                        });
                        return true;
                    }

                    @Override
                    public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                        return false;
                    }

                    @Override
                    public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                        return false;
                    }
                });

                rootNode.addChild(yes);
                rootNode.addChild(no);

                return true;
            }
        }, 1.0f);

        action2.setNextAction(action3);
        action3.setNextAction(action4);

        //} else {
//
        //}

        preAction.setNextAction(action);
        action.setNextAction(action1);
        action1.setNextAction(action2);


        node1.addAction(preAction);



        rootNode.addChild(node1);

    }

    private void createGameScene() {
        System.out.println("MGLSV| creating Game Scene");
        rootNode.removeAllChildren();

        // Clear any previous planets or ships
        ships = new ArrayList<>();
        planets = new ArrayList<>();


        for (int i = 0; i < mAContext.mTurnData.planets.length; i++) {
            rootNode.addChild(mAContext.mTurnData.planets[i]);// new Planet("", turn.planets[i].getPositionX(), turn.planets[i].getPositionY()));
            planets.add((Planet) rootNode.getChild(rootNode.getChildren().size() - 1));
        }

        for (int i = 0; i < mAContext.mTurnData.ships.length; i++) {

//            turn.ships[i].addTouchListener(new TouchListener() {
//                @Override
//                public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
//                    Menu menu = ((Ship) sender).getMenu();
//                    menu.toggle(sender.getPosition(), sender.getScaleX());
//                    return true;
//                }
//
//                @Override
//                public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
//                    return false;
//                }
//
//                @Override
//                public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
//                    return false;
//                }
//            });

            rootNode.addChild(mAContext.mTurnData.ships[i]);//new Ship("Ship" + i, turn.ships[i].getPositionX(), turn.ships[i].getPositionY()));
            ships.add((Ship) rootNode.getChild(rootNode.getChildren().size() - 1));

            ships.get(ships.size() - 1).updateBaseShapeMatrix(mRenderer);
        }

        // Play simulation
        if(mAContext.mTurnData.simulate) {
            // Reset any previous simulation before we do our thing
            simulateRuns = 0;
            lasersStillAlive = 0;
            simulateShouldContinue = true;
            startSimThread(); //simulate();
        }
    }

    private void createMainScene() {
        System.out.println("MGLSV| creating Main Scene");
        rootNode.removeAllChildren();

        // Create main Menu
        mainMenu = new Menu("Main menu", 0, 0);


        TextNode startMatch = new TextNode("Initiate battle");

        startMatch.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
//                mAContext.onQuickMatchClicked();

//                updateMainState(STATE_GAME_GEN);
                mainMenu.close();
                mAContext.onStartMatchClicked();
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        TextNode checkMatch = new TextNode("Check battle");

        checkMatch.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                mainMenu.close();
                mAContext.onCheckGamesClicked();
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        TextNode singlePlayerScene = new TextNode("Training");

        singlePlayerScene.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                mainMenu.close();
                mAContext.createSinglePlayerGame(new GameTurn(1,1,2,0,random.nextInt(8)+2));
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        TextNode muteMusic = new TextNode("mute");

        muteMusic.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                if(mAContext.isMuted()) {
                    mAContext.unmuteMusic();
                    setMuteString((TextNode)sender);
                } else {
                    mAContext.muteMusic();
                    setMuteString((TextNode)sender);
                }
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        setMuteString(muteMusic);


        if(!mAContext.isSignedIn) {
            TextNode signIn = new TextNode("Google sign in");
            signIn.addTouchListener(new TouchListener() {
                boolean touchedDown = false;

                @Override
                public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                    touchedDown = true;
                    return true;
                }

                @Override
                public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                    return false;
                }

                @Override
                public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                    if(touchedDown) {
                        mAContext.signIn();
                    }
                    return true;
                }
            });

            mainMenu.addChild(signIn);


        } else {
            mainMenu.addChild(startMatch);
            mainMenu.addChild(checkMatch);
        }

        mainMenu.addChild(singlePlayerScene);
        mainMenu.addChild(muteMusic);

        rootNode.addChild(mainMenu);
        rootNode.addTouchListener(new TouchListener() {
            int movement = 0;
            Point prevPoint = new Point();
            Point curPoint = new Point();
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                movement = 0;
                prevPoint.set(event.getX(), event.getY(), 0);
                return false;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                curPoint.set(event.getX(), event.getY(), 0);
                System.out.println("MGLSV| distance from point down: " + curPoint.distance(prevPoint));
                if(curPoint.distance(prevPoint) > 4) { //TODO scale this for smaller phones vs large tablets
                    movement++;
                }

                if(movement > 3) {

//                    System.out.println("MGLSV| curpoint prevpoint: " + curPoint.x + ", " + curPoint.y + ", " + prevPoint.x + ", " + prevPoint.y);
                    mRenderer.translateView((curPoint.x - prevPoint.x) / 1000,
                            -(curPoint.y - prevPoint.y) / 1000);
                    prevPoint.set(curPoint.x, curPoint.y, 0);
                    return true;
                }
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                if(movement < 3 && !simRunning) {
                    mainMenu.toggle(realPoint, 1f);
                    return true;
                }
                return false;
            }
        });

        requestRender();


    }

    private void createGameGenerationScene() {
        System.out.println("MGLSV| creating Game Gen Scene");
        rootNode.removeAllChildren();

        Node addPlayer = new Node("Add player");
        addPlayer.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                mAContext.loadFriends(false, false, 0);
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        rootNode.addChild(addPlayer);
    }

    private void createTutorialScene() {
        System.out.println("MGSLV| creating Tutorial Scene");
        rootNode.removeAllChildren();

//        TextNode tnode = new TextNode("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
//        rootNode.addChild(tnode);


        final TextNode textNode = new TextNode("tut textnode");
        textNode.setPositionInNormCoords(true);
        textNode.setPosition(-0.95f, 0.95f);
        textNode.setScale(0.7f);
        textNode.setMaxWidth(40);
        textNode.setAlign(TextNode.LEFT);
        textNode.setText(getResources().getString(R.string.tutorialLine2));

        final ActionChangeText action = new ActionChangeText("", getResources().getString(R.string.tutorialLine2), 2000, textNode);
        ActionChangeText action1 = new ActionChangeText("", getResources().getString(R.string.tutorialLine3), 2000, textNode);

        textNode.addAction(action);
        action.setNextAction(action1);

        Ship ship = new Ship("tutship");
        ship.setEnemy(false);
        ship.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, final Node sender) {
                Menu menu = ((Ship) sender).getMenu();
                if(menu == null) {
                    ((Ship) sender).createMenu();
                    menu = ((Ship) sender).getMenu();
                }
                menu.toggle(sender.getPosition(), sender.getScaleX());
                ActionChangeText action2 = new ActionChangeText("", getResources().getString(R.string.tutorialLine4), 4000, textNode);
                ActionChangeText action3 = new ActionChangeText("", getResources().getString(R.string.tutorialLine5), 2000, textNode);

                action3.setRunAtPercent(new ActionRunnable() {
                    @Override
                    public boolean run() {
//                        ((Ship) sender).getMenu().
                        return true;
                    }
                }, 1.0f);


                textNode.removeAllActions();
                textNode.addAction(action2);
                action2.setNextAction(action3);
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        ship.addObserver(textNode);

        textNode.addGameEvent(new GameEvent() {
            @Override
            public void run(Node n, Object arg) {
                if(arg == "onTouchDown") {
                    ((TextNode)n).setText("Congrats!!!! You found the ship!");
                }
            }
        });


        addShip(ship);


        rootNode.addChild(textNode);


    }

    private void createTestScene() {
        System.out.println("MGLSV| creating Test Scene");
        rootNode.removeAllChildren();

        Planet planet = new Planet("Planet", 0f, 0);
        planet.setRadius(2.5f);
        planet.setMass(100f);

        Planet p2 = new Planet("Planet2", 7.5f, 0);
        p2.setRadius(2.5f);
        p2.setMass(100f);

        addPlanet(planet);
        //addPlanet(p2);

        Ship ship = new Ship("Ship", 0, 5);
        addShip(ship);
        ship.setEnemy(false);
        ship.createMenu();

//        Node testBary = new Node("Bary rect");
//        testBary.setScale(5);
//        testBary.baseShape = new Barymetric(testBary.getName());
//        testBary.baseShape.color = new float[] {1.0f, 0.70588f, 0.13725f, 1.0f};
//        testBary.material = MyGLRenderer.baryMat;


        mainMenu = new Menu("Test game menu", 0, 0);
        mainMenu.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                toggleMainMenu(realPoint);
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        Sprite backToMainMenu = new Sprite("Go back to main menu", 0f, 0f);
        backToMainMenu.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                setMainState(STATE_MAIN_MENU);
                updateMainState();
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        mainMenu.addChild(backToMainMenu);
        rootNode.addChild(mainMenu);
        //rootNode.addChild(testBary);

        //simulate();

    }


    public void updateMainState(int mainState, int gameState) {
        setMainState(mainState);
        setGameState(gameState);

        updateMainState();
    }

    public void updateMainState(int mainState) {
        setMainState(mainState);

        updateMainState();
    }

    public void updateMainState() {
        while(!requestHold(EDIT_LOCK)) {
            Thread.yield();
        }

        switch (mainState) {
            case STATE_MAIN_MENU:
                createMainScene();
                break;
            case STATE_GAME:
                createGameScene();

                updateGameState();
                break;
            case STATE_GAME_GEN:
                createGameGenerationScene();
                break;
            case STATE_TEST_SCENE:
                createTestScene();
                break;
            case STATE_INTRO_SCENE:
                createIntroScene();
                break;
            case STATE_TUTORIAL_SCENE:
                createTutorialScene();
                break;
            default:
                break;
        }

        requestHold(UNLOCKED);

        requestRender();
    }

    private void setMuteString(TextNode node) {
        if(mAContext.isMuted()) {
            node.setText("unmute");
        } else {
            node.setText("mute");
        }
    }

    // ------------------------ GAME TURN STATES -----------------------------

    // Game State
    public static final int STATE_USER_TURN = 0;
    public static final int STATE_NOT_USER_TURN = 1;
    public static final int STATE_USER_OUT = 2;
    public static final int STATE_GAME_OVER = 3;
    public static final int STATE_GAME_PAUSED = 4;


    private void setGameSceneUserTurn() {
        System.out.println("MGLSV| creating Game Scene user Turn");

        rootNode.removeChild(mainMenu);

        mainMenu = new Menu("Game menu", 0, 0);

        TextNode done = new TextNode("Send commands");
        done.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                // TODO make sure calling this doesn't break the menu in some way
//                setGameSceneNotUserTurn();
                setGameState(STATE_NOT_USER_TURN);
                updateGameState();

                mAContext.onDoneClicked();

                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        mainMenu.addChild(done);

        TextNode backToMainMenu = new TextNode("Main menu");
        backToMainMenu.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                setMainState(STATE_MAIN_MENU);
                updateMainState();
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        mainMenu.addChild(backToMainMenu);

        if(mAContext.mTurnData.numHumanPlayers > 1) {
            TextNode removeThisUserFromGame = new TextNode("Retreat");
            removeThisUserFromGame.addTouchListener(new TouchListener() {
                @Override
                public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                    mAContext.onLeaveClicked(true);
                    return true;
                }

                @Override
                public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                    return false;
                }

                @Override
                public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                    return false;
                }
            });

            mainMenu.addChild(removeThisUserFromGame);
        }

//        // TODO take this out
//        Node selfDestruct = new Node("Self Destruct");
//        selfDestruct.addTouchListener(new TouchListener() {
//            @Override
//            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
//                for (Ship ship : ships) {
//                    if (!ship.isEnemy()) {
//                        ship.setNextRoundDead(true);
//                    }
//                }
//                return true;
//            }
//
//            @Override
//            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
//                return false;
//            }
//
//            @Override
//            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
//                return false;
//            }
//        });
//
//        mainMenu.addChild(selfDestruct);


        rootNode.addChild(mainMenu);
    }

    private void setGameSceneNotUserTurn() {
        System.out.println("MGLSV| creating Game Scene not user turn");
        for (int i = 0; i < ships.size(); i++) {
            ships.get(i).createShipInfoMenu();
        }

        rootNode.removeChild(mainMenu);

        mainMenu = new Menu("Game menu", 0, 0);

        TextNode backToMainMenu = new TextNode("Main menu");
        backToMainMenu.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                setMainState(STATE_MAIN_MENU);
                updateMainState();
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        TextNode removeThisUserFromGame = new TextNode("Retreat");
        removeThisUserFromGame.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                mAContext.onLeaveClicked(false);
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        mainMenu.addChild(backToMainMenu);
        mainMenu.addChild(removeThisUserFromGame);

        rootNode.addChild(mainMenu);
    }

    private void setGameSceneGameOver() {
        System.out.println("MGLSV| creating Game Scene game over");
        for (int i = 0; i < ships.size(); i++) {
            ships.get(i).createShipInfoMenu();
        }

        mAContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAContext.onFinishClicked();
            }
        });

        rootNode.removeChild(mainMenu);

        mainMenu = new Menu("Game menu", 0, 0);

//        Sprite done = new Sprite("Return to base", 0f, 0f);
//        done.addTouchListener(new TouchListener() {
//            @Override
//            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
//                mAContext.onFinishClicked();
//                return true;
//            }
//
//            @Override
//            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
//                return false;
//            }
//
//            @Override
//            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
//                return false;
//            }
//        });

        TextNode backToMainMenu = new TextNode("Main menu");
        backToMainMenu.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                setMainState(STATE_MAIN_MENU);
                updateMainState();
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        //mainMenu.addChild(done);
        mainMenu.addChild(backToMainMenu);

        rootNode.addChild(mainMenu);
        //requestRender();
    }

    private void setGameSceneUserOut() {
        System.out.println("MGLSV| creating Game Scene user out");

        rootNode.removeChild(mainMenu);

        mainMenu = new Menu("Game menu", 0, 0);

        // Sends game on to next user
        TextNode backToMainMenu = new TextNode("Main menu");
        backToMainMenu.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                setMainState(STATE_MAIN_MENU);
                updateMainState();
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        TextNode removeThisUserFromGame = new TextNode("Retreat");
        removeThisUserFromGame.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                mAContext.onLeaveClicked(true);
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        mainMenu.addChild(backToMainMenu);

        rootNode.addChild(mainMenu);
    }

    private void setGameScenePaused() {
        final Node pausedOverlay = new Node("Paused overlay");
        pausedOverlay.setScale(5);

        pausedOverlay.addTouchListener(new TouchListener() {
            @Override
            public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
                setGameState(prevGameState);
                rootNode.removeChild(pausedOverlay);
//                System.out.println("MGLSV| gameState, should Continue: " + getGameState() + ", " + simulateShouldContinue);
                if(mAContext.mTurnData.simulate && simulateShouldContinue) {
                    startSimThread();
                }
                return true;
            }

            @Override
            public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }

            @Override
            public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
                return false;
            }
        });

        rootNode.addChild(pausedOverlay);
    }


    // Nodes should only be changed when surrounded by lock check
    public void updateGameState() {
        while(!requestHold(EDIT_LOCK)) {
            Thread.yield();
        }

        switch (gameState) {
            case STATE_USER_TURN:
                setGameSceneUserTurn();
                break;
            case STATE_NOT_USER_TURN:
                setGameSceneNotUserTurn();
                break;
            case STATE_GAME_OVER:
                setGameSceneGameOver();
                break;
            case STATE_USER_OUT:
                setGameSceneUserOut();
                break;
            case STATE_GAME_PAUSED:
                setGameScenePaused();
                break;
            default:
                System.out.println("MGLSV| THE VALUE: " + gameState + " IS NOT A GAME STATE.");
                break;
        }

        requestHold(UNLOCKED);

        requestRender();
    }



    public void addPlanet(Planet planet) {
        rootNode.addChild(planet);
        planets.add(planet);
    }

    public void addShip(Ship ship) {
        rootNode.addChild(ship);
        ships.add(ship);
    }

    public void toggleMainMenu(Point point) {
        mainMenu.toggle(point, 1f);


        requestRender();
    }



    //----------------------------- Simulation -------------------------------
/**/
    //Date datetime = new Date();
//    int simFrameCount = 0;

    SimulateThread simulateThread = new SimulateThread();

    static int simThreadNum = 0;

    class SimulateThread extends Thread {

        public SimulateThread() {
            setName("SimulateThread" + MyGLSurfaceView.simThreadNum++);
        }

        @Override
        public void run() {

            while (simulateShouldContinue && gameState != STATE_GAME_PAUSED) {

                simulate();

                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    Log.e("MGLSV", "wait failed");
                    e.printStackTrace();
                }

            }

        }
    }

    public void startSimThread() {
//        Log.v("MGLSV", "Attempting startSimThread: " + simulateThread.isAlive());

//        if(simulateThread.isAlive()) {
//            Log.v("MGLSV", "Simulate thread is alive");
//            stopSimThread();
//        }

//        try {
//            MyGLRenderer.sprites.clear();

        simulateThread = new SimulateThread();

        setSimulationTimer(System.currentTimeMillis());

        if(simulateThread.getState() == Thread.State.NEW) {
            simulateThread.start();
        } else {
            simulateThreadNotNew();
        }
//        } catch(IllegalThreadStateException e) {
//            System.out.println("MGLSV| tried to start sim thread but was already running");
//            e.printStackTrace();
//        }
    }

    private void simulateThreadNotNew() {
        throw new IllegalThreadStateException();
    }

    public void stopSimThread() {
        Log.v("MGLSV", "Attempting thread join");
        simRunning = false;

        try {
            simulateThread.join();
            System.out.println("MGLSV| Thread joined");
        } catch (InterruptedException e) {
            Log.e("MGLSV", "Thread could not be joined");
            e.printStackTrace();
        }
    }

    public void simulate() {
//        System.out.println("MGLSV| simulating");
        simRunning = true;

        simulateShouldContinue = !simulateUpdate();

        if(!simulateShouldContinue) {
            System.out.println("MGLSV| simulation finished on run #" + simulateRuns);

            // gameState comes in with either STATE_USER_TURN or STATE_NOT_USER_TURN

            // If more than one user left, keep game going
            if(simulateNumUsersLeft == 1) {
                // If one is left, declare winner
                setGameState(STATE_GAME_OVER);

            } else if(simulateNumUsersLeft == 0) {
                // If zero are left, declare the last ones alive to have tied
                setGameState(STATE_GAME_OVER);

            } else if(!userAlive.contains(mAContext.getCurrentParticipantId()) && gameState == STATE_USER_TURN) {
                // If the gameState should be this user's turn but this user is no longer alive, the result is user_out


                // Pass the game on whether the user leaves or not because we don't want them to stall the game
                // TODO a user could stall a game by not opening the game, the next player wouldn't be able to go even though this user was out
                mAContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAContext.onDoneClicked();
                    }
                });
                // Let them leave the game or continue to watch it
                setGameState(STATE_USER_OUT);

            }

            // Once simulation is over, update the game state
            updateGameState();

            // Clear lasers?
            //MyGLRenderer.sprites.clear();

            simRunning = false;//stopSimThread();
            setSimulationTimer(0);

            for (Ship s : ships) {
                s.laserPath.trace.setVisible(true);
                s.laserEmitter.setVisible(false);
            }

        }
    }



    int simulateRuns = 0;
    int lasersStillAlive = 0;
    int simulateNumUsersLeft = 0;
    ArrayList<Ship> ships = new ArrayList<>();
    ArrayList<Planet> planets = new ArrayList<>();
    ArrayList<String> userAlive = new ArrayList<>();


    boolean simulateShouldContinue = false;

    // Reusable variables

    float time;
    Ship curShip;

    float mps; // meters/sec
    float impulse;
    float f;

    Vector curPow;


    Planet curPlanet;
    float gravityConst;
    float planetMass; // initializes at 100 kg
    float laserMass; // initializes at 1 kg

    // Get the distance from the planet to the laser's current position
    float dist;

    // Equation of gravity
    float forceGravity; // / (dist * dist);

    final int MAX_SIM_RUNS = 2500;


    public boolean simulateUpdate() {
        simulateRuns++;
        if(simulateRuns % 100 == 0) {
            System.out.println("MGLSV| simulation run #" + simulateRuns);
        }
        lasersStillAlive = 0;
        simulateNumUsersLeft = 0;
        userAlive = new ArrayList<>();
        time = 0.016f * 0.1f; // Seconds
        shipMoving = false;

        simDeltaTime = System.currentTimeMillis() - simulationTimer;
        setSimulationTimer(System.currentTimeMillis());



        // Set up shields at the beginning
//        if(simulateRuns == 1) {
//            for (int i = 0; i < ships.size(); i++) {
//                ships.get(i).shieldCircle.updateMatrix();
//            }
//        }


        for (int i = 0; i < ships.size(); i++) {
            curShip = ships.get(i);

            switch (curShip.getPrevRoundLaserShieldEngine()) {
                case 0: // laser

                    fireLaser();
                    break;

                case 1: // shield
                    // Don't really do anything here because shields are passive and are dealt with
                    //      in the laser section
                    curShip.showShield();

                    break;

                case 2: // engine

                    if(!curShip.isShipIsDead()) {
                        moveShip();
                    }

                    break;

                default:
                    break;
            }

            // If the user is not already alive and the current ship is not dead they are alive
            if(!userAlive.contains(curShip.userId)) {
                if(!curShip.isShipIsDead()) {
                    userAlive.add(curShip.userId);
                    simulateNumUsersLeft++;

                }
            }


        }

        if(simulateRuns % 1000 == 0) {
            System.out.println("MGLSV| num lasers alive: " + lasersStillAlive);
        }

        requestRender();

        // Stop simulation conditions // return true if done
        // We don't want to stop at a certain user amount because lasers could end up killing everyone

        // check if any ships are moving

        if(simulateRuns > MAX_SIM_RUNS || (lasersStillAlive <= 0 && !shipMoving)) {
            simulateShouldContinue = false;
            simRunning = false;
            // Return number of users left?
            return true;
        }
        else {
            return false;
        }
    }

    Vector tempLFVect = new Vector();
    Vector planetForce = new Vector();
    Point previousPoint = new Point();
    LaserPath laser;

    private void fireLaser() {
        if(!curShip.laserPath.initialForceApplied) {
//                Vector curPow = new Vector(curShip.currentPower * 5000000 * (float) Math.sin(curShip.getRotation() * 3.14159265f / 180.0f),
//                        curShip.currentPower * 5000000 * (float) Math.cos(curShip.getRotation() * 3.14159265f / 180.0f));

            // Calculate the force we need over the time we update to give us the velocity we want using impulse
            mps = 60 * curShip.getPrevRoundLaserPower(); // meters/sec
            impulse = curShip.laserPath.laserMass * mps;
            f = impulse / time;

            curPow = new Vector(f * (float) -Math.sin(curShip.getPrevRoundRotation() * 3.14159265f / 180f),
                    f * (float) Math.cos(curShip.getPrevRoundRotation() * 3.14159265f / 180f));

            curShip.laserPath.setPosition(curShip.getPrevRoundLocation());
            curShip.laserPath.laserPosition = curShip.laserPath.getPosition();

            curShip.laserPath.laserForce = curPow;

            curShip.laserPath.laserIsDead = curShip.isPrevRoundDead();

            curShip.laserPath.initialForceApplied = true;
        } else {
            tempLFVect.set(0,0,0);
            curShip.laserPath.laserForce = tempLFVect;
        }

        if(!curShip.laserPath.laserIsDead) {
            lasersStillAlive++;

            // Gravity Math - calculate force vector from each planet and add them
            for (int j = 0; j < planets.size(); j++) {
                curPlanet = planets.get(j);
                gravityConst = 6.1f;
                planetMass = planets.get(j).mass; // initializes at 100 kg
                laserMass = curShip.laserPath.laserMass; // initializes at 1 kg

                // Get the distance from the planet to the laser's current position
                dist = curShip.laserPath.laserPosition.distance(curPlanet.getPosition());

                // Equation of gravity
                forceGravity = (gravityConst * planetMass * laserMass) / (float) (Math.pow(dist, 2.7)); // / (dist * dist);

                planetForce.set(0,0,0);
                planetForce.set(curShip.laserPath.laserPosition, curPlanet.getPosition());

                planetForce.multiply(forceGravity);

                curShip.laserPath.laserForce.add(planetForce);

            }

            previousPoint.set(curShip.laserPath.laserPosition.x, curShip.laserPath.laserPosition.y, 0);

            laser = curShip.laserPath;

            // todo don't access x, y directly?
            laser.laserPosition.x += (laser.laserVelocity.x * time) +
                    ((laser.laserForce.x / laser.laserMass) * time * time);

            laser.laserPosition.y += (laser.laserVelocity.y * time) +
                    ((laser.laserForce.y / laser.laserMass) * time * time);

            laser.laserVelocity.x = (laser.laserPosition.x - previousPoint.x) / time;
            laser.laserVelocity.y = (laser.laserPosition.y - previousPoint.y) / time;
/*

                System.out.println("MGLSV| Laser Force(x,y): " + laser.laserForce.x + ", " + laser.laserForce.y);
                System.out.println("MGLSV| Laser Velocity(x,y): " + laser.laserVelocity.x + ", " + laser.laserVelocity.y);
                System.out.println("MGLSV| Laser Position(x,y): " + laser.laserPosition.x + ", " + laser.laserPosition.y);
*/
            if(simulateRuns % 1 == 0) {
                if(curShip.laserEmitter.getPosition().distance(laser.laserPosition) > 0.07f) {
                    curShip.laserEmitter.setPosition(laser.laserPosition);
                }
                curShip.laserEmitter.update(simDeltaTime);  //simulateRuns);

                laser.trace.setPosition(laser.laserPosition);
                laser.trace.update(simDeltaTime);

            }

            for (int j = 0; j < planets.size(); j++) {
                if (!laser.laserIsDead) {
                    if (laser.laserPosition.distance(planets.get(j).getPosition()) <
                            planets.get(j).getRadius()) {
                        laser.setLaserIsDead(true);
                        System.out.println("MGLSV| Planet hit!");
                        lasersStillAlive--;
                    }
                }
            }

            //System.out.println("MGLSV| laser Pos: " + laser.laserPosition.x + ", " + laser.laserPosition.y);

            // Iterate through ships
            for (int j = 0; j < ships.size(); j++) {
                // Don't hit own ship for now
                if (!ships.get(j).equals(curShip)) {
                    // If the j ship isn't already dead & the curShip's laser is not dead
                    if (!laser.laserIsDead && !ships.get(j).isShipIsDead()) {

                        // If the ship has a shield
                        if(ships.get(j).getPrevRoundLaserShieldEngine() == 1) {

                            // Update the matrix just in case of who knows what. Probably doesn't do anything
                            if(ships.get(j).baseShape.modelMatrix[0] == 0) {
                                ships.get(j).updateMatrix();
                            }

                            if(ships.get(j).shieldCircle.baseShape.isTouched(laser.laserPosition, true)) {

                                laser.setLaserIsDead(true);
                                System.out.println("MGLSV| Shield hit!");
                                lasersStillAlive--;
                            }

                        } else {

                            if(ships.get(j).baseShape.isTouched(laser.laserPosition, true)) {

                                ships.get(j).setNextRoundDead(true);
                                //userAlive.remove(ships.get(j).userId);
                                laser.setLaserIsDead(true);
                                System.out.println("MGLSV| Ship hit!");
                                lasersStillAlive--;
                            }
                        }
                    }
                }
            }

            // Cancel laser if it is too far away
            if(laser.laserPosition.distance(Point.ZERO) > 50) {
                laser.setLaserIsDead(true);
                lasersStillAlive--;
            }
        } else {
            // Display trail
//            curShip.laserEmitter.update(simDeltaTime);
        }
    }

    private float xDist = 0;
    private float yDist = 0;
    private boolean shipMoving;
    private float percentSim;
    private Vector velocity = new Vector();
    private Vector totalPath = new Vector();
    private Vector currentPath = new Vector();

    private void moveShip() {
        if(!curShip.isMoving()) return;

        percentSim = ((float)simulateRuns) / ((float)MAX_SIM_RUNS) * 2.0f;

        if(percentSim > 1.0f) {
            return;
        }
        currentPath.set(curShip.getPrevRoundLocation(), curShip.getCurrentLocation());
        totalPath.set(curShip.getPrevRoundLocation(), curShip.getNextRoundStartLocation());
        velocity.set(totalPath);
        velocity.setToUnitLength();
        velocity.multiply(0.01f);



//        float inner = (2.0f*(100 - simulateRuns) - 1.0f);
//
//        velocity.multiply(-1 * inner * inner + 1.01f);
//
//        xDist = velocity.x * (curShip.getNextRoundStartLocation().x - curShip.getPrevRoundLocation().x);
//        yDist = velocity.y * (curShip.getNextRoundStartLocation().y - curShip.getPrevRoundLocation().y);

//        if(currentPath.length() < totalPath.length() / 2.0f) {
//            curShip.setVelocity(curShip.getVelocity() + 0.0001f);
//            velocity.multiply(curShip.getVelocity());
//        } else {
//            curShip.setVelocity(curShip.getVelocity() - 0.0001f);
//            velocity.multiply(curShip.getVelocity());
//        }
//
//        System.out.println("MGLSV| curship velocity: " + curShip.getVelocity());
//        System.out.println("MGLSV| vector velocity: " + velocity.length());


//        xDist = percentSim * (curShip.getNextRoundStartLocation().x - curShip.getPrevRoundLocation().x);
//        yDist = percentSim * (curShip.getNextRoundStartLocation().y - curShip.getPrevRoundLocation().y);

        xDist = velocity.x;// * simulateRuns;
        yDist = velocity.y;// * simulateRuns;

        //System.out.println("MGLSV| xDist, yDist: " + xDist + ", " + yDist);

        // check that ship won't collide with planet-
        for (int i = 0; i < planets.size(); i++) {
            if(planets.get(i).getPosition().distance(curShip.getPosition()) < curShip.getScaleX() / 2 + planets.get(i).getRadius()) {

                float tempPercentSim = ((float)simulateRuns - 3.0f) / ((float)MAX_SIM_RUNS) * 2.0f;// TODO fix to velocity
                float tempXDist = (curShip.getNextRoundStartLocation().x - curShip.getPrevRoundLocation().x);
                float tempYDist = (curShip.getNextRoundStartLocation().y - curShip.getPrevRoundLocation().y);


                curShip.setNextRoundStartLocation(
                        curShip.getPrevRoundLocation().x + tempPercentSim * tempXDist,
                        curShip.getPrevRoundLocation().y + tempPercentSim * tempYDist);

                curShip.setMoving(false);
                return;
            }
        }

        for (int i = 0; i < ships.size(); i++) {
            if(!ships.get(i).isShipIsDead() && !ships.get(i).equals(curShip)) {
                if(ships.get(i).getPosition().distance(curShip.getPosition()) < curShip.getScaleX() / 2 + ships.get(i).getScaleX() / 2) {

                    float tempPercentSim = ((float)simulateRuns - 3.0f) / ((float)MAX_SIM_RUNS) * 2.0f;// TODO fix to velocity
                    float tempXDist = (curShip.getNextRoundStartLocation().x - curShip.getPrevRoundLocation().x);
                    float tempYDist = (curShip.getNextRoundStartLocation().y - curShip.getPrevRoundLocation().y);


                    curShip.setNextRoundStartLocation(
                            curShip.getPrevRoundLocation().x + tempPercentSim * tempXDist,
                            curShip.getPrevRoundLocation().y + tempPercentSim * tempYDist);

                    curShip.setMoving(false);
                    return;
                }
            }
        }

        curShip.setCurrentLocation(curShip.getCurrentLocation().x + xDist, curShip.getCurrentLocation().y + yDist);
//        curShip.setCurrentLocation(curShip.getPrevRoundLocation().x + xDist, curShip.getPrevRoundLocation().y + yDist);

        currentPath.set(curShip.getPrevRoundLocation(), curShip.getCurrentLocation());

        if(totalPath.length() < currentPath.length()) {
            System.out.println("MGSLV| ship done moving.");
            curShip.setCurrentLocation(curShip.getNextRoundStartLocation());
            curShip.setMoving(false);
            return;
        }

        shipMoving = true; // A ship is moving so we need to continue the simulation

    }

    private int prevGameState = -1;

    public int getGameState() {
        return gameState;
    }

    public void setGameState(int gameState) {
//        System.out.println("MGLSV| Game State: " + gameState);
        prevGameState = this.gameState;
        this.gameState = gameState;
    }

    public int getMainState() {
        return mainState;
    }

    public void setMainState(int mainState) {
        this.mainState = mainState;
    }

    // Returns true if the function asking currently has the lock
    public boolean requestHold(String whoAskin) {
        // If they want to unlock we can do that fast
        if(whoAskin.equals(UNLOCKED)) {
            nodesLocked = whoAskin;
            return false;
        }

        // If they are checking
        if(whoAskin.equals(nodesLocked)) {
            return true;
        } else if(nodesLocked.equals(UNLOCKED)) {
            nodesLocked = whoAskin;
            return true;
        }

        return false;
    }

    public void setSimulationTimer(long time) {
        simulationTimer = time;
    }


    //---------------------------- App State --------------------------------

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("Pause called in view");

        if(getMainState() == STATE_GAME && simRunning) {
            setGameState(STATE_GAME_PAUSED);
            updateGameState();
            simRunning = false;

            simDeltaTime = System.currentTimeMillis() - simulationTimer;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("Resume called in view");

        setSimulationTimer(System.currentTimeMillis() - simDeltaTime);
    }
}
