package com.nomadjackalope.tgem_t47;

import java.util.ArrayList;

/**
 * Created by benjamin on 1/25/17.
 *
 * This class is used to toggle the menus on and off
 */
public class MenuManager implements MenuState {

    // Currently not queried
    private ArrayList<Menu> menus;

    private Menu menuOpen;

    public MenuManager() {
        menus = new ArrayList<>();
    }

    public void registerMenu(Menu menu) {
        menus.add(menu);
    }

    // Open a menu with a Node
    public void openMenu(Menu menu, Node nodeToOpenMenuOn) {
        openMenu(menu, nodeToOpenMenuOn.getPosition(), nodeToOpenMenuOn.getScaleX());
    }

    // Open a menu with a default radius of 1
    public void openMenu(Menu menu, Point point) {
        openMenu(menu, point, 1.0f);
    }

    public void openMenu(Menu menu, Point point, float radius) {
        // Close possibly open menu // Doing the most basic route here should finess this
        closeMenu();

        // Get layout for particular node
        menu.layoutMenu(point, radius);

        // Show new menu
        menu.setAllChildrenVisibility(true);

        // A menu is open, store it
        menuOpen = menu;
    }

    public void openMenu(Menu menu, boolean open) {

    }

    public void closeMenu() {
        // If a menu is open, close it
        if(menuOpen != null) {
            menuOpen.setAllChildrenVisibility(false);
            menuOpen = null;
        }
    }

    @Override
    public void setMenuState(Menu menu, boolean open) {
        openMenu(menu, open);
    }

    @Override
    public void menuClicked(Menu menu, Node node) {
        if(menu == menuOpen) {
            closeMenu();
        } else {
            openMenu(menu, node);
        }
    }
}

interface MenuState {
    void setMenuState(Menu menu, boolean open);
    void menuClicked(Menu menu, Node node);
}
