package com.nomadjackalope.tgem_t47;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by benjamin on 4/5/17.
 */
public class Circle extends Rectangle {

    private Point center;

    private int resolution; //# segments / whole circle
    private int numSegmentsVisible;

    private Segment[] segments; // Just an array of the segment changes, not all the segments

    private class Segment {
        private float innerDiameter = 1.0f;
        private float outerDiameter = 1.5f;
        private float arcDegrees = 360;
        private int id = 0;

        public Segment(float innerDiameter, float outerDiameter, float arcDegrees, int id) {
            this.innerDiameter = innerDiameter;
            this.outerDiameter = outerDiameter;
            this.arcDegrees = arcDegrees;
            this.id = id;
        }

        public float getInnerDiameter() {
            return innerDiameter;
        }

        public void setInnerDiameter(float innerDiameter) {
            this.innerDiameter = innerDiameter;
        }

        public float getOuterDiameter() {
            return outerDiameter;
        }

        public void setOuterDiameter(float outerDiameter) {
            this.outerDiameter = outerDiameter;
        }

        public float getArcDegrees() {
            return arcDegrees;
        }

        public void setArcDegrees(float arcDegrees) {
            this.arcDegrees = arcDegrees;
        }
    }

    //float[] segmentPattern;
    boolean mirrorY;

    private float[] circleCoords;
    private FloatBuffer circleVertBuffer;
    private int circleVertCount;

    public Circle(String name) {
        this.name = name;
    }

    void init() {

        segments = new Segment[1];

        segments[0] = new Segment(1, 1.5f, 360, 0);

        Segment activeSeg = segments[0];

        resolution = 24;

        numSegmentsVisible = 24;

        circleCoords = new float[(resolution + 1) * 3]; // * 2? or 4? when I change from simple line circle to complex

        int offset = 3;
        // simple circle
        for (int i = 0; i < resolution + 1; i++) {

            circleCoords[offset * i] = (float)Math.sin(360.0 / resolution * i * 0.0174532); // pi / 180
            circleCoords[offset * i + 1] = (float)Math.cos(360.0 / resolution * i * 0.0174532);
            circleCoords[offset * i + 2] = 0.0f;
        }

        // initialize vertex byte buffer for shape coords
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // number of coord vals * 4 bytes per float
                circleCoords.length * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        circleVertBuffer = bb.asFloatBuffer();
        circleVertBuffer.put(circleCoords);
        // set the buffer to read the first coordinate
        circleVertBuffer.position(0); // or vertexBuffer.rewind(); either works

        circleVertCount = circleCoords.length / COORDS_PER_VERTEX;
    }

    @Override
    public void draw(float[] vMatrix, float[] pMatrix, Material mat, Point position, float scaleX, float scaleY, float rotation) {
        GLES20.glUseProgram(mat.getProgram());

        // Get int that refers to variable vPosition and set it to coordinates matrix
        positionHandle = GLES20.glGetAttribLocation(mat.getProgram(), "vPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, circleVertBuffer);

        // get handle to fragment shader's vColor member and set color for drawing the triangle
        colorHandle = GLES20.glGetUniformLocation(mat.getProgram(), "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);

        // get handle to shape's transformation matrix and pass the projectionView transformation to the shader
        mVPMatrixHandle = GLES20.glGetUniformLocation(mat.getProgram(), "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mVPMatrixHandle, 1, false, mVPMatrix, 0);

        GLES20.glLineWidth(2.0f);

        // Draw the rectangle
        GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, 0, numSegmentsVisible + 1);

        // Disable vertex array // Don't know why
        GLES20.glDisableVertexAttribArray(positionHandle);
    }

    public int getNumSegmentsVisible() {
        return numSegmentsVisible;
    }

    public void setNumSegmentsVisible(int numSegmentsVisible) {
        if(numSegmentsVisible > resolution) {
            numSegmentsVisible = resolution;
        } else if (numSegmentsVisible < 0) {
            numSegmentsVisible = 0;
        }
        this.numSegmentsVisible = numSegmentsVisible;
    }

    public void setNumSegmentsVisible(float percentPower) {
        setNumSegmentsVisible((int)((float)resolution * percentPower));
    }

    Point tP = new Point();

    @Override
    public boolean isTouched(Point position, boolean isSim) {

        tempModelBefore = modelMatrix;
        lockedModelMatrix = modelMatrix;


        tempMatRet = Matrix.invertM(inverted, 0, lockedModelMatrix, 0);
        //System.out.println("R| matrix was inverted: " + tempMatRet);

        //TODO BUG: Modelmatrix sometimes comes in filled with zeros
        // This makes sure that if the model matrix is wrong, this function will not return
        //  saying that it has been touched. We forgo the times where it might be true,
        //  for the times where it is not even close.
        // This might be a thread volatile situation
        if(!tempMatRet && name.equals("Ship sprite")) {

            lockedModelMatrix[0] = 1.0f;
            lockedModelMatrix[5] = 1.0f;
            lockedModelMatrix[10] = 1.0f;

            tempModelAfter = lockedModelMatrix;

            tempMatRet = Matrix.invertM(inverted, 0, lockedModelMatrix, 0);
            System.out.println("R| replacement worked: " + tempMatRet);
            System.out.println("R| ________________");

            System.out.print("R| model matrix before: ");
            for (float x : tempModelBefore) {
                System.out.print(x);
                System.out.print(", ");
            }
            System.out.println();

            System.out.print("R| model matrix after: ");
            for (float x : tempModelAfter) {
                System.out.print(x);
                System.out.print(", ");
            }
            System.out.println();

            if(!tempMatRet) {
                Log.e("Rectangle", "Model matrix could not be inverted. Aborting collision check");
                return false;
            }
        }

        // Copy values because we don't want to change touchPoint as it is used for all checks
        float[] eachRectTouch = new float[4];
        eachRectTouch[0] = position.x;
        eachRectTouch[1] = position.y;
        eachRectTouch[2] = 0;
        eachRectTouch[3] = 1;

        // Convert touchPoint into model coordinate system
        Matrix.multiplyMV(eachRectTouch, 0, inverted, 0, eachRectTouch, 0);
        //System.out.println("Rectangle| touch4 " + name +  " x,y: " + eachRectTouch[0] + ", " + eachRectTouch[1]);

        tP.x = eachRectTouch[0];
        tP.y = eachRectTouch[1];

        // Check if coordinates are within the boundaries of the circle
        if(isSim) {
            if (tP.distance(Point.ZERO) < 1.0f)//segments[0].getOuterDiameter())
            {
                return true;
            }
        } else {
            if (tP.distance(Point.ZERO) * touchScale < 1.0f)//segments[0].getOuterDiameter())
            {
                return true;
            }
        }

        return false;
    }
}
