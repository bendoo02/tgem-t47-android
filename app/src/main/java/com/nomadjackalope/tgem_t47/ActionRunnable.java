package com.nomadjackalope.tgem_t47;

/**
 * Created by benjamin on 4/26/17.
 */
public interface ActionRunnable {
    boolean run();
}
