package com.nomadjackalope.tgem_t47;

/**
 * Created by benjamin on 3/30/17.
 */
public class ActionMove extends Action {

    Point initialPos;
    Point finalPos;
    Point tempPoint;

    public ActionMove(Point initalPos, Point finalPos, float seconds, Node target) {
        this.initialPos = initalPos;
        this.finalPos = finalPos;

        tempPoint = new Point(initalPos);

        init(target, seconds);
    }

    @Override
    protected void updateSubClass() {

        // Update values by interpolation
        tempPoint.x = (finalPos.x - initialPos.x) * getPercentDone();
        tempPoint.y = (finalPos.y - initialPos.y) * getPercentDone();
        tempPoint.z = (finalPos.z - initialPos.z) * getPercentDone();

        // Update the object
        if(getTarget() != null) {
            getTarget().setPosition(tempPoint);
        }
    }

}
