package com.nomadjackalope.tgem_t47;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by benjamin on 6/1/17.
 */
public class ParticleEmitter extends Node {

    private int maxParticles = 10000;
    private float emitterSpeed = 100f; // number of particles emitted per second
    private float newParticles = 0; // Count of particles that should be added

    ArrayList<Particle> particles = new ArrayList<>();
    ArrayList<Particle> deadParticles = new ArrayList<>();

    Point previousPosition;
    Vector deltaPosition = new Vector();

    Random randomizer = new Random();

    // Life of the emitter to be used for general change of particles attributes over time
    float eLife;

    //----------------- Particle variables ---------------------
    Point pLocation = new Point();
    Vector pLocationVariation = new Vector();

    Point pSize = new Point();
    float pSizeVariation = 0;

    float pLife;
    float pLifeVariation = 0;

    Vector pVeloctiy = new Vector();
    Vector pVelocityVariation = new Vector();

    float[] pStartColor = {1.0f, 1.0f, 1.0f, 1.0f};
    float pSHueVarition = 0;
    float pSAlphaVariation = 0;
    float pSValueVariation = 0;

    float[] pEndColor = {1.0f, 0.0f, 0.0f, 0.0f};
    float pEHueVarition = 0;
    float pEAlphaVariation = 0;
    float pEValueVariation = 0;

    float[] pCurrentColor = new float[4];
    //----------------------------------------------------------//

    public ParticleEmitter(String name) {
        initDefaults(name);

        baseShape = new JaserSprite();
        material = MyGLRenderer.particleMat;

        baseShape.color = new float[]{0.8f, 0.3f, 0.5f, 0.5f};
    }

    @Override
    public void draw(MyGLRenderer renderer) {
        if(material == null) {
            material = renderer.getDefaultMaterial();
        }

        // TODO Remove this to increase performance but make sure it happens if the view matrix changes
        if(MyGLRenderer.viewChanged) {
            updateMatrix();
        }


        // Actions get removed if they return true indicating they are finished
        for (int i = 0; i < getActions().size(); i++) {
            if(getActions().get(i).update(MyGLRenderer.clock)) {
                getActions().remove(i);
                i--;
            }
        }

        if(isVisible()) {
            ((JaserSprite) baseShape).draw(this, renderer.getViewMatrix(), renderer.getProjectionMatrix(),
                    material, getPosition(), getScaleX(), getScaleY(), getRotation());
//            baseShape.draw(renderer.getViewMatrix(), renderer.getProjectionMatrix(),
//                    material, getPosition(), getScaleX(), getScaleY(), getRotation());
        }

        // Tells children to render
        int childrenSize = children.size();
        for (int i = 0; i < childrenSize; i++) {

//            System.out.print("N| name & i & size: ");
//            System.out.print(name);
//            System.out.print(", ");
//            System.out.print(i);
//            System.out.print(", ");
//            System.out.print(childrenSize);
//            System.out.println();

            children.get(i).draw(renderer);
        }
    }

    private Particle generateParticleAttributes(Particle particle) {

        particle.position.x += (randomizer.nextFloat() * 2.0f - 1.0f) * pLocationVariation.x;
        particle.position.y += (randomizer.nextFloat() * 2.0f - 1.0f) * pLocationVariation.y;

        particle.life = pLife + (randomizer.nextFloat() * 2.0f - 1.0f) * pLifeVariation;

        particle.veloctiy.x += (randomizer.nextFloat() * 2.0f - 1.0f) * pVelocityVariation.x;
        particle.veloctiy.y += (randomizer.nextFloat() * 2.0f - 1.0f) * pVelocityVariation.y;

        // TODO pColor stuff

        return particle;
    }

    public void update(float deltaTime) {
        if(previousPosition == null) {
            previousPosition = new Point(getPositionX(), getPositionY(), getPositionZ());
        }

//        System.out.println("PE| Delta time");
//        System.out.println(deltaTime);

        newParticles += emitterSpeed * deltaTime / 1000; // particles = particles/sec * millisec * sec/1000 millisec

        while (newParticles >= 1) { // Won't do a particle if 0 < newParticles < 1
            Particle newParticle;


            if(deadParticles.size() > 0){
                newParticle = deadParticles.get(deadParticles.size() - 1);
                deadParticles.remove(deadParticles.size() - 1);
            } else {
                if(particles.size() >= maxParticles) {
                    break;
                }

                newParticle = new Particle();

                particles.add(newParticle);
            }

            // FOR TESTING PURPOSES
//            newParticle.position.set((particles.size()-1)%100.0f,(particles.size()-1)/100,0);//getPositionX(), getPositionY(), getPositionZ());
            newParticle.position.set(0,0,0);

            generateParticleAttributes(newParticle);

            newParticles--;

        }

//        System.out.println("PE| particles size; " + particles.size());

        deltaPosition.set(getPosition(), previousPosition);

        for (int i = 0; i < particles.size(); i++) {
            particles.get(i).position.x += deltaPosition.x;
            particles.get(i).position.y += deltaPosition.y;
            particles.get(i).position.z += deltaPosition.z;

            // If particle is dead then add to dead list
            if (!particles.get(i).update(deltaTime)) {
                deadParticles.add(particles.get(i));
//                particles.remove(i);
//                i--;
            }
        }

//        System.out.println(particles.size());

        previousPosition.set(getPositionX(), getPositionY(), getPositionZ());
    }

    public float getEmitterSpeed() {
        return emitterSpeed;
    }

    public void setEmitterSpeed(float emitterSpeed) {
        this.emitterSpeed = emitterSpeed;
    }

    public int getMaxParticles() {
        return maxParticles;
    }

    public void setMaxParticles(int maxParticles) {
        this.maxParticles = maxParticles;
    }
}
