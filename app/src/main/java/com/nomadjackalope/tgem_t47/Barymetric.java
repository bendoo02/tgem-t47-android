package com.nomadjackalope.tgem_t47;

import android.graphics.Rect;
import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

/**
 * Created by benjamin on 3/14/17.
 */
public class Barymetric extends Rectangle {

    private FloatBuffer baryBuffer;
    private ShortBuffer gridOrderBuffer;
    private int baryCoordHandle;

    private float[] gridCoords;
    private short[] gridOrder;

    private static final int COORDS_PER_GRID = 4;

    private int gridWidth;
    private int gridHeight;

    // These are the counter-clockwise coordinates relative to bot-left for a Rectangle made of triangles
    private static float[] baryCoords = {
            0.0f, 0.0f, //bot-left
            1.0f, 0.0f, //bot-right
            0.0f, 1.0f, //top-left
            1.0f, 1.0f  //top-right
    };

    private float[] gridBaryCoords;

    public Barymetric(String name, int gridWidth, int gridHeight) {
        this.name = name;

        init(gridWidth, gridHeight);
    }

    private void generateGridCoords(int width, int height) {
        gridCoords = new float[(width + 1) * (height + 1) * COORDS_PER_VERTEX];

        float widthF = width;
        float heightF = height;

        for (int i = 0; i <= height; i++) {
            for (int j = 0; j <= width; j++) {
                gridCoords[i * (width + 1) * COORDS_PER_VERTEX + j * COORDS_PER_VERTEX] = (-widthF / 2 + j);
                gridCoords[i * (width + 1) * COORDS_PER_VERTEX + j * COORDS_PER_VERTEX + 1] = (-heightF / 2 + i);
                gridCoords[i * (width + 1) * COORDS_PER_VERTEX + j * COORDS_PER_VERTEX + 2] = 0.0f;

            }
        }

        gridOrder = new short[width * height * 2 * COORDS_PER_VERTEX]; // 2 = number of tris per rect

        for (int i = 0; i < width * height; i++) {
            int j = i * 6;

            gridOrder[  j  ] = (short)i;
            gridOrder[j + 1] = (short)(i + 1);
            gridOrder[j + 2] = (short)(i + 4);

            gridOrder[j + 3] = (short)(i + 4);
            gridOrder[j + 4] = (short)(i + 1);
            gridOrder[j + 5] = (short)(i + 5);

        }




    }

    private static final short[] squareOrder = new short[] { 0,1,2, 2,1,3 };

    void init(int gridWidth, int gridHeight) {

        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;

        generateGridCoords(gridWidth, gridHeight);

        // initialize vertex byte buffer for shape coords
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // number of coord vals * 4 bytes per float
                gridCoords.length * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(gridCoords);
        // set the buffer to read the first coordinate
        vertexBuffer.position(0); // or vertexBuffer.rewind(); either works

        ByteBuffer bcb = ByteBuffer.allocateDirect(
                // number of coord vals * 4 bytes per float
                baryCoords.length * 4);
        bcb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        baryBuffer = bcb.asFloatBuffer();
        baryBuffer.put(baryCoords);
        // set the buffer to read the first coordinate
        baryBuffer.position(0);

        ByteBuffer gob = ByteBuffer.allocateDirect(
                // number of coord vals * 4 bytes per float
                gridOrder.length * 4
        );
        gob.order(ByteOrder.nativeOrder());

        gridOrderBuffer = gob.asShortBuffer();
        gridOrderBuffer.put(gridOrder);
        gridOrderBuffer.position(0);
    }

    public void generateBaryCoords(float[] vertices) {
        // How on earth would I do this? eek...
    }

    int[] vbo = new int[1];
    int[] ibo = new int[1];

    @Override
    public void draw(float[] vMatrix, float[] pMatrix, Material mat, Point position, float scaleX, float scaleY, float rotation) {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mat.getProgram());

        // Get int that refers to variable vPosition and set it to coordinates matrix
        positionHandle = GLES20.glGetAttribLocation(mat.getProgram(), "vPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        // get handle to fragment shader's vColor member and set color for drawing the triangle
        colorHandle = GLES20.glGetUniformLocation(mat.getProgram(), "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);

        baryCoordHandle = GLES20.glGetAttribLocation(mat.getProgram(), "baryCoordIn");
        GLES20.glEnableVertexAttribArray(baryCoordHandle);
        GLES20.glVertexAttribPointer(baryCoordHandle, 2,
                GLES20.GL_FLOAT, false,
                baryCoords.length, baryBuffer);

        updateMatrix(vMatrix, pMatrix, position, scaleX, scaleY, rotation);


        // get handle to shape's transformation matrix and pass the projectionView transformation to the shader
        mVPMatrixHandle = GLES20.glGetUniformLocation(mat.getProgram(), "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mVPMatrixHandle, 1, false, mVPMatrix, 0);


        //----------------- learnopengles lesson 8 -----------------------


        GLES20.glGenBuffers(1, vbo, 0);
        GLES20.glGenBuffers(1, ibo, 0);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo[0]);
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexBuffer.capacity()
                * 4, vertexBuffer, GLES20.GL_STATIC_DRAW);

        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, ibo[0]);
        GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, gridOrderBuffer.capacity()
                * 2, gridOrderBuffer, GLES20.GL_STATIC_DRAW);

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, gridOrder.length, GLES20.GL_UNSIGNED_SHORT, 0);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);




        // Draw the rectangle
        //GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, vertexCount);
        //GLES20.glDrawElements(GLES20.GL_LINES, 0, GLES20.GL_UNSIGNED_SHORT, gridOrderBuffer);

        // Disable vertex array // Don't know why
        GLES20.glDisableVertexAttribArray(positionHandle);
        GLES20.glDisableVertexAttribArray(baryCoordHandle);

    }
}
