package com.nomadjackalope.tgem_t47;

/**
 * Created by benjamin on 1/23/17.
 */
public class Sprite extends Node {


    public Sprite(String name, float x, float y) {
        this.setName(name);
        this.setPositionX(x);
        this.setPositionY(y);
    }
}
