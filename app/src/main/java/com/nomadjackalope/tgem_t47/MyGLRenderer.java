package com.nomadjackalope.tgem_t47;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.SystemClock;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by benjamin on 1/4/17.
 * This handles callbacks from OpenGL and manages scene rendering
 */
public class MyGLRenderer implements GLSurfaceView.Renderer {

    public static Material myMaterial;
    private static Material defaultMaterial;
    public static Material texturedMat;
    public static Material batchLaser;
    public static Material baryMat;
    public static Material fontMat;
    public static Material backgroundMat;
    public static Material particleMat;

    public int[] textures;
    private Bitmap textureBitmap;
    private Bitmap textureBitmap2;
    private Bitmap shipBitmap;
    static Bitmap fontBitmap;
    static int fontTextureId;

    public static FontObjectPNG fontObj;

    private final float[] viewMatrix = new float[16];
    private final float[] projectionMatrix = new float[16];

    public static boolean viewChanged = false;

    // Game time
    static long clock;

    MyGLSurfaceView myView;

    public Point glViewportSize;

    public static MyGLRenderer activeRenderer;

    public MyGLRenderer(MyGLSurfaceView view) {
        myView = view;

        activeRenderer = this;

        myMaterial = new Material(BasicShader.basicVertexShaderCode, BasicShader.basicFragmentShaderCode, this);
        texturedMat = new Material(BasicShader.textureVertexShaderCode, BasicShader.textureFragmentShaderCode, this);
        batchLaser = new Material(BasicShader.batchTextureVertexShaderCode, BasicShader.batchTextureFragmentShaderCode, this);
        baryMat = new Material(BasicShader.barymetricVertexShaderCode, BasicShader.barymetricFragmentShaderCode, this);
        fontMat = new Material(BasicShader.fontVertexShaderCode, BasicShader.fontFragmentShaderCode, this);
        backgroundMat = new Material(BasicShader.backgroundVertexShaderCode, BasicShader.backgroundFragmentShaderCode, this);
        particleMat = new Material(BasicShader.particleVertexShaderCode, BasicShader.particleFragmentShaderCode, this);

        //fontObj = new FontObject(myView.getContext().getAssets());
        fontObj = new FontObjectPNG();
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);


//        // Position the eye behind the origin
//        final float eyeX = 0.0f;
//        final float eyeY = 0.0f;
//        final float eyeZ = 1.5f;
//
//        // We are looking toward the distance
//        final float lookX = 0.0f;
//        final float lookY = 0.0f;
//        final float lookZ = -5.0f;
//
//        // Set our up vector. This is where our head would be pointing were we holding the camera.
//        final float upX = 0.0f;
//        final float upY = 1.0f;
//        final float upZ = 0.0f;
//
//        // Set the view matrix. This matrix can be said to represent the camera position.
//
//        // NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model and
//        // view matrix. In OpenGL 2, we can keep track of these matrices separately if we choose.
//        Matrix.setLookAtM(viewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);


        // Load materials into OpenGL from OpenGL context
        myMaterial.loadShaders(); // could be passed to MyGLSurfaceView to hold material reference
        texturedMat.loadShaders();
        baryMat.loadShaders();
        batchLaser.loadShaders();
        fontMat.loadShaders(new AttribVariable[] {
                AttribVariable.A_Position, AttribVariable.A_TexCoordinate, AttribVariable.A_MVPMatrixIndex
        });
        System.out.println("MGLR| gl error: " + GLES20.glGetError());
        backgroundMat.loadShaders();
        System.out.println("MGLR| gl error: " + GLES20.glGetError());
        particleMat.loadShaders(new AttribVariable[] {
                AttribVariable.A_Position, AttribVariable.A_TexCoordinate, AttribVariable.A_MVPMatrixIndex
        });

        //myView.createBackground();

        defaultMaterial = myMaterial;



        fontObj.setTextureId(TextureHelper.loadTexture(BitmapFactory.decodeResource(myView.getResources(), R.drawable.mensch_distance_field)));
        fontObj.load("menschDistanceField.fnt", myView.mAContext);

        textures = new int[8];
        GLES20.glGenTextures(8, textures, 0);
        textureBitmap = BitmapFactory.decodeResource(myView.getResources(), R.drawable.planet);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, textureBitmap, 0);

        textureBitmap.recycle();


        textureBitmap2 = BitmapFactory.decodeResource(myView.getResources(), R.drawable.laser);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[1]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, textureBitmap2, 0);

        textureBitmap2.recycle();


        shipBitmap = BitmapFactory.decodeResource(myView.getResources(), R.drawable.ship);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE2);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[2]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, shipBitmap, 0);

        shipBitmap.recycle();


        //---------- Ship menu textures --------------------

        shipBitmap = BitmapFactory.decodeResource(myView.getResources(), R.drawable.shipslaser);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE4);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[3]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, shipBitmap, 0);

        shipBitmap.recycle();

        shipBitmap = BitmapFactory.decodeResource(myView.getResources(), R.drawable.shipsengine);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE5);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[4]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, shipBitmap, 0);

        shipBitmap.recycle();

        shipBitmap = BitmapFactory.decodeResource(myView.getResources(), R.drawable.shipshield);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE6);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[5]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, shipBitmap, 0);

        shipBitmap.recycle();

        shipBitmap = BitmapFactory.decodeResource(myView.getResources(), R.drawable.rot);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE7);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[6]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, shipBitmap, 0);

        shipBitmap.recycle();

        shipBitmap = BitmapFactory.decodeResource(myView.getResources(), R.drawable.grab);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE8);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[7]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, shipBitmap, 0);

        shipBitmap.recycle();



        //batchRenderInit();

    }

    private float ratio;
    private float scale;

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0, 0, width, height);

        System.out.println("MGLR|  width, height: " + width + ", " + height);

        glViewportSize = new Point(width, height);

        ratio = (float) width / height;

        // This sets the world coordinate scale so, assuming landscape mode and no viewMatrix,
        //      the highest y unit shown is this number.
        scale = 1; //10

        // Create a new perspective projection matrix. The height will stay the same
        // while the width will vary as per aspect ratio.
        final float left = -ratio * scale;
        final float right = ratio * scale;
        final float bottom = -1.0f * scale;
        final float top = 1.0f * scale;
        final float near = -5.0f;
        final float far = 10.0f;

        // Orthographic
       Matrix.orthoM(projectionMatrix, 0, left, right, bottom, top, near, far);

//        Matrix.setIdentityM(projectionMatrix, 0);
//        projectionMatrix[0] = 1/ratio;

        //View matrix
//        Matrix.setIdentityM(viewMatrix, 0);

//        scaleView(0.1f);

        setTotalScale(0.08f);
        totalXTran = 0f;//-0.05f;
        totalYTran = 0;//-0.05f;
        updateView();
        // Perspective
        //Matrix.frustumM(projectionMatrix, 0, left, right, bottom, top, near, far);
    }

    // Main Render loop
    @Override
    public void onDrawFrame(GL10 gl) {
        // Update clock
        clock = System.nanoTime() / 1000000;

        // Render everything
        if(myView.requestHold(MyGLSurfaceView.DRAW_LOCK)) {

            MyGLSurfaceView.activeAction = false;

            // Redraw the background color
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

            myView.rootNode.draw(this);

            // Render all RectSprites
            //batchRender();
            myView.requestHold(MyGLSurfaceView.UNLOCKED);

            viewChanged = false;
        }

        continueDrawing();
    }

    float[] out = new float[4];
    float[] inverted = new float[16];
    float[] vPMatrix = new float[16];
    int resetIndex = 0;

    // Get camera space coordinates from normalized space
    public float[] glCoordinates(float normalizedX, float normalizedY) {
        out[0] = normalizedX;
        out[1] = normalizedY;
        out[2] = 0.0f;
        out[3] = 1.0f;

        for (resetIndex = 0; resetIndex < inverted.length; resetIndex++) {
            inverted[resetIndex] = 0;
            vPMatrix[resetIndex] = 0;
        }

//        System.out.println("MGLR| translation x,y: " + totalXTran + ", " + totalYTran);
//        System.out.println("MGLR| scale: " + totalScale);

        Matrix.multiplyMM(vPMatrix, 0, projectionMatrix, 0, viewMatrix, 0);

        Matrix.invertM(inverted, 0, vPMatrix, 0);

        Matrix.multiplyMV(out, 0, inverted, 0, out, 0);



//        out[0] = out[0] * ratio * scale / totalScale - (totalXTran * ratio);
//        out[1] = out[1] * scale / totalScale - totalYTran;

        return out;
    }

    float totalScale = 1.0f;
    float totalXTran = 0.0f;
    float totalYTran = 0.0f;

    private void continueDrawing() {
        if(MyGLSurfaceView.activeAction) {
            myView.requestRender();
        }
    }

    //TODO smooth this out
    public void scaleFromDist(float deltaDistance) {
        deltaDistance /= 30;
//        System.out.println("MGLR| deltaDist: " + deltaDistance);
        if(deltaDistance > 1) {
            deltaDistance = 1;
        } else if (deltaDistance < -1) {
            deltaDistance = -1;
        }
        deltaDistance *= -1;

        //System.out.println("MGLR| deltaDist after: " + deltaDistance);

        scaleView(deltaDistance);

    }

    public void scaleView(float scale) {
        totalScale += scale;

        //System.out.println("MGLR| total scale: "+ totalScale);

        if(totalScale < 0.15f && totalScale > 0.01f) {
            updateView();
        } else {
            //undo last change so the user can shrink or expand immediately
            totalScale -= scale;
        }
    }

    public void setTotalScale(float scale) {
        totalScale = scale;
    }

    public void translateView(float x, float y) {
//        System.out.println("MGLSV| finger move x,y: " + x + ", " + y);
        totalXTran += x/totalScale;
        totalYTran += y/totalScale;

        updateView();
    }

    private void updateView() {

        viewChanged = true;

        Matrix.setIdentityM(viewMatrix, 0);

//        // Position the eye behind the origin
//        final float eyeX = 0.0f;
//        final float eyeY = 0.0f;
//        final float eyeZ = 1.5f;
//
//        // We are looking toward the distance
//        final float lookX = 0.0f;
//        final float lookY = 0.0f;
//        final float lookZ = -5.0f;
//
//        // Set our up vector. This is where our head would be pointing were we holding the camera.
//        final float upX = 0.0f;
//        final float upY = 1.0f;
//        final float upZ = 0.0f;
//
//        // Set the view matrix. This matrix can be said to represent the camera position.
//
//        // NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model and
//        // view matrix. In OpenGL 2, we can keep track of these matrices separately if we choose.
//        Matrix.setLookAtM(viewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);


      // Old
      Matrix.scaleM(viewMatrix, 0, totalScale, totalScale, 1.0f);
      Matrix.translateM(viewMatrix, 0, totalXTran, totalYTran, 0);

        // New - doesn't work
//        Matrix.translateM(viewMatrix, 0, totalXTran, totalYTran, 0);
//        Matrix.scaleM(viewMatrix, 0, totalScale, totalScale, 1.0f);


        // Scale by hand
//        viewMatrix[0] *= totalScale;
//        viewMatrix[5] *= totalScale;
//
//        // Translate by hand
//        viewMatrix[12] = totalXTran;
//        viewMatrix[13] = totalYTran;


    }

    public float[] getViewMatrix() {
        return viewMatrix;
    }

    public float[] getProjectionMatrix() {
        return projectionMatrix;
    }

    public Material getDefaultMaterial() {
        return defaultMaterial;
    }
}
