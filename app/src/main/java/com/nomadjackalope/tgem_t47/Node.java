package com.nomadjackalope.tgem_t47;

import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.function.Function;

/**
 * Created by benjamin on 1/23/17.
 *
 * This class is based on the Node class from Cocos2d-x
 *
 * The main features of a Node are:
 *      They contain other Node objects(addChild, removeChild, etc)
 *      They can execute actions(runAction, stopAction, etc)
 *
 *  Note: 01/24/2017 I need to change these private values to protected I think
 */
public class Node extends Observable implements Observer {

    // Identification // Currently not used
    private String name;
    private String id;

    // Most basic properties
    private Point position;
    private float scaleX;
    private float scaleY;
    private float rotation; // in degrees, counter-clockwise from x-axis
    private Point anchorPoint; // Point from which object rotates, scales, and translates
    private boolean visible;
    private boolean positionInNormCoords; // Position of object is based on normalized screen coordinates, not game coordinates
    private Point normPos = new Point();
    private float[] normPosGL = new float[4];

    // Children & parents
    protected ArrayList<Node> children;
    protected Node parent;
    private boolean childrenInheritPSR = false; // Children inherit position, rotation, scale
    private boolean relativePos = false; // if node position is relative to parent
    private Point offset; // this is the amount the node should be moved from relative parent

    // Rectangle for rendering
    protected Rectangle baseShape;
    Material material;

    private TouchListener touchRunnable;
    private boolean touchEvenIfInvisible;
    private boolean ignoreTouch; // This node does not check for touches

    private GameEvent gameEvent;

    private ArrayList<Action> actions = new ArrayList<>();
    private ArrayList<Integer> actionsToRemove = new ArrayList<>();


    // ------------------- Constructors ---------------------------------------

    public Node() {
        initDefaults("");
    }

    public Node(String name) {
        initDefaults(name);
    }

    protected void initDefaults(String name) {
        this.name = name;
        id = "";

        position = new Point();
        setPosition(0, 0);
        setScale(1f);
        setRotation(0f);
        setAnchorPoint(new Point());
        setVisible(true);
        setTouchEvenIfInvisible(false);
        setIgnoreTouch(false);

        children = new ArrayList<>();
        parent = null;

        baseShape = new Rectangle(name);

        material = MyGLRenderer.myMaterial;
    }



    // ------------------- Main Functions -------------------------------------

    // Children and parents -----------------------------
    public Node addChild(Node child) {
        child.setParent(this);
        children.add(child);
        return child;
    }

    // Returns true if successful
    public boolean removeChild(Node child) {
        if(children.remove(child)) {
            child.parent = null;
            return true;
        }
        return false;
    }

    /**
    * Removes children from this node // Removes them from memory if they have no other reference? Should it?
    * Removing children from the root node means that they won't be checked for touches or rendered
    * Using clear sets the references to null and resizes the arraylist to 0. So, the objects
    *     are still there but if they have no more references they should be picked up by GC
    * If they have children that have references to them do they get deleted? How would I test this?
    */
    public void removeAllChildren() {
        int childrenSize = children.size();
        for (int i = 0; i < childrenSize; i++) {
            children.get(i).setParent(null);
        }
        children.clear();
    }


    // Drawing ------------------------------------------

    /**
     * Calls the base shape to render object to screen.
     * Continues draw call to all children.
     * This chain of draw calls is started in renderer.draw() by calling surfaceview.rootnode.draw()
     *
     * @param renderer the current renderer being used by OpenGL
     */
    public void draw(MyGLRenderer renderer) {
        if(material == null) {
            material = renderer.getDefaultMaterial();
        }

        // TODO Remove this to increase performance but make sure it happens if the view matrix changes
        if(MyGLRenderer.viewChanged) {
            updateMatrix();
        }


        // Actions get removed if they return true indicating they are finished
        for (int i = 0; i < actions.size(); i++) {
            if(actions.get(i).update(MyGLRenderer.clock)) {
                actions.remove(i);
                i--;
            }
        }

        if(visible) {
            baseShape.draw(renderer.getViewMatrix(), renderer.getProjectionMatrix(),
                    material, position, scaleX, scaleY, rotation);
        }

        // Tells children to render
        int childrenSize = children.size();
        for (int i = 0; i < childrenSize; i++) {

//            System.out.print("N| name & i & size: ");
//            System.out.print(name);
//            System.out.print(", ");
//            System.out.print(i);
//            System.out.print(", ");
//            System.out.print(childrenSize);
//            System.out.println();

            children.get(i).draw(renderer);
        }

    }

    public void updateBaseShapeMatrix(MyGLRenderer renderer) {
        baseShape.updateMatrix(renderer.getViewMatrix(), renderer.getProjectionMatrix(), position, scaleX, scaleY, rotation);
    }

    // Only sets the visibility of immediate children not grandchildren nor this node(the parent)
    public void setAllChildrenVisibility(boolean visible) {
        int childrenSize = children.size();
        for (int i = 0; i < childrenSize; i++) {
            children.get(i).setVisible(visible);
        }
    }

    // Keeps the base shape drawing position, rotation, scale matrix updated
    public void updateMatrix() {
        if(material == null) {
            return;
        }

        if(isPositionInNormCoords()) {
            normPosGL = material.renderer.glCoordinates(getPositionX(), getPositionY());
            normPos.set(normPosGL[0], normPosGL[1], normPosGL[2]);
            baseShape.updateMatrix(
                    material.renderer.getViewMatrix(),
                    material.renderer.getProjectionMatrix(),
                    normPos,
                    scaleX,
                    scaleY,
                    rotation);
        } else {
            baseShape.updateMatrix(
                    material.renderer.getViewMatrix(),
                    material.renderer.getProjectionMatrix(),
                    position,
                    scaleX,
                    scaleY,
                    rotation);
        }
    }


    // Touches ------------------------------------------
    // Checks if this object or its children contain the touch point. If so it adds them to touched objects array.
    public ArrayList<Node> checkTouch(Point position, ArrayList<Node> touchedNodes) {
        // Only receives touches if this node is visible
        if(!ignoreTouch && baseShape.isTouched(position, false) && (visible || touchEvenIfInvisible)) { touchedNodes.add(this); }

        // Check children
        int childrenSize = children.size();
        for (int i = 0; i < childrenSize; i++) {
            children.get(i).checkTouch(position, touchedNodes);
        }

        return touchedNodes;
    }

    // Called from MyGLSurfaceView if after checkTouch finishes this node is the highest z
    //      or previous touch handling has not stopped the bubbling up
    // Use the executable for handling your logic.
    // Returns true if touch was handled
    public boolean onTouchDown(MotionEvent event, Point realPoint, Node sender) {
        System.out.println("N| " + name + " Node touched down");
        setChanged();
        notifyObservers("onTouchDown");
        return touchRunnable != null && touchRunnable.onTouchDown(event, realPoint, sender);
    }

    public boolean onTouchUp(MotionEvent event, Point realPoint, Node sender) {
        System.out.println("N| " + name + " Node touched up");
        setChanged();
        notifyObservers("onTouchUp");
        return touchRunnable != null && touchRunnable.onTouchUp(event, realPoint, sender);

    }

    public boolean onTouchMove(MotionEvent event, Point realPoint, Node sender) {
        System.out.println("N| " + name + " Node touched move");
        setChanged();
        notifyObservers("onTouchMove");
        return touchRunnable != null && touchRunnable.onTouchMove(event, realPoint, sender);

    }

    public void addTouchListener(TouchListener touchListener) {
        touchRunnable = touchListener;
    }



    // -------------------------- Actions -------------------------------------

    public void addAction(Action action) {
        actions.add(action);
    }

    public void removeAllActions() {
        actions.clear();
    }

    public ArrayList<Action> getActions() {
        return actions;
    }

    // -------------------------- Observer -------------------------------------

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("N| update calling");
        if(gameEvent != null) {
            gameEvent.run(this, arg);
        }
    }

    public void addGameEvent(GameEvent event) { gameEvent = event; }


    // -------------------- Getters and Setters --------------------------------


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        setPosition(position.x, position.y, position.z);
    }

    public void setPosition(float x, float y) {
        setPosition(x, y, getPositionZ());
    }

    // All position setting goes through here
    public void setPosition(float x, float y, float z) {
        this.position.x = x;
        this.position.y = y;
        this.position.z = z;

        if(childrenInheritPSR) {
            boolean stillTrue = false; // setting childrenInheritPSR = false if no children have relativePos = true
            for (int i = 0; i < children.size(); i++) {
                if(children.get(i).isRelativePos()) {
                    updateRelativePos(getPosition(), children.get(i));

                    stillTrue = true;
                }
            }
            setChildrenInheritPSR(stillTrue);
        }

        updateMatrix();
    }

    public float getPositionX() { return position.x; }

    public void setPositionX(float x) { setPosition(x, getPositionY(), getPositionZ()); }

    public float getPositionY() { return position.y; }

    public void setPositionY(float y) { setPosition(getPositionX(), y, getPositionZ()); }

    public float getPositionZ() { return position.z; }

    public void setPositionZ(float z) { setPosition(getPositionX(), getPositionY(), z); }

    public void setScale(float scale) {
        setScale(scale, scale);
    }

    public void setScale(float scaleX, float scaleY) {
        this.scaleX = scaleX;
        this.scaleY = scaleY;

        updateMatrix();
    }

    public float getScaleX() {
        return scaleX;
    }

    public void setScaleX(float scaleX) {
        setScale(scaleX, getScaleY());
    }

    public float getScaleY() {
        return scaleY;
    }

    public void setScaleY(float scaleY) {
        setScale(getScaleX(), scaleY);
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;

        updateMatrix();
    }

    public Point getAnchorPoint() {
        return anchorPoint;
    }

    public void setAnchorPoint(Point anchorPoint) {
        this.anchorPoint = anchorPoint;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setVisible(boolean visible, boolean applyToChildren) {
        this.visible = visible;

        if(applyToChildren) {
            for (int i = 0; i < children.size(); i++) {
                children.get(i).setVisible(visible, true);
            }
        }
    }

    public boolean isTouchEvenIfInvisible() {
        return touchEvenIfInvisible;
    }

    public void setTouchEvenIfInvisible(boolean touchEvenIfInvisible) {
        this.touchEvenIfInvisible = touchEvenIfInvisible;
    }

    public boolean isIgnoreTouch() {
        return ignoreTouch;
    }

    public void setIgnoreTouch(boolean ignoreTouch) {
        this.ignoreTouch = ignoreTouch;
    }

    // Children and parents
    public ArrayList<Node> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Node> children) {
        this.children = children;
    }

    public Node getChild(int index) { return children.get(index); }

    // public Node getChildByName(String name) { return ....

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;

        if(parent == null) {
            return;
        }

        if(relativePos) {
            getParent().setChildrenInheritPSR(true);

            updateRelativePos(getParent().getPosition(), this);
        }
    }


    public boolean isRelativePos() {
        return relativePos;
    }

    public void setRelativePos(boolean relativePos, Point offset) {
        setOffset(offset);

        if(getParent() != null) {
            updateRelativePos(getParent().getPosition(), this);
        }

        this.relativePos = relativePos;
    }

    private void updateRelativePos(Point parentPos, Node child) {
        child.setPosition(
                parentPos.x + child.getOffset().x,
                parentPos.y + child.getOffset().y,
                parentPos.z + child.getOffset().z);
    }

    public boolean isChildrenInheritPSR() {
        return childrenInheritPSR;
    }

    public void setChildrenInheritPSR(boolean childrenInheritPSR) {
        this.childrenInheritPSR = childrenInheritPSR;
    }

    public Point getOffset() {
        return offset;
    }

    private void setOffset(Point offset) {
        this.offset = offset;
    }

    public boolean isPositionInNormCoords() {
        return positionInNormCoords;
    }

    public void setPositionInNormCoords(boolean positionInNormCoords) {
        this.positionInNormCoords = positionInNormCoords;
    }

    public void delete() {
        for (int i = 0; i < children.size(); i++) {
            children.get(i).delete();
        }
        children = null;
    }
}
