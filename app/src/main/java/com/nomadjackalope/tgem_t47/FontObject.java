package com.nomadjackalope.tgem_t47;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;

/**
 * Created by benjamin on 3/27/17.
 *
 * This object stores a font of a specific size?
 *
 * Anything that is not dynamic between nodes is stored here
 */
public class FontObject {

    //--Constants--//
    public final static int CHAR_START = 32;           // First Character (ASCII Code)
    public final static int CHAR_END = 126;            // Last Character (ASCII Code)
    public final static int CHAR_CNT = ( ( ( CHAR_END - CHAR_START ) + 1 ) + 1 );  // Character Count (Including Character to use for Unknown)

    public final static int CHAR_NONE = 32;            // Character to Use for Unknown (ASCII Code)
    public final static int CHAR_UNKNOWN = ( CHAR_CNT - 1 );  // Index of the Unknown Character

    public final static int FONT_SIZE_MIN = 6;         // Minumum Font Size (Pixels)
    public final static int FONT_SIZE_MAX = 180;       // Maximum Font Size (Pixels)

    public final static int CHAR_BATCH_SIZE = 24;     // Number of Characters to Render Per Batch
    // must be the same as the size of u_MVPMatrix
    // in BatchTextProgram

    private float charWidthMax;                                // Character Width (Maximum; Pixels)
    private float charHeight;                                  // Character Height (Maximum; Pixels)
    private final float[] charWidths;                          // Width of Each Character (Actual; Pixels)
    private TextureRegion[] charRgn;                           // Region of Each Character (Texture Coordinates)
    private int cellWidth, cellHeight;                         // Character Cell Width/Height
    private int rowCnt, colCnt;

    private int fontPadX, fontPadY;                            // Font Padding (Pixels; On Each Side, ie. Doubled on Both X+Y Axis)

    private float fontHeight;                                  // Font Height (Actual; Pixels)
    private float fontAscent;                                  // Font Ascent (Above Baseline; Pixels)
    private float fontDescent;                                 // Font Descent (Below Baseline; Pixels)

    private int textureId;                                     // Font Texture ID [NOTE: Public for Testing Purposes Only!]
    private int textureSize;                                   // Texture Size for Font (Square) [NOTE: Public for Testing Purposes Only!]

    TextureRegion textureRgn;                          // Full Texture Region

    AssetManager assets;

    public FontObject(AssetManager assets) {
        this.assets = assets;

        charWidths = new float[FontObject.CHAR_CNT];               // Create the Array of Character Widths
        charRgn = new TextureRegion[FontObject.CHAR_CNT];          // Create the Array of Character Regions

        // initialize remaining members
        fontPadX = 0;
        fontPadY = 0;

        fontHeight = 0.0f;
        fontAscent = 0.0f;
        fontDescent = 0.0f;

        textureId = -1;
        textureSize = 0;

    }

    public boolean load(String file, int size, int padX, int padY) {

        // setup requested values
        fontPadX = padX;                                // Set Requested X Axis Padding
        fontPadY = padY;                                // Set Requested Y Axis Padding

        // load the font and setup paint instance for drawing
        Typeface tf = Typeface.createFromAsset( assets, file );  // Create the Typeface from Font File
        Paint paint = new Paint();                      // Create Android Paint Instance
        paint.setAntiAlias( true );                     // Enable Anti Alias
        paint.setTextSize( size );                      // Set Text Size
        paint.setColor( 0xffffffff );                   // Set ARGB (White, Opaque)
        paint.setTypeface( tf );                        // Set Typeface

        // get font metrics
        Paint.FontMetrics fm = paint.getFontMetrics();  // Get Font Metrics
        fontHeight = (float)Math.ceil( Math.abs( fm.bottom ) + Math.abs( fm.top ) );  // Calculate Font Height
        fontAscent = (float)Math.ceil( Math.abs( fm.ascent ) );  // Save Font Ascent
        fontDescent = (float)Math.ceil( Math.abs( fm.descent ) );  // Save Font Descent

        // determine the width of each character (including unknown character)
        // also determine the maximum character width
        char[] s = new char[2];                         // Create Character Array
        charWidthMax = charHeight = 0;                  // Reset Character Width/Height Maximums
        float[] w = new float[2];                       // Working Width Value
        int cnt = 0;                                    // Array Counter
        for ( char c = CHAR_START; c <= CHAR_END; c++ )  {  // FOR Each Character
            s[0] = c;                                    // Set Character
            paint.getTextWidths( s, 0, 1, w );           // Get Character Bounds
            charWidths[cnt] = w[0];                      // Get Width
            if ( charWidths[cnt] > charWidthMax )        // IF Width Larger Than Max Width
                charWidthMax = charWidths[cnt];           // Save New Max Width
            cnt++;                                       // Advance Array Counter
        }
        s[0] = CHAR_NONE;                               // Set Unknown Character
        paint.getTextWidths( s, 0, 1, w );              // Get Character Bounds
        charWidths[cnt] = w[0];                         // Get Width
        if ( charWidths[cnt] > charWidthMax )           // IF Width Larger Than Max Width
            charWidthMax = charWidths[cnt];              // Save New Max Width
        cnt++;                                          // Advance Array Counter

        // set character height to font height
        charHeight = fontHeight;                        // Set Character Height

        // find the maximum size, validate, and setup cell sizes
        cellWidth = (int)charWidthMax + ( 2 * fontPadX );  // Set Cell Width
        cellHeight = (int)charHeight + ( 2 * fontPadY );  // Set Cell Height
        int maxSize = cellWidth > cellHeight ? cellWidth : cellHeight;  // Save Max Size (Width/Height)
        if ( maxSize < FONT_SIZE_MIN || maxSize > FONT_SIZE_MAX )  // IF Maximum Size Outside Valid Bounds
            return false;                                // Return Error

        // set texture size based on max font size (width or height)
        // NOTE: these values are fixed, based on the defined characters. when
        // changing start/end characters (CHAR_START/CHAR_END) this will need adjustment too!
        if ( maxSize <= 24 )                            // IF Max Size is 18 or Less
            textureSize = 256;                           // Set 256 Texture Size
        else if ( maxSize <= 40 )                       // ELSE IF Max Size is 40 or Less
            textureSize = 512;                           // Set 512 Texture Size
        else if ( maxSize <= 80 )                       // ELSE IF Max Size is 80 or Less
            textureSize = 1024;                          // Set 1024 Texture Size
        else                                            // ELSE IF Max Size is Larger Than 80 (and Less than FONT_SIZE_MAX)
            textureSize = 2048;                          // Set 2048 Texture Size

        // create an empty bitmap (alpha only)
        Bitmap bitmap = Bitmap.createBitmap( textureSize, textureSize, Bitmap.Config.ALPHA_8 );  // Create Bitmap
        Canvas canvas = new Canvas( bitmap );           // Create Canvas for Rendering to Bitmap
        bitmap.eraseColor( 0x00000000 );                // Set Transparent Background (ARGB)

        // calculate rows/columns
        // NOTE: while not required for anything, these may be useful to have :)
        colCnt = textureSize / cellWidth;               // Calculate Number of Columns
        rowCnt = (int)Math.ceil( (float)CHAR_CNT / (float)colCnt );  // Calculate Number of Rows

        // render each of the characters to the canvas (ie. build the font map)
        float x = fontPadX;                             // Set Start Position (X)
        float y = ( cellHeight - 1 ) - fontDescent - fontPadY;  // Set Start Position (Y)
        for ( char c = CHAR_START; c <= CHAR_END; c++ )  {  // FOR Each Character
            s[0] = c;                                    // Set Character to Draw
            canvas.drawText( s, 0, 1, x, y, paint );     // Draw Character
            x += cellWidth;                              // Move to Next Character
            if ( ( x + cellWidth - fontPadX ) > textureSize )  {  // IF End of Line Reached
                x = fontPadX;                             // Set X for New Row
                y += cellHeight;                          // Move Down a Row
            }
        }
        s[0] = CHAR_NONE;                               // Set Character to Use for NONE
        canvas.drawText( s, 0, 1, x, y, paint );        // Draw Character

        // save the bitmap in a texture
        textureId = TextureHelper.loadTexture(bitmap);

        // setup the array of character texture regions
        x = 0;                                          // Initialize X
        y = 0;                                          // Initialize Y
        for ( int c = 0; c < CHAR_CNT; c++ )  {         // FOR Each Character (On Texture)
            charRgn[c] = new TextureRegion( textureSize, textureSize, x, y, cellWidth-1, cellHeight-1 );  // Create Region for Character
            x += cellWidth;                              // Move to Next Char (Cell)
            if ( x + cellWidth > textureSize )  {
                x = 0;                                    // Reset X Position to Start
                y += cellHeight;                          // Move to Next Row (Cell)
            }
        }

        // create full texture region
        textureRgn = new TextureRegion( textureSize, textureSize, 0, 0, textureSize, textureSize );  // Create Full Texture Region

        // return success
        return true;                                    // Return Success
    }

    public int getFontPadX() {
        return fontPadX;
    }

    public int getFontPadY() {
        return fontPadY;
    }

    public TextureRegion[] getCharRgn() {
        return charRgn;
    }

    public float[] getCharWidths() {
        return charWidths;
    }

    public int getCellWidth() {
        return cellWidth;
    }

    public int getCellHeight() {
        return cellHeight;
    }

    public int getTextureId() {
        return textureId;
    }
}
